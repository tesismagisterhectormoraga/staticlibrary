/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.wrappers;

import cl.uchile.dcc.staticlibrary.primitives.Edge2D;
import java.util.Objects;

/**
 *
 * @author hmoraga
 */
public class Edge2DWrapper {

    private Edge2D arista;
    private int id;

    /**
     *
     * @param arista
     * @param id
     */
    public Edge2DWrapper(Edge2D arista, int id) {
        this.arista = arista;
        this.id = id;
    }

    /**
     *
     * @return
     */
    public Edge2D getArista() {
        return this.arista;
    }

    /**
     *
     * @return
     */
    public int getId() {
        return this.id;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.arista);
        hash = 67 * hash + this.id;
        return hash;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final Edge2DWrapper other = (Edge2DWrapper) obj;
        if (this.id != other.id) {
            return false;
        }
        return Objects.equals(this.arista, other.arista);
    }
}
