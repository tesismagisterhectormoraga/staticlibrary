/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.wrappers;

import cl.uchile.dcc.staticlibrary.primitives.AABB2D;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import java.util.Objects;

/**
 *
 * @author hmoraga
 */
public class AABB2DWrapper {
    private AABB2D box;
    private int id;

    /**
     *
     * @param box
     * @param id
     */
    public AABB2DWrapper(AABB2D box, int id) {
        this.box = box;
        this.id = id;
    }

    /**
     *
     * @return
     */
    public AABB2D getBox() {
        return this.box;
    }

    /**
     *
     * @param box
     */
    public void setBox(AABB2D box) {
        this.box = box;
    }

    /**
     *
     * @return
     */
    public int getId() {
        return this.id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.box);
        hash = 47 * hash + this.id;
        return hash;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final AABB2DWrapper other = (AABB2DWrapper) obj;
        if (this.id != other.id) {
            return false;
        }
        return Objects.equals(this.box, other.box);
    }

    /**
     *
     * @param p
     */
    public void move(Point2D p) {
        this.box.move(p);
    }
}
