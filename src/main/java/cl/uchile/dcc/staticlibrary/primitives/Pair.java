/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.primitives;

import java.util.List;

/**
 *
 * @author hmoraga
 * @param <T>
 */
public interface Pair<T> {
    @Override
    public String toString();
    @Override
    public boolean equals(Object o);
    @Override
    public int hashCode();
    public T getFirst();    
    public T getSecond();
    public void setFirst(T first);    
    public void setSecond(T second);
    public Pair<T> inverted();
    public boolean exists(List<Pair<T>> listaPares);
    public Pair<T> intersection(Pair<T> otro);
    public boolean intersects(Pair<T> otro);
    
}
