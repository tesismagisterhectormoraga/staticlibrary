/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.primitives;

/**
 *
 * @author hmoraga
 */
public interface Primitive extends Cloneable {
    public Edge getEdge();
    public int getIndex();
}
