/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.primitives;

import static java.lang.Math.max;
import static java.lang.Math.min;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author hmoraga
 */
public class PairDouble implements Pair<Double> {
    private Double first, second;

    /**
     *
     * @param first
     * @param second
     */
    public PairDouble(double first, double second) {
        this.first = first;
        this.second = second;
    }

    /**
     *
     * @param otro
     */
    public PairDouble(Pair<Double> otro) {
        this.first = otro.getFirst();
        this.second = otro.getSecond();                
    }

    /**
     *
     * @return
     */
    @Override
    public Pair<Double> inverted() {
        return new PairDouble(this.second, this.first);
    }

    /**
     *
     * @param listaPares
     * @return
     */
    @Override
    public boolean exists(List<Pair<Double>> listaPares) {
        return (listaPares.contains(this) || listaPares.contains(this.inverted()));
    }

    /**
     *
     * @param first
     */
    @Override
    public void setFirst(Double first) {
        this.first = first;
    }

    /**
     *
     * @return
     */
    @Override
    public Double getFirst() {
        return this.first;
    }

    /**
     *
     * @return
     */
    @Override
    public Double getSecond() {
        return this.second;
    }

    /**
     *
     * @param second
     */
    @Override
    public void setSecond(Double second) {
        this.second = second;
    }
    
    /**
     *
     * @param otro
     * @return
     */
    @Override
    public Pair<Double> intersection(Pair<Double> otro){
        if (this.second < otro.getFirst() || otro.getSecond() < this.first) {
            return null;
        } else {
            return new PairDouble(max(this.first, otro.getFirst()), min(this.second, otro.getSecond()));
        }
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.first);
        hash = 47 * hash + Objects.hashCode(this.second);
        return hash;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final PairDouble other = (PairDouble) obj;

        return ((Objects.equals(this.second, other.second)
                && Objects.equals(this.first, other.first))
                || (Objects.equals(this.first, other.second)
                && Objects.equals(this.second, other.first)));
    }

    @Override
    public boolean intersects(Pair<Double> otro) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
