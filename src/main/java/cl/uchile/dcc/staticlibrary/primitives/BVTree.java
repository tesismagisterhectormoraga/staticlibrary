/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.primitives;

import cl.uchile.dcc.staticlibrary.poligons.AnyPolygon2D;
import cl.uchile.dcc.staticlibrary.poligons.Polygon2D;
import static cl.uchile.dcc.staticlibrary.primitives.AABB2D.sortFunction;
import static cl.uchile.dcc.staticlibrary.primitives.BVNode.obtainMinimalAreaCombinadedNode;
import cl.uchile.dcc.staticlibrary.wrappers.ListAABB2D;
import cl.uchile.dcc.staticlibrary.wrappers.ListPuntos2D;
import static java.lang.Math.pow;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author hmoraga
 */
public class BVTree {

    private BVNode root;

    /**
     *
     */
    public BVTree() {
        this.root = null;
    }

    /**
     *
     * @param objeto
     */
    public BVTree(Polygon2D objeto) {
        switch (objeto.getEdgeList().size()) {
            case 0:
                this.root = null;
                break;
            case 1:
                this.root = new BVNode(new AABB2D(objeto.getEdgeList().get(0), 0));
                break;
            default:
                ListAABB2D listaCajas = new ListAABB2D();
                List<BVNode> listaNodos = new ArrayList<>();
                int i = 0;

                for (Edge2D arista : objeto.getEdgeList()) {
                    listaCajas.add(new AABB2D(arista, i++));
                }

                //ordeno la lista de AABB2D
                sortFunction(listaCajas);

                listaCajas.stream().map((boundingVolume) -> new BVNode(boundingVolume)).forEachOrdered((nodo) -> {
                    listaNodos.add(nodo);
        });

                this.crearArbol(listaNodos);
                break;
        }
    }

    /**
     *
     * @param listaPuntos
     */
    public BVTree(ListPuntos2D listaPuntos) {
        this(new AnyPolygon2D((List<Point2D>) listaPuntos, false));
    }

    /**
     *
     * @param raiz
     */
    public BVTree(BVNode raiz) {
        this.root = raiz;
    }

    /**
     *
     * @param listaBVs
     */
    public BVTree(List<? extends BV> listaBVs) {
        switch (listaBVs.size()) {
            case 0:
                this.root = null;
                break;
            case 1:
                this.root = new BVNode(listaBVs.get(0));
                break;
            default:
                List<BVNode> listaNodos = new ArrayList<>();
                // se supone que si el vector no viene ordenado hago el orden de acuerdo a la posicion primero
                // y despues respecto a su volumen

                if (listaBVs.get(0) instanceof AABB2D) {
                    sortFunction((List<AABB2D>) listaBVs);
                }

                listaBVs.stream().forEach((caja) -> {
                    listaNodos.add(new BVNode(caja));
                });

                this.crearArbol(listaNodos);
                break;
        }
    }

    /**
     *
     * @return
     */
    public BVNode getRoot() {
        return this.root;
    }

    /**
     *
     * @param listaNodos
     */
    protected final void crearArbol(List<BVNode> listaNodos) {
        // construyo el arbol bottom-up
        switch (listaNodos.size()) {
            case 0:
                this.root = null;
                break;
            case 1:
                this.root = new BVNode(listaNodos.get(0));
                break;
            default:
                do {
                    List<BVNode> listaNodosTmp = new ArrayList<>();

                    //averiguo el tamaño de nodos de la lista
                    int n = listaNodos.size();

                    //si el numero es par debo hacer n/2 veces la siguiente operacion
                    for (int i = 0; i < n / 2; i++) {
                        BVNode nodoNuevo = obtainMinimalAreaCombinadedNode(listaNodos);
                        listaNodosTmp.add(nodoNuevo);
                    }

                    //en el caso que n es impar quedaba un nodo extra
                    if (n % 2 != 0) {
                        listaNodosTmp.add(listaNodos.get(0));
                    }

                    listaNodos = new ArrayList<>(listaNodosTmp);
                } while (listaNodos.size() > 1);

                this.root = new BVNode(listaNodos.get(0));
                break;
        }
    }

    /**
     *
     * @param listaNodos
     * @return
     */
    public static Pair<Double> getVariance(List<BVNode> listaNodos) {
        Pair<Double> mean = getMean(listaNodos);
        double varX = 0, varY = 0;
        Iterator<BVNode> it = listaNodos.iterator();

        while (it.hasNext()) {
            BV p = it.next().unwrap();

            double ptoX = (p.getMaximum(0) + p.getMinimum(0)) / 2;
            double ptoY = (p.getMaximum(1) + p.getMinimum(1)) / 2;

            varX += pow(ptoX, 2.0);
            varY += pow(ptoY, 2.0);
        }

        varX /= listaNodos.size();
        varY /= listaNodos.size();

        varX -= pow(mean.getFirst(), 2);
        varY -= pow(mean.getSecond(), 2);

        return new PairDouble(varX, varY);
    }

    /**
     *
     * @param listaNodos
     * @return
     */
    public static Pair<Double> getMean(List<BVNode> listaNodos) {
        double meanX = 0, meanY = 0;

        Iterator<BVNode> it = listaNodos.iterator();

        while (it.hasNext()) {
            BV box = it.next().unwrap();

            double ptoX = (box.getMaximum(0) + box.getMinimum(0)) / 2;
            double ptoY = (box.getMaximum(1) + box.getMinimum(1)) / 2;

            meanX += ptoX;
            meanY += ptoY;
        }

        meanX /= listaNodos.size();
        meanY /= listaNodos.size();

        return new PairDouble(meanX, meanY);
    }

    /**
     *
     * @param dimension
     * @return
     */
    public Pair<Double> obtainEnds(int dimension) {
        BV datosRaiz = this.getRoot().getData();

        return new PairDouble(datosRaiz.getMinimum(dimension), datosRaiz.getMaximum(dimension));
    }

    /**
     *
     * @param listaColisiones
     * @param objetoUno
     * @param objetoDos
     */
    public static void obtainCollidingEdges(List<Pair<Integer>> listaColisiones, BVTree objetoUno, BVTree objetoDos) {
        BVNode.obtainCollidingEdges(listaColisiones, objetoUno.getRoot(), objetoDos.getRoot());
    }
}
