/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.primitives;

import java.util.Objects;

/**
 *
 * @author hmoraga
 */
public class Primitive2D implements Primitive, Cloneable {
    private final Edge2D objeto;
    private final int indice;

    /**
     *
     * @param objeto
     * @param indice
     */
    public Primitive2D(Edge2D objeto, int indice) {
        this.objeto = objeto;
        this.indice = indice;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "Primitive2D{" + "objeto=" + this.objeto + ", indice=" + this.indice + '}';
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.objeto);
        hash = 17 * hash + this.indice;
        return hash;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final Primitive2D other = (Primitive2D) obj;
        if (this.indice != other.indice) {
            return false;
        }
        return Objects.equals(this.objeto, other.objeto);
    }

    /**
     *
     * @return
     */
    @Override
    public Edge2D getEdge() {
        return this.objeto;
    }

    /**
     *
     * @return
     */
    @Override
    public int getIndex() {
        return this.indice;
    }
}
