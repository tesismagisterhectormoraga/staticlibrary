/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.primitives;

/**
 *
 * @author hmoraga
 */
public interface Point {

    /**
     *
     * @param dimension
     * @return
     */
    public double getCoords(int dimension);

    /**
     *
     * @param p
     */
    public void add(Point p);

    /**
     *
     * @param p
     * @return
     */
    public double distanceTo(Point p);

    /**
     *
     * @param p
     * @return
     */
    public double distanceSquaredTo(Point p);

    /**
     *
     * @param xmin
     * @param xmax
     * @param ymin
     * @param ymax
     * @param A
     * @param B
     * @return
     */
    public Point linearTransform(double xmin, double xmax, double ymin, double ymax, double A, double B);

    /**
     *
     * @param scale
     * @return
     */
    public Point multipliedByScalar(double scale);
}
