/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.primitives;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.lang.System.out;
import static java.util.Arrays.deepHashCode;
import java.util.Objects;
import static java.util.logging.Level.SEVERE;
import static java.util.logging.Logger.getLogger;

/**
 *
 * @author hmoraga
 */
public class Edge2D implements Edge {
    private Point2D[] puntos;

    public Edge2D() {
        this.puntos = new Point2D[2];
    }

    public Edge2D(Point2D a, Point2D b) {
        this.puntos = new Point2D[2];
        this.puntos[0] = a;
        this.puntos[1] = b;
    }

    public Point2D[] getPoints() {
        return this.puntos;
    }

    public double signed2DTriArea(Point2D c) {
        double ax = this.puntos[0].getCoords(0);
        double ay = this.puntos[0].getCoords(1);
        double bx = this.puntos[1].getCoords(0);
        double by = this.puntos[1].getCoords(1);
        double cx = c.getCoords(0);
        double cy = c.getCoords(1);

        return (ax - cx) * (by - cy) - (ay - cy) * (bx - cx);
    }

    /**
     *
     * @param c
     * @return
     */
    public double signed2DTriArea(Point c) {
        Point2D pto = (Point2D)c;
        
        return (pto!=null)?this.signed2DTriArea(pto):0.0;
    }

    /**
     * Revisa si mi arista intersecta a la arista ingresada como parámetro.
     * <P>
     * Para ello se debe cumplir que un punto de la arista parametro debe dar
     * signo &gt;= 0 y la otra &lt;=0 respecto de this, y los puntos de esta
     * arista arista deben dar signos alternados respecto a la arista parametro.
     *
     * @param arista
     * @return
     */
    public boolean intersects(Edge2D arista) {
        boolean x1 = ((this.signed2DTriArea(arista.puntos[0]) >= 0) && (this.signed2DTriArea(arista.puntos[1]) <= 0))
                || ((this.signed2DTriArea(arista.puntos[0]) <= 0) && (this.signed2DTriArea(arista.puntos[1]) >= 0));
        boolean x2 = ((arista.signed2DTriArea(this.puntos[0]) >= 0) && (arista.signed2DTriArea(this.puntos[1]) <= 0))
                || ((arista.signed2DTriArea(this.puntos[0]) <= 0) && (arista.signed2DTriArea(this.puntos[1]) >= 0));
        return (x1 && x2);
    }

    /**
     *
     * @param arista
     * @return
     */
    @Override
    public boolean intersects(Edge arista) {
        if (arista == null) {
            return false;
        }
        if (!Objects.equals(this.getClass(), arista.getClass())) {
            return false;
        }
        return this.intersects((Edge2D) arista);
    }

    /**
     *
     * @return
     */
    @Override
    public double getDistance() {
        return this.puntos[0].distanceTo(this.puntos[1]);
    }

    /**
     *
     * @param p
     */
    @Override
    public void add(Point p) {
        this.puntos[0].add(p);
        this.puntos[1].add(p);
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "Edge2D{a=" + this.puntos[0].toString() + ", b=" + this.puntos[1].toString() + '}';
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + deepHashCode(this.puntos);
        return hash;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!Objects.equals(this.getClass(), obj.getClass())) {
            return false;
        }
        final Edge2D other = (Edge2D) obj;
        // comparo los elementos en sus posiciones o al reves

        return ((this.puntos[0].equals(other.puntos[0]) && this.puntos[1].equals(other.puntos[1]))
                || (this.puntos[0].equals(other.puntos[1]) && this.puntos[1].equals(other.puntos[0])));
    }

    /**
     *
     * @param dimension
     * @return
     */
    public Edge2D getProjection(int dimension) {
        Point2D p = this.puntos[0];
        Point2D q = this.puntos[1];

        return (dimension == 0) ? new Edge2D(new Point2D(p.getCoords(dimension), 0), new Point2D(q.getCoords(dimension), 0))
                : new Edge2D(new Point2D(0, p.getCoords(dimension)), new Point2D(0, q.getCoords(dimension)));
    }

    /**
     *
     * @param o
     * @param dimension
     * @return
     */
    @Override
    public Edge2D axisProjectionIntersection(Edge o, int dimension) {
        Edge2D other = (Edge2D) o;
        Edge2D a, b;

        if ((other == null) || (this == null)) {
            return null;
        } else if (noExisteTraslape(this, other, dimension)) {
            return null;
        } else if (validarCaso8(this, other, dimension)) {
            return caso8(this, other, dimension);
        } else {
            // averiguo cual de las dos es de mayor longitud
            if (this.getProjection(dimension).getDistance() < other.getProjection(dimension).getDistance()){
                a = other;
                b = this;
            } else {
                a = this;
                b = other;
            }

            if (validarCaso1(a, b, dimension)) {
                return caso1(a, b, dimension);
            } else if (validarCaso2(a, b, dimension)) {
                return caso2(a, b, dimension);
            } else if (validarCaso3(a, b, dimension)) {
                return caso3(a, b, dimension);
            } else if (validarCaso4(a, b, dimension)) {
                return caso4(a, b, dimension);
            } else if (validarCaso5(a, b, dimension)) {
                return caso5(a, b, dimension);
            } else if (validarCaso6(a, b, dimension)) {
                return caso6(a, b, dimension);
            } else if (validarCaso7(a, b, dimension)) {
                return caso7(a, b, dimension);
            } else {
                try {
                    throw new Exception("Comparacion con situacion desconocida!");
                } catch (Exception ex) {
                    out.println("a="+a.toString());
                    out.println("b="+b.toString());
                    getLogger(Edge2D.class.getName()).log(SEVERE, null, ex);
                    return null;
                }                
            }
        }
    }

    /**
     *
     * @param dimension
     * @return
     */
    @Override
    public double getMinValue(int dimension) {
        return (dimension == 0) ? min(this.puntos[0].getX(), this.puntos[1].getX()) : min(this.puntos[0].getY(), this.puntos[1].getY());
    }

    /**
     *
     * @param dimension
     * @return
     */
    @Override
    public double getMaxValue(int dimension) {
        return (dimension == 0) ? max(this.puntos[0].getX(), this.puntos[1].getX()) : max(this.puntos[0].getY(), this.puntos[1].getY());
    }

    private static boolean noExisteTraslape(Edge2D a, Edge2D b, int dimension){
        Edge2D thisProjection = a.getProjection(dimension);
        Edge2D otherProjection = b.getProjection(dimension);

        double thisMin = min((thisProjection.getPoints()[0]).getCoords(dimension), (thisProjection.getPoints()[1]).getCoords(dimension));
        double thisMax = max((thisProjection.getPoints()[0]).getCoords(dimension), (thisProjection.getPoints()[1]).getCoords(dimension));        
        double otherMin = min((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        double otherMax = max((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        
        boolean caso1 = (thisMin < otherMin) && (thisMin < otherMax) && (thisMax < otherMin) && (thisMax < otherMax);
        boolean caso2 = (otherMin < thisMin) && (otherMin < thisMax) && (otherMax < thisMin) && (otherMax < thisMax);
    
        return (caso1 || caso2);
    }
    
    private static boolean validarCaso1(Edge2D a, Edge2D b, int dimension){
        Edge2D thisProjection = a.getProjection(dimension);
        Edge2D otherProjection = b.getProjection(dimension);

        double thisMin = min((thisProjection.getPoints()[0]).getCoords(dimension), (thisProjection.getPoints()[1]).getCoords(dimension));
        double thisMax = max((thisProjection.getPoints()[0]).getCoords(dimension), (thisProjection.getPoints()[1]).getCoords(dimension));        
        double otherMin = min((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        double otherMax = max((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        
        return ((thisMin < otherMin) && (otherMin < thisMax) && (thisMax < otherMax));
    }

    // Se supone que a > b
    private static Edge2D caso1(Edge2D a, Edge2D b, int dimension){
        Edge2D thisProjection = a.getProjection(dimension);
        Edge2D otherProjection = b.getProjection(dimension);

        double thisMin = min((thisProjection.getPoints()[0]).getCoords(dimension), (thisProjection.getPoints()[1]).getCoords(dimension));
        double thisMax = max((thisProjection.getPoints()[0]).getCoords(dimension), (thisProjection.getPoints()[1]).getCoords(dimension));        
        double otherMin = min((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        double otherMax = max((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        
        return (dimension==0)? new Edge2D(new Point2D(otherMin, 0), new Point2D(thisMax, 0)):new Edge2D(new Point2D(0, otherMin), new Point2D(0, thisMax));
    }

    private static boolean validarCaso2(Edge2D a, Edge2D b, int dimension){
        Edge2D thisProjection = a.getProjection(dimension);
        Edge2D otherProjection = b.getProjection(dimension);

        double thisMin = min((thisProjection.getPoints()[0]).getCoords(dimension), (thisProjection.getPoints()[1]).getCoords(dimension));
        double thisMax = max((thisProjection.getPoints()[0]).getCoords(dimension), (thisProjection.getPoints()[1]).getCoords(dimension));        
        double otherMin = min((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        //double otherMax = Math.max((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        
        return ((thisMin < otherMin) && (thisMax==otherMin));
    }

    private static Edge2D caso2(Edge2D a, Edge2D b, int dimension){
        Edge2D thisProjection = a.getProjection(dimension);
        Edge2D otherProjection = b.getProjection(dimension);

        //double thisMin = Math.min((thisProjection.getPoints()[0]).getCoords(dimension), (thisProjection.getPoints()[1]).getCoords(dimension));
        double thisMax = max((thisProjection.getPoints()[0]).getCoords(dimension), (thisProjection.getPoints()[1]).getCoords(dimension));        
        //double otherMin = Math.min((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        //double otherMax = Math.max((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        
        return (dimension==0)?new Edge2D(new Point2D(thisMax, 0), new Point2D(thisMax, 0)):new Edge2D(new Point2D(0, thisMax), new Point2D(0, thisMax));
    }

    private static boolean validarCaso3(Edge2D a, Edge2D b, int dimension){
        Edge2D thisProjection = a.getProjection(dimension);
        Edge2D otherProjection = b.getProjection(dimension);

        double thisMin = min((thisProjection.getPoints()[0]).getCoords(dimension), (thisProjection.getPoints()[1]).getCoords(dimension));
        double thisMax = max((thisProjection.getPoints()[0]).getCoords(dimension), (thisProjection.getPoints()[1]).getCoords(dimension));        
        double otherMin = min((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        double otherMax = max((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        
        return ((thisMin < otherMin) && (thisMax==otherMax));
    }

    private static Edge2D caso3(Edge2D a, Edge2D b, int dimension){
        //Edge2D thisProjection = a.getProjection(dimension);
        Edge2D otherProjection = b.getProjection(dimension);

        double otherMin = min((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        double otherMax = max((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        
        return (dimension==0)?new Edge2D(new Point2D(otherMin, 0), new Point2D(otherMax, 0)):new Edge2D(new Point2D(0, otherMin), new Point2D(0, otherMax));
    }

    private static boolean validarCaso4(Edge2D a, Edge2D b, int dimension){
        Edge2D thisProjection = a.getProjection(dimension);
        Edge2D otherProjection = b.getProjection(dimension);

        double thisMin = min((thisProjection.getPoints()[0]).getCoords(dimension), (thisProjection.getPoints()[1]).getCoords(dimension));
        double thisMax = max((thisProjection.getPoints()[0]).getCoords(dimension), (thisProjection.getPoints()[1]).getCoords(dimension));        
        double otherMin = min((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        double otherMax = max((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        
        return ((thisMin == otherMin) && (otherMax<thisMax));
    }

    private static Edge2D caso4(Edge2D a, Edge2D b, int dimension){
        Edge2D thisProjection = a.getProjection(dimension);
        Edge2D otherProjection = b.getProjection(dimension);

        //double thisMin = Math.min((thisProjection.getPoints()[0]).getCoords(dimension), (thisProjection.getPoints()[1]).getCoords(dimension));
        //double thisMax = Math.max((thisProjection.getPoints()[0]).getCoords(dimension), (thisProjection.getPoints()[1]).getCoords(dimension));        
        double otherMin = min((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        double otherMax = max((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        
        return (dimension==0)?new Edge2D(new Point2D(otherMin, 0), new Point2D(otherMax, 0)):new Edge2D(new Point2D(0, otherMin), new Point2D(0, otherMax));
    }

    private static boolean validarCaso5(Edge2D a, Edge2D b, int dimension){
        Edge2D thisProjection = a.getProjection(dimension);
        Edge2D otherProjection = b.getProjection(dimension);

        double thisMin = min((thisProjection.getPoints()[0]).getCoords(dimension), (thisProjection.getPoints()[1]).getCoords(dimension));
        double thisMax = max((thisProjection.getPoints()[0]).getCoords(dimension), (thisProjection.getPoints()[1]).getCoords(dimension));        
        double otherMin = min((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        double otherMax = max((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        
        return ((otherMin < thisMin) && (thisMin < otherMax) && (otherMax < thisMax));
    }

    private static Edge2D caso5(Edge2D a, Edge2D b, int dimension){
        Edge2D thisProjection = a.getProjection(dimension);
        Edge2D otherProjection = b.getProjection(dimension);

        double thisMin = min((thisProjection.getPoints()[0]).getCoords(dimension), (thisProjection.getPoints()[1]).getCoords(dimension));
        //double thisMax = Math.max((thisProjection.getPoints()[0]).getCoords(dimension), (thisProjection.getPoints()[1]).getCoords(dimension));        
        //double otherMin = Math.min((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        double otherMax = max((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        
        return (dimension==0)?new Edge2D(new Point2D(thisMin, 0), new Point2D(otherMax, 0)):new Edge2D(new Point2D(0, thisMin), new Point2D(0, otherMax));
    }
    
    private static boolean validarCaso6(Edge2D a, Edge2D b, int dimension){
        Edge2D thisProjection = a.getProjection(dimension);
        Edge2D otherProjection = b.getProjection(dimension);

        double thisMin = min((thisProjection.getPoints()[0]).getCoords(dimension), (thisProjection.getPoints()[1]).getCoords(dimension));
        double thisMax = max((thisProjection.getPoints()[0]).getCoords(dimension), (thisProjection.getPoints()[1]).getCoords(dimension));        
        double otherMin = min((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        double otherMax = max((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        
        return ((otherMin < thisMin) && (otherMax == thisMin));
    }

    private static Edge2D caso6(Edge2D a, Edge2D b, int dimension){
        Edge2D thisProjection = a.getProjection(dimension);
        Edge2D otherProjection = b.getProjection(dimension);

        double thisMin = min((thisProjection.getPoints()[0]).getCoords(dimension), (thisProjection.getPoints()[1]).getCoords(dimension));
        //double thisMax = Math.max((thisProjection.getPoints()[0]).getCoords(dimension), (thisProjection.getPoints()[1]).getCoords(dimension));        
        //double otherMin = Math.min((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        //double otherMax = Math.max((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        
        return (dimension==0)?new Edge2D(new Point2D(thisMin, 0), new Point2D(thisMin, 0)):new Edge2D(new Point2D(0, thisMin), new Point2D(0, thisMin));
    }

    private static boolean validarCaso7(Edge2D a, Edge2D b, int dimension){
        Edge2D thisProjection = a.getProjection(dimension);
        Edge2D otherProjection = b.getProjection(dimension);

        double thisMin = min((thisProjection.getPoints()[0]).getCoords(dimension), (thisProjection.getPoints()[1]).getCoords(dimension));
        double thisMax = max((thisProjection.getPoints()[0]).getCoords(dimension), (thisProjection.getPoints()[1]).getCoords(dimension));        
        double otherMin = min((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        double otherMax = max((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        
        return ((thisMin < otherMin) && (otherMax < thisMax));
    }

    private static Edge2D caso7(Edge2D a, Edge2D b, int dimension){
        //Edge2D thisProjection = a.getProjection(dimension);
        Edge2D otherProjection = b.getProjection(dimension);

        //double thisMin = Math.min((thisProjection.getPoints()[0]).getCoords(dimension), (thisProjection.getPoints()[1]).getCoords(dimension));
        //double thisMax = Math.max((thisProjection.getPoints()[0]).getCoords(dimension), (thisProjection.getPoints()[1]).getCoords(dimension));        
        double otherMin = min((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        double otherMax = max((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        
        return (dimension==0)?new Edge2D(new Point2D(otherMin, 0), new Point2D(otherMax, 0)):new Edge2D(new Point2D(0, otherMin), new Point2D(0, otherMax));
    }
    
    
    // solo usar cuando largo(a)==largo(b)
    private static boolean validarCaso8(Edge2D a, Edge2D b, int dimension){
        Edge2D thisProjection = a.getProjection(dimension);
        Edge2D otherProjection = b.getProjection(dimension);

        double thisMin = min((thisProjection.getPoints()[0]).getCoords(dimension), (thisProjection.getPoints()[1]).getCoords(dimension));
        double thisMax = max((thisProjection.getPoints()[0]).getCoords(dimension), (thisProjection.getPoints()[1]).getCoords(dimension));        
        double otherMin = min((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        double otherMax = max((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        
        return ((thisMin == otherMin) && (thisMax == otherMax));
    }

    private static Edge2D caso8(Edge2D a, Edge2D b, int dimension){
        Edge2D thisProjection = a.getProjection(dimension);
        //Edge2D otherProjection = b.getProjection(dimension);

        double thisMin = min((thisProjection.getPoints()[0]).getCoords(dimension), (thisProjection.getPoints()[1]).getCoords(dimension));
        double thisMax = max((thisProjection.getPoints()[0]).getCoords(dimension), (thisProjection.getPoints()[1]).getCoords(dimension));        
        //double otherMin = Math.min((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        //double otherMax = Math.max((otherProjection.getPoints()[0]).getCoords(dimension), (otherProjection.getPoints()[1]).getCoords(dimension));
        
        return (dimension==0)?new Edge2D(new Point2D(thisMin, 0), new Point2D(thisMax, 0)):new Edge2D(new Point2D(0, thisMin), new Point2D(0, thisMax));
    }
}
