/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.primitives;

import static java.lang.Math.PI;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;
import java.util.Arrays;
import static java.util.Arrays.fill;
import java.util.List;

/**
 *
 * @author hmoraga
 */
public class Point2D implements Point {
    private double[] coords;

    /**
     *
     */
    public Point2D() {
        this.coords = new double[2];
        fill(this.coords, 0);
    }

    /**
     *
     * @param x
     * @param y
     */
    public Point2D(double x, double y) {
        this.coords = new double[2];
        this.coords[0] = x;
        this.coords[1] = y;
    }

    /**
     *
     * @param coords
     */
    public Point2D(double[] coords) {
        this.coords = new double[2];

        assert (coords.length == 2);

        this.coords = coords;
    }

    /**
     *
     * @param p
     */
    public Point2D(Point2D p) {
        this.coords = new double[2];
        assert (this.coords.length == 2);
        this.coords[0] = p.getX();
        this.coords[1] = p.getY();
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }

        final Point2D other = (Point2D) obj;

        return ((this.getCoords(0) == other.getCoords(0)) && (this.getCoords(1) == other.getCoords(1)));
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Arrays.hashCode(this.coords);
        return hash;
    }

    /**
     *
     * @return
     */
    public double getX() {
        return this.getCoords(0);
    }

    /**
     *
     * @return
     */
    public double getY() {
        return this.getCoords(1);
    }

    /**
     *
     * @param dimension
     * @return
     */
    @Override
    public double getCoords(int dimension) {
        assert (dimension >= 0 && dimension < 2);
        return this.coords[dimension];
    }

    /**
     *
     * @param p
     * @return
     */
    @Override
    public double distanceTo(Point p) {
        try {
            return sqrt(this.distanceSquaredTo(p));
        } catch (ArithmeticException e) {
            return -1.0;
        }
    }

    /**
     *
     * @param p
     * @return
     */
    @Override
    public double distanceSquaredTo(Point p) {
        Point2D other = (Point2D)p;
        if (other == null) {
            return -1.0;
        }
        if (this.getClass() != p.getClass()) {
            return -1.0;
        } else {
            double sum = (this.coords[0]-other.coords[0])*(this.coords[0]-other.coords[0])+(this.coords[1]-other.coords[1])*(this.coords[1]-other.coords[1]);

            return sum;
        }
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        String s = "(";
        if (this.coords.length > 0) {
            for (int i = 0; i < this.coords.length - 1; i++) {
                s = s + this.coords[i] + ",";
            }
            s = s + this.coords[this.coords.length - 1] + ")";
            return s;
        } else {
            return null;
        }
    }

    /**
     *
     * @param listaPuntos
     * @return
     */
    public boolean exists(List<Point2D> listaPuntos) {
        return listaPuntos.contains(this);
    }

    /**
     *
     * @param box
     * @return
     */
    public boolean isInside(AABB2D box) {
        return box.contains(this);
    }

    /**
     *
     * @param p
     */
    public void add(Point2D p) {
        assert (p != null);
        this.add(p.getX(), p.getY());
    }

    /**
     *
     * @param x
     * @param y
     */
    public void add(double x, double y) {
        if (x != 0) {
            this.coords[0] += x;
        }
        if (y != 0) {
            this.coords[1] += y;
        }
    }

    /**
     *
     * @param p
     */
    @Override
    public void add(Point p) {
        Point2D pto = (Point2D) p;

        if (pto != null) {
            this.add(pto);
        }
    }

    /**
     * Me devuelve las coordenadas del punto pero si se hubiera rotado el eje
     * coordenado en un angulo angle
     *
     * @param angle en radianes
     * @return
     */
    public Point2D rotateBy(double angle) {
        if (angle != 0 && angle != 2 * PI) {
            double x = this.getX() * cos(angle) + this.getY() * sin(angle);
            double y = -this.getX() * sin(angle) + this.getY() * cos(angle);

            return new Point2D(x, y);
        }
        return this;
    }

    /**
     * Me multiplica el punto por un valor escalar
     *
     * @param scale valor escalar a multiplicar el punto
     * @return punto escalado
     */
    @Override
    public Point2D multipliedByScalar(double scale) {
        if (scale==0.0) {
            return new Point2D();
        } else if (scale==1.0) {
            return this;
        } else {
            return new Point2D(this.getX() * scale, this.getY() * scale);
        }
    }

    /**
     * Metodo para transformar coordenadas cartesianas a coordenadas de pantalla
     *
     * @param xmin valor mínimo en X de la simulacion (en coordenadas
     * cartesianas).
     * @param xmax valor máximo en X de la simulacion (en coordenadas
     * cartesianas).
     * @param ymin valor mínimo en Y de la simulacion (en coordenadas
     * cartesianas).
     * @param ymax valor máximo en Y de la simulacion (en coordenadas
     * cartesianas).
     * @param A cantidad de pixeles horizontales
     * @param B cantidad de pixeles verticales
     * @return valor del punto en coordenadas de pantalla
     */
    @Override
    public Point2D linearTransform(double xmin, double xmax, double ymin, double ymax, double A, double B) {
        double xp = A * (this.getX() - xmin) / (xmax - xmin);
        double yp = -B * (this.getY() - ymax) / (ymax - ymin);

        return new Point2D(xp, yp);
    }
}
