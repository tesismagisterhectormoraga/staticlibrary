/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.primitives;

import static java.util.Collections.sort;
import java.util.List;
import java.util.Objects;
import cl.uchile.dcc.staticlibrary.comparators.BVNodeComparator;

/**
 *
 * @author hmoraga
 */
public class BVNode implements Comparable<BVNode> {

    private BV data;
    private BVNode left;
    private BVNode right;

    /**
     *
     * @param data
     */
    public BVNode(BV data) {
        this.data = data;
    }

    /**
     *
     * @param nodo
     */
    public BVNode(BVNode nodo) {
        this.data = nodo.data;
        this.left = nodo.left;
        this.right = nodo.right;
    }

    /**
     *
     * @return
     */
    public BV getData() {
        return this.data;
    }

    /**
     *
     * @param data
     */
    public void setData(BV data) {
        this.data = data;
    }

    /**
     *
     * @return
     */
    public BVNode getLeft() {
        return this.left;
    }

    /**
     *
     * @param left
     */
    public void setLeft(BVNode left) {
        this.left = left;
    }

    /**
     *
     * @param data
     */
    public void setLeft(BV data) {
        this.left = new BVNode(data);
    }

    /**
     *
     * @return
     */
    public BVNode getRight() {
        return this.right;
    }

    /**
     *
     * @param right
     */
    public void setRight(BVNode right) {
        this.right = right;
    }

    /**
     *
     * @param data
     */
    public void setRight(BV data) {
        this.right = new BVNode(data);
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.data);
        hash = 29 * hash + Objects.hashCode(this.left);
        hash = 29 * hash + Objects.hashCode(this.right);
        return hash;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final BVNode other = (BVNode) obj;
        if (!Objects.equals(this.data, other.data)) {
            return false;
        }
        if (!Objects.equals(this.left, other.left)) {
            return false;
        }
        return Objects.equals(this.right, other.right);
    }

    /**
     *
     * @param o
     * @return
     */
    @Override
    public int compareTo(BVNode o) {
        BV nodoLeft = this.getData();
        BV nodoRight = o.getData();

        // la idea es ordenar de menor a mayor
        if (nodoLeft.getProductoDimensiones() > nodoRight.getProductoDimensiones()) {
            return -1;
        } else if (nodoLeft.getProductoDimensiones() < nodoRight.getProductoDimensiones()) {
            return 1;
        } else {
            if ((nodoLeft instanceof AABB2D) && (nodoRight instanceof AABB2D)) {
                AABB2D nl = (AABB2D) nodoLeft;
                AABB2D nr = (AABB2D) nodoRight;

                if (nl.getCentro().getX() < nr.getCentro().getX()) {
                    return 1;
                } else if (nl.getCentro().getX() > nr.getCentro().getX()) {
                    return -1;
                } else if (nl.getCentro().getY() < nr.getCentro().getY()) {
                    return 1;
                } else if (nl.getCentro().getY() > nr.getCentro().getY()) {
                    return -1;
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        }
    }

    /**
     *
     * @param box
     * @return
     */
    public static BVNode wrap(BV box) {
        return new BVNode(box);
    }

    /**
     *
     * @return
     */
    public BV unwrap() {
        return this.data;
    }

    /**
     *
     * @param nodo
     * @return
     */
    public BVNode union(BVNode nodo) {
        BV a = this.getData();
        BV b = nodo.getData();

        return new BVNode(a.unionBV(b));
    }

    /**
     *
     * @param listaBVNodes
     * @param nodo
     * @return
     */
    public static int getMinorCombinedVolumeIndice(List<BVNode> listaBVNodes, BVNode nodo) {
        int res;

        switch (listaBVNodes.size()) {
            case 0:
                return -1;
            case 1:
                return 0;
            default:
                // combino el primer BVNode con el que quiero unir
                BVNode nodoPrimero = listaBVNodes.get(0);
                BVNode nuevoNodo = nodo.addingNode(nodoPrimero);
                res = 0;

                for (int i = 1; i < listaBVNodes.size(); i++) {
                    // por cada BVNode de la lista lo combino con el a agregar
                    // comparo cual es "menor" (primero por tamaño y luego por posicion
                    // retorno ese indice
                    BVNode nodoTmp = nodo.addingNode(listaBVNodes.get(i));

                    if (nodoTmp.compareTo(nuevoNodo) == 1) {
                        res = i;
                        nuevoNodo = nodoTmp;
                    }
                }
                return res;
        }
    }

    /**
     *
     * @return
     */
    public boolean isLeaf() {
        return ((this.getLeft() == null) && (this.getRight() == null));
    }

    /**
     *
     * @param other
     * @return
     */
    public BVNode addingNode(BVNode other) {
        BV volumenUno = this.getData();
        BV volumenDos = other.getData();

        BV nuevoVolumen = volumenUno.unionBV(volumenDos);
        BVNode tmp = new BVNode(nuevoVolumen);

        if (volumenUno.getProductoDimensiones() <= volumenDos.getProductoDimensiones()) {
            tmp.left = this;
            tmp.right = other;
        } else {
            tmp.left = other;
            tmp.right = this;
        }

        return tmp;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "BVNode{" + "data=" + this.data + ", left=" + this.left + ", right=" + this.right + '}';
    }

    /**
     *
     * @param listaColisiones
     * @param objetoUno
     * @param objetoDos
     */
    public static void obtainCollidingEdges(List<Pair<Integer>> listaColisiones, BVNode objetoUno, BVNode objetoDos) {
        if (!objetoUno.intersects(objetoDos)) {
            return;
        }

        if (objetoUno.isLeaf() && objetoDos.isLeaf()) {
            collidePrimitives(listaColisiones, objetoUno, objetoDos);
        } else {
            if (descendA(objetoUno, objetoDos)) {
                obtainCollidingEdges(listaColisiones, objetoUno.getLeft(), objetoDos);
                obtainCollidingEdges(listaColisiones, objetoUno.getRight(), objetoDos);
            } else {
                obtainCollidingEdges(listaColisiones, objetoUno, objetoDos.getLeft());
                obtainCollidingEdges(listaColisiones, objetoUno, objetoDos.getRight());
            }
        }
    }

    //Descend larger Volume Rule

    /**
     *
     * @param objetoUno
     * @param objetoDos
     * @return
     */
    public static boolean descendA(BVNode objetoUno, BVNode objetoDos) {
        return (objetoDos.isLeaf()
                || (!objetoUno.isLeaf()
                && (objetoUno.getData().getProductoDimensiones() == objetoDos.getData().getProductoDimensiones())));
    }

    /**
     *
     * @param other
     * @return
     */
    public boolean intersects(BVNode other) {
        return (this.getData().intersects(other.getData()));
    }

    /**
     *
     * @param listaColisiones
     * @param objetoUno
     * @param objetoDos
     */
    public static void collidePrimitives(List<Pair<Integer>> listaColisiones, BVNode objetoUno, BVNode objetoDos) {
        if (objetoUno.isLeaf()) {
            if (objetoDos.isLeaf()) {
                AABB2D.collidePrimitives(listaColisiones, (AABB2D) objetoUno.getData(), (AABB2D) objetoDos.getData());
            } else {
                if (objetoDos.getLeft() != null) {
                    collidePrimitives(listaColisiones, objetoUno, objetoDos.getLeft());
                }
                if (objetoDos.getRight() != null) {
                    collidePrimitives(listaColisiones, objetoUno, objetoDos.getRight());
                }
            }
        } else {
            if (objetoUno.getLeft() != null) {
                collidePrimitives(listaColisiones, objetoUno.getLeft(), objetoDos);
            }
            if (objetoUno.getRight() != null) {
                collidePrimitives(listaColisiones, objetoUno.getRight(), objetoDos);
            }
        }
    }

    private boolean collidePrimitives(List<Pair<Integer>> listaColisiones, BVNode objeto) {
        if ((this == null) || (objeto == null)) {
            return false;
        } else if (this.isLeaf() && objeto.isLeaf()) {
            AABB2D.collidePrimitives(listaColisiones, (AABB2D) this.getData(), (AABB2D) objeto.getData());
            return true;
        } else {
            return (this.getLeft() == null) ? false : this.getLeft().collidePrimitives(listaColisiones, objeto)
                    || (this.getRight() == null) ? false : this.getRight().collidePrimitives(listaColisiones, objeto)
                    || (objeto.getLeft() == null) ? false : this.collidePrimitives(listaColisiones, objeto.getLeft())
                    || (objeto.getRight() == null) ? false : this.collidePrimitives(listaColisiones, objeto.getRight());
        }
    }

    /**
     *
     * @param listaNodos
     * @return
     */
    public static BVNode obtainMinimalAreaCombinadedNode(List<BVNode> listaNodos) {
        BVNode nodoUno, nodoDos, nodoNuevo;

        sortNodesList(listaNodos);

        // obtengo el nodo con menor volumen (debe ser el primero de la lista)
        // hago una copia local
        nodoUno = new BVNode(listaNodos.get(0));

        // elimino el nodo de la lista
        listaNodos.remove(0);

        // obtengo el segundo nodo de la lista
        int idx = getMinorCombinedVolumeIndice(listaNodos, nodoUno);

        // hago una copia local
        nodoDos = new BVNode(listaNodos.get(idx));

        // elimino el nodo
        listaNodos.remove(idx);

        // genero un nuevo BVNode resultante como la suma de AMBOS
        nodoNuevo = nodoUno.addingNode(nodoDos);

        // coloco los 2 nodos sacados como hijos del nodoNuevo
        if (nodoUno.getData().getProductoDimensiones() <= nodoDos.getData().getProductoDimensiones()) {
            nodoNuevo.setLeft(nodoUno);
            nodoNuevo.setRight(nodoDos);
        } else {
            nodoNuevo.setLeft(nodoDos);
            nodoNuevo.setRight(nodoUno);
        }

        return nodoNuevo;
    }

    /**
     *
     * @param listaBVNodes
     */
    public static void sortNodesList(List<BVNode> listaBVNodes) {
        sort(listaBVNodes, new BVNodeComparator());
    }
}
