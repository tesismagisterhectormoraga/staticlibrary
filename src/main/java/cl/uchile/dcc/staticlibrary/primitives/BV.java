package cl.uchile.dcc.staticlibrary.primitives;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author hmoraga
 *
 */
public interface BV extends Cloneable {

    /**
     *
     * @param dimension
     * @return
     */
    public abstract double getMinimum(int dimension);

    /**
     *
     * @param dimension
     * @return
     */
    public abstract double getMaximum(int dimension);

    /**
     *
     * @return
     */
    public abstract double getProductoDimensiones();

    /**
     *
     * @param other
     * @return
     */
    public abstract BV unionBV(BV other);

    /**
     *
     * @param o
     * @return
     */
    public abstract BV intersectionBV(BV o);

    /**
     *
     * @param data
     * @return
     */
    public abstract boolean intersects(BV data);

    /**
     *
     * @param volume
     * @param dim
     * @return
     */
    public int compareTo(BV volume, int dim);
}
