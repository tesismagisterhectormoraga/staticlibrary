/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.primitives;

import cl.uchile.dcc.staticlibrary.comparators.PairIntegerComparator;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeSet;

/**
 *
 * @author hmoraga
 */
public class PairList {
    private HashMap<Integer, TreeSet<Pair<Integer>>> listaDePares;

    /**
     *
     */
    public PairList() {
        this.listaDePares = new HashMap<>();
    }

    /**
     *
     * @param par
     * @return
     */
    public boolean addPair(Pair<Integer> par) {
        if (!this.listaDePares.containsKey(par.getFirst())) {
            this.listaDePares.put(par.getFirst(), new TreeSet<>(new PairIntegerComparator()));
            this.listaDePares.get(par.getFirst()).add(par);
            return true;
        } else {
            return this.listaDePares.get(par.getFirst()).add(par);
        }
    }

    /**
     *
     */
    public void showList() {
        Iterator<Map.Entry<Integer, TreeSet<Pair<Integer>>>> it = this.listaDePares.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry<Integer, TreeSet<Pair<Integer>>> entrada = it.next();
            TreeSet<Pair<Integer>> value = entrada.getValue();

            if (!value.isEmpty()) {
                value.forEach((pair) -> {
                    out.print(pair.toString() + " ");
                });
                out.println("");
            }
        }
    }

    /**
     *
     * @param par
     * @return
     */
    public boolean pairExists(Pair<Integer> par) {
        boolean resultado = false;

        if (par != null) {
            if (this.listaDePares.containsKey(par.getFirst())) {
                TreeSet<Pair<Integer>> value = this.listaDePares.get(par.getFirst());
                resultado = value.contains(par);
            }
        }
        return resultado;
    }

    /**
     *
     * @param par
     * @return
     */
    public boolean deletePair(Pair<Integer> par) {
        if (this.listaDePares.containsKey(par.getFirst())) {
            TreeSet<Pair<Integer>> value = this.listaDePares.get(par.getFirst());
            return value.remove(par);
        }
        return false;
    }

    /**
     *
     * @return
     */
    public boolean isEmpty() {
        return this.listaDePares.isEmpty();
    }

    /**
     *
     * @return
     */
    public int size() {
        int contador = 0;

        for (Map.Entry<Integer, TreeSet<Pair<Integer>>> entry : this.listaDePares.entrySet()) {
            TreeSet<Pair<Integer>> value = entry.getValue();

            contador = contador + value.size();
        }

        return contador;
    }

    /**
     *
     * @param listaParcialY
     */
    public void retainAll(PairList listaParcialY) {
        Iterator<Map.Entry<Integer, TreeSet<Pair<Integer>>>> it = this.listaDePares.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry<Integer, TreeSet<Pair<Integer>>> entrada = it.next();
            Integer key = entrada.getKey();
            TreeSet<Pair<Integer>> value = entrada.getValue();

            if (!listaParcialY.keyExists(key)) {
                it.remove();
            } else {
                Iterator<Pair<Integer>> it2 = value.iterator();

                while (it2.hasNext()) {
                    PairInteger par = (PairInteger) it2.next();

                    if (!listaParcialY.pairExists(par)) {
                        it2.remove();
                    }
                }
            }
        }
    }

    /**
     *
     * @param key
     * @return
     */
    public boolean keyExists(Integer key) {
        return this.listaDePares.containsKey(key);
    }

    /**
     *
     * @return
     */
    public List<Pair<Integer>> obtainList() {
        List<Pair<Integer>> listaSalida = new ArrayList<>();

        this.listaDePares.entrySet().stream().map((entry) -> {
            Integer key = entry.getKey();
            return entry;
        }).map((entry) -> entry.getValue()).filter((value) -> (!value.isEmpty())).forEachOrdered((value) -> {
            listaSalida.addAll(value);
        });

        return listaSalida;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 43 * hash + Objects.hashCode(this.listaDePares);
        return hash;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final PairList other = (PairList) obj;
        return Objects.equals(this.listaDePares, other.listaDePares);
    }
}
