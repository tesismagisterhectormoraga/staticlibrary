/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.primitives;

/**
 *
 * @author hmoraga
 */
public interface Edge{
    double getDistance();
    boolean intersects(Edge arista);
    void add(Point pto);
    Edge axisProjectionIntersection(Edge other, int dimension);
    public double getMinValue(int dimension);
    public double getMaxValue(int dimension);
}
