/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.primitives;

import static java.lang.Math.max;
import static java.lang.Math.min;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author hmoraga
 */
public class PairInteger implements Pair<Integer>, Comparable<PairInteger>{
    private Integer x, y;
    
    /**
     *
     * @param x
     * @param y
     */
    public PairInteger(Integer x, Integer y) {
        this.x = min(x, y);
        this.y = max(x, y);
    }

    private PairInteger(Pair<Integer> otro) {
        this.x = otro.getFirst();
        this.y = otro.getSecond();
    }
    
    /**
     *
     * @return
     */
    @Override
    public PairInteger inverted() {
        return new PairInteger(this.y, this.x);
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + Objects.hashCode(this.x);
        hash = 23 * hash + Objects.hashCode(this.y);
        return hash;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final PairInteger other = (PairInteger) obj;
        if (!Objects.equals(this.x, other.x)) {
            return false;
        }
        return Objects.equals(this.y, other.y);
    }

    /**
     *
     * @return
     */
    @Override
    public Integer getFirst() {
        return this.x;
    }
    
    /**
     *
     * @return
     */
    @Override
    public Integer getSecond() {
        return this.y;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "(" + this.x + "," + this.y + ')';
    }

    /**
     *
     * @param first
     */
    @Override
    public void setFirst(Integer first) {
        this.x = first;
    }

    /**
     *
     * @param second
     */
    @Override
    public void setSecond(Integer second) {
        this.y = second;
    }

    /**
     *
     * @param listaPares
     * @return
     */
    @Override
    public boolean exists(List<Pair<Integer>> listaPares) {
        return listaPares.contains(this);
    }

    /**
     *
     * @param otro
     * @return
     */
    @Override
    public Pair<Integer> intersection(Pair<Integer> otro) {
        if ((this.x < otro.getFirst() && otro.getFirst() < this.y) && 
                (otro.getSecond() >= this.y)) {
            return new PairInteger(otro.getFirst(), this.y);
        } else if ((otro.getFirst()<= this.x) && (this.y <= otro.getSecond())) {
            return this;
        } else if ((this.x <= otro.getFirst()) && (otro.getSecond() <= this.y)) {
            return otro;
        } else if ((otro.getFirst() <= this.x) && (this.x <= otro.getSecond()) &&
                (otro.getSecond() <= this.y)) {
            return new PairInteger(x, otro.getSecond());
        }
        return null;
    }
    
    @Override
    public boolean intersects(Pair<Integer> otro){
        return !((this.x > otro.getSecond()) || (this.y < otro.getFirst()));
    }

    @Override
    public int compareTo(PairInteger t) {
        if (!Objects.equals(this.x, t.getFirst())) {
            return (this.x - t.getFirst());
        } else {
            return (this.y - t.getSecond());
        }
    }
}
