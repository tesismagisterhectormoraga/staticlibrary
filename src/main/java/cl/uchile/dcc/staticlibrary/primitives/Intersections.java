package cl.uchile.dcc.staticlibrary.primitives;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import cl.uchile.dcc.staticlibrary.comparators.PairIntegerComparator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.TreeSet;

/**
 *
 * @author hmoraga
 */
public class Intersections extends TreeSet<Pair<Integer>> {

    /**
     *
     */
    public Intersections() {
        super(new PairIntegerComparator());
    }

    /**
     *
     * @param c
     */
    public Intersections(Collection<? extends PairInteger> c) {
        super(new PairIntegerComparator());
        super.addAll(c);
    }

    public Intersections(Intersections listaMenor) {
        super(new PairIntegerComparator());
        super.addAll(listaMenor);
    }

    /**
     *
     * @return
     */
    public List<Pair<Integer>> getCollisionsList() {
        return (this.isEmpty()) ? null : new ArrayList<>(this);
    }

    /**
     *
     * @param e
     * @return
     */
    @Override
    public boolean add(Pair<Integer> e) {
        return super.add(e);
    }

    /*@Override
    public String toString() {
        String s = "[";

        if (!isEmpty()) {
            for (Pair<Integer> par : this) {
                s = s.concat(par.toString());
                s = s.concat(", ");
            }
            s = s.substring(0, s.length() - 2);
        }
        s = s.concat("]");

        return s;
    }*/
}
