package cl.uchile.dcc.staticlibrary.primitives;

import cl.uchile.dcc.staticlibrary.comparators.AABB2DComparator;
import cl.uchile.dcc.staticlibrary.comparators.AABB2DComparatorArea;
import cl.uchile.dcc.staticlibrary.comparators.AABB2DComparatorXAxisByCenter;
import cl.uchile.dcc.staticlibrary.comparators.AABB2DComparatorYAxisByCenter;
import static java.lang.Double.NEGATIVE_INFINITY;
import static java.lang.Double.POSITIVE_INFINITY;
import static java.lang.Math.abs;
import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.lang.Math.pow;
import java.util.ArrayList;
import static java.util.Collections.sort;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 * Caja en 2D que es subclase de BV (bounding Volume), e implementa la
 * interfaz Comparable
 *
 * @author hmoraga
 */
public class AABB2D implements BV, Comparable<AABB2D> {
    private Point2D p; //p corresponde al punto inferior mas a la izq
    private Point2D q; //q corresponde al punto superior mas a la derecha
    private Primitive arista; // elemento que es envuelto por el AABB2D

    /**
     * Constructor por defecto, centrado en (0,0) y con dimensiones alto=0 y
     * ancho=0, que no contiene ningún elemento.
     */
    public AABB2D() {
        this.p = new Point2D();
        this.q = new Point2D();
        this.arista = null;
    }

    /**
     * Constructor de la caja, con una lista de puntos
     * <P>
     * De la lista de entrada se busca la menor coordenada en X y en Y,
     * generando el punto p, y la mayor coordenada en X y en Y, generando el
     * punto q.
     *
     * @param listaPuntos lista de Point2D
     */
    public AABB2D(List<Point2D> listaPuntos) {
        if (listaPuntos.isEmpty()) {
            this.p = new Point2D();
            this.q = new Point2D();
        } else {
            double minX = listaPuntos.get(0).getX(), minY = listaPuntos.get(0).getY();
            double maxX = listaPuntos.get(0).getX(), maxY = listaPuntos.get(0).getY();

            //busco las dimensiones maximo y minimo de toda la lista de puntos ingresada
            for (Point2D listaPunto : listaPuntos) {
                if (minX > listaPunto.getCoords(0)) {
                    minX = listaPunto.getCoords(0);
                }
                if (minY > listaPunto.getCoords(1)) {
                    minY = listaPunto.getCoords(1);
                }
                if (maxX < listaPunto.getCoords(0)) {
                    maxX = listaPunto.getCoords(0);
                }
                if (maxY < listaPunto.getCoords(1)) {
                    maxY = listaPunto.getCoords(1);
                }
            }

            this.p = new Point2D(minX, minY);
            this.q = new Point2D(maxX, maxY);
        }
        this.arista = null;
    }

    /**
     * Constructor usando un punto central y las dimensiones de largo y de
     * ancho.
     *
     * @param center Point2D del centro de la caja
     * @param largoX largo en la dimension X
     * @param largoY largo en la dimension Y
     */
    public AABB2D(Point2D center, double largoX, double largoY) {
        assert (largoX >= 0);
        assert (largoY >= 0);

        this.p = new Point2D(center.getX() - largoX / 2, center.getY() - largoY / 2);
        this.q = new Point2D(center.getX() + largoX / 2, center.getY() + largoY / 2);
        this.arista = null;
    }
    
    /**
     *
     * @param minimo
     * @param maximo
     */
    public AABB2D(Point2D minimo, Point2D maximo){
        this.p = minimo;
        this.q = maximo;
        this.arista = null;
    }

    /**
     * Constructor usando una arista que será almacenada como una primitiva y un
     * índice asociado a la arista
     *
     * @param arista objeto de la clase Edge2D asociado a la caja
     * @param objIndex índice asociado a la arista
     */
    public AABB2D(Edge2D arista, int objIndex) {
        Point2D[] puntos = arista.getPoints();
        this.p = new Point2D(min(puntos[0].getX(), puntos[1].getX()),
                min(puntos[0].getY(), puntos[1].getY()));
        this.q = new Point2D(max(puntos[0].getX(), puntos[1].getX()),
                max(puntos[0].getY(), puntos[1].getY()));
        this.arista = new Primitive2D(arista, objIndex);
    }

    /**
     * copy constructor
     *
     * @param other any other AABB2D
     */
    public AABB2D(AABB2D other) {
        this.p = new Point2D(other.p.getX(), other.p.getY());
        this.q = new Point2D(other.q.getX(), other.q.getY());

        if (other.arista != null) {
            Edge2D aristaTemp = (Edge2D) other.arista.getEdge();
            this.arista = new Primitive2D(new Edge2D(aristaTemp.getPoints()[0], aristaTemp.getPoints()[1]), other.arista.getIndex());
        } else {
            this.arista = null;
        }
    }

    /**
     * Me devuelve el Point2D del centro de la caja.
     *
     * @return Point2D del centro de la caja
     */
    public Point2D getCentro() {
        return new Point2D((this.p.getX() + this.q.getX()) / 2, (this.p.getY() + this.q.getY()) / 2);
    }

    /**
     * Devuelve la primitiva que existe por debajo, si no existe devuelve null
     *
     * @return primitiva que se encuentra en la caja, sólo para nodos que son
     * hojas
     */
    public Primitive getPrimitive() {
        return (this.arista == null) ? null : this.arista;
    }

    /**
     * Devuelve la arista que existe por debajo, si no existe devuelve null
     *
     * @return arista2D que se encuentra en la caja, sólo para nodos que son
     * hojas
     */
    public Edge2D getEdge() {
        return (this.arista == null) ? null : (Edge2D) this.arista.getEdge();
    }

    /**
     *
     * @param dimension
     * @return
     */
    public Pair<Double> getProjection(int dimension) {
        return (dimension == 0) ? new PairDouble(this.p.getX(), this.q.getX())
                : new PairDouble(this.p.getY(),this.q.getY());
    }

    /**
     * retorna la nueva caja que corresponde a la intersección entre esta caja y
     * una ingresada como parámetro. Si ambas cajas son disjuntas, se retornará
     * null.
     *
     * @param o caja con la que se calculará la intersección
     * @return nueva caja que corresponde a la intersección o, null en caso de
     * que las cajas originales sean disjuntas.
     */
    @Override
    public AABB2D intersectionBV(BV o) {
        //double minX = 0, maxX = 0, minY = 0, maxY = 0;

        if (this == null || o == null || !this.intersects(o)) {
            return null;
        } else {
            AABB2D other = (AABB2D) o;

            // Obtengo los puntos p y q de cada caja
            // y con esos construyo las proyecciones
            Pair<Double> thisProjX = this.getProjection(0);
            Pair<Double> thisProjY = this.getProjection(1);
            Pair<Double> otherProjX = other.getProjection(0);
            Pair<Double> otherProjY = other.getProjection(1);

            // reviso si los intervalos son disjuntos (en X y en Y)
            boolean interseccionEnX=true, interseccionEnY=true;
             
            if (((thisProjX.getFirst() < otherProjX.getFirst()) && (thisProjX.getSecond() < otherProjX.getFirst()))
                || ((otherProjX.getFirst() < thisProjX.getFirst()) && (otherProjX.getSecond() < thisProjX.getFirst()))) {
                interseccionEnX=false;
            }
            
            if (((thisProjY.getFirst() < otherProjY.getFirst()) && (thisProjY.getSecond() < otherProjX.getFirst()))
                || ((otherProjX.getFirst() < thisProjX.getFirst()) && (otherProjY.getSecond() < thisProjX.getFirst()))) {
                interseccionEnY=false;
            }
            
            if (!(interseccionEnX && interseccionEnY)) {
                return null;
            }

            Pair<Double> proyeccionEnX = thisProjX.intersection(otherProjX);
            Pair<Double> proyeccionEnY = thisProjY.intersection(otherProjY);
            
            List<Point2D> listaTemp = new ArrayList<>(2);
            listaTemp.add(new Point2D(proyeccionEnX.getFirst(), proyeccionEnY.getFirst()));
            listaTemp.add(new Point2D(proyeccionEnX.getSecond(), proyeccionEnY.getSecond()));

            return new AABB2D(listaTemp);
        }
    }

    /**
     * Devuelve si efectivamente dos cajas se intersectan.
     *
     * @param otro caja con la que comparar si existe intersección.
     * @return
     */
    public boolean intersects(AABB2D otro) {
        return !((otro==null) || (this==null) || 
                (this.p.getX()-otro.q.getX() > 0) ||
                (otro.p.getX()-this.q.getX() > 0) ||
                (this.p.getY()-otro.q.getY() > 0) ||
                (otro.p.getY()-this.q.getY() > 0));
    }

    /**
     *
     * @param box
     * @return
     */
    public boolean isInside(AABB2D box) {
        if ((this == null) || (box == null)) {
            return false;
        }

        List<Point2D> listaPuntos = this.getPoints();

        return (listaPuntos.get(0).isInside(box)
                && listaPuntos.get(1).isInside(box)
                && listaPuntos.get(2).isInside(box)
                && listaPuntos.get(3).isInside(box));
    }

    /**
     *
     * @return
     */
    @Override
    public double getProductoDimensiones() {
        return this.getArea();
    }

    /**
     *
     * @param dimension
     * @return
     */
    @Override
    public double getMinimum(int dimension) {
        assert (dimension >= 0 && dimension <= 2);

        switch (dimension) {
            case 0:
                return this.getMinX();
            case 1:
                return this.getMinY();
            default:
                return NEGATIVE_INFINITY;
        }
    }

    /**
     *
     * @param dimension
     * @return
     */
    @Override
    public double getMaximum(int dimension) {
        assert (dimension >= 0 && dimension <= 2);

        switch (dimension) {
            case 0:
                return this.getMaxX();
            case 1:
                return this.getMaxY();
            default:
                return POSITIVE_INFINITY;
        }
    }

    /**
     *
     * @return
     */
    public double getMinX() {
        return this.p.getX();
    }

    /**
     *
     * @return
     */
    public double getMaxX() {
        return this.q.getX();
    }

    /**
     *
     * @return
     */
    public double getMinY() {
        return this.p.getY();
    }

    /**
     *
     * @return
     */
    public double getMaxY() {
        return this.q.getY();
    }

    /**
     *
     * @return
     */
    public double getArea() {
        double largoX = abs(this.p.getX() - this.q.getX());
        double largoY = abs(this.p.getY() - this.q.getY());
        return (largoX * largoY);
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "AABB2D{" + "p=" + this.p + ", q=" + this.q + ", arista=" + this.arista + '}';
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.p);
        hash = 79 * hash + Objects.hashCode(this.q);
        hash = 79 * hash + Objects.hashCode(this.arista);
        return hash;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final AABB2D other = (AABB2D) obj;
        if (!Objects.equals(this.p, other.p)) {
            return false;
        }
        if (!Objects.equals(this.q, other.q)) {
            return false;
        }
        return Objects.equals(this.arista, other.arista);
    }

    /**
     *
     * @return
     */
    public boolean isLeaf() {
        return (this.arista != null) ? (this.arista.getIndex() != -1) : false;
    }

    /**
     *
     * @return
     */
    public int getIndex() {
        return (this.arista != null) ? this.arista.getIndex() : -1;
    }

    /**
     *
     * @return
     */
    public List<Point2D> getPoints() {
        List<Point2D> listPoints = new ArrayList<>();

        //order (minx, miny) to (minx, maxy) right hand rule
        listPoints.add(this.p);
        listPoints.add(new Point2D(this.q.getX(), this.p.getY()));
        listPoints.add(this.q);
        listPoints.add(new Point2D(this.p.getX(), this.q.getY()));

        return listPoints;
    }

    /**
     *
     * @param listaCajas
     * @return
     */
    public static Pair<Double> getMean(List<? extends BV> listaCajas) {
        assert (listaCajas.size() > 0);
        double resX = 0, resY = 0;

        Iterator<? extends BV> it = listaCajas.iterator();

        while (it.hasNext()) {
            BV box = it.next();
            Point2D pto = ((AABB2D) box).getCentro();

            double ptoX = pto.getX();
            double ptoY = pto.getY();

            resX += ptoX;
            resY += ptoY;
        }

        resX /= listaCajas.size();
        resY /= listaCajas.size();

        return new PairDouble(resX, resY);
    }

    /**
     *
     * @param listaCajas
     * @return
     */
    public static Pair<Double> getVariance(List<? extends BV> listaCajas) {
        assert (listaCajas.size() > 0);

        Pair<Double> mean = getMean(listaCajas);
        double resX = 0, resY = 0;

        Iterator<? extends BV> it = listaCajas.iterator();

        while (it.hasNext()) {
            BV box = it.next();
            Point2D pto = ((AABB2D) box).getCentro();

            double ptoX = pto.getX();
            double ptoY = pto.getY();

            resX += pow(ptoX, 2.0);
            resY += pow(ptoY, 2.0);
        }

        resX /= listaCajas.size();
        resY /= listaCajas.size();

        resX -= pow(mean.getFirst(), 2.0);
        resY -= pow(mean.getSecond(), 2.0);

        return new PairDouble(resX, resY);
    }

    /**
     *
     * @param listaColisiones
     * @param objetoUno
     * @param objetoDos
     */
    public static void collidePrimitives(List<Pair<Integer>> listaColisiones, AABB2D objetoUno, AABB2D objetoDos) {
        if (objetoUno.intersects(objetoDos)) {
            Edge p = objetoUno.getEdge();
            Edge q = objetoDos.getEdge();

            if (p != null && q != null) {
                if (p.intersects(q)) {
                    listaColisiones.add(new PairInteger(objetoUno.getIndex(), objetoDos.getIndex()));
                }
            }
        }
    }

    /**
     *
     * @param listaCajas
     */
    public static void sortFunction(List<AABB2D> listaCajas) {
        Pair<Double> varianzas = getVariance(listaCajas);

        if (varianzas.getFirst() > varianzas.getSecond()) {
            sort(listaCajas, new AABB2DComparatorXAxisByCenter());
        } else if (varianzas.getFirst() < varianzas.getSecond()) {
            sort(listaCajas, new AABB2DComparatorYAxisByCenter());
        } else {
            sort(listaCajas, new AABB2DComparatorArea());
        }
    }

    /**
     *
     * @param other
     * @return
     */
    @Override
    public AABB2D unionBV(BV other) {
        AABB2D o = (AABB2D) other;

        if (o != null) {
            double minimumX = min(this.getMinX(), o.getMinX());
            double minimumY = min(this.getMinY(), o.getMinY());
            double maximumX = max(this.getMaxX(), o.getMaxX());
            double maximumY = max(this.getMaxY(), o.getMaxY());

            Point2D center = new Point2D((minimumX + maximumX) / 2, (minimumY + maximumY) / 2);

            return new AABB2D(center, (maximumX - minimumX), (maximumY - minimumY));
        } else {
            return null;
        }
    }

    /**
     *
     * @param data
     * @return
     */
    @Override
    public boolean intersects(BV data) {
        return this.intersects((AABB2D) data);
    }

    /**
     *
     * @param dimension
     * @return
     */
    /*public PairElement getElements(int dimension) {
        if (getIndex() == -1) {
            switch (dimension) {
                case 0:
                    return new PairElement(new Element(getMinX(), false), new Element(getMaxX(), true));
                case 1:
                    return new PairElement(new Element(getMinY(), false), new Element(getMaxY(), true));
                default:
            }
        } else {
            switch (dimension) {
                case 0:
                    return new PairElement(new Element(getMinX(), getIndex(), false), new Element(getMaxX(), getIndex(), true));
                case 1:
                    return new PairElement(new Element(getMinY(), getIndex(), false), new Element(getMaxY(), getIndex(), true));
                default:
            }
        }
        return null;
    } */

    /**
     *
     * @param listaCajas
     * @param dimension
     * @return
     */
    /*public static List<Pair<Integer>> obtainerPairsList(List<AABB2D> listaCajas, int dimension) {
        List<Element> listaElementos = new ArrayList<>();

        for (int i = 0; i < listaCajas.size(); i++) {
            AABB2D caja = listaCajas.get(i);
            PairElement par = caja.getElements(dimension);
            par.getFirst().setId(i);
            par.getSecond().setId(i);
            listaElementos.add(par.getFirst());
            listaElementos.add(par.getSecond());
        }

        Collections.sort(listaElementos, new ElementComparator());

        return Element.obtainPairsList(listaElementos);
    }*/

    /**
     *
     * @param pto
     */
    public void move(Point2D pto) {
        if (this.arista != null) {
            this.arista.getEdge().add(pto);
        }

        this.p.add(pto);
        this.q.add(pto);
    }

    /**
     *
     * @return
     */
    public double getLargoX() {
        return abs(this.q.getX() - this.p.getX());
    }

    /**
     *
     * @return
     */
    public double getLargoY() {
        return abs(this.q.getY() - this.p.getY());
    }

    /**
     * Comparador "natural" entre AABB2D, me compara en base al punto mas a la
     * izquierda y mas abajo en ambas cajas.
     *
     * @param o caja con la que comparar
     * @return
     */
    @Override
    public int compareTo(AABB2D o) {
        return new AABB2DComparator().compare(this, o);
    }

    /**
     *
     * @param o
     * @param dimension
     * @return
     */
    public int compareTo(AABB2D o, int dimension) {
        return new AABB2DComparator(dimension).compare(this, o);
    }

    /**
     *
     * @param o
     * @param dimension
     * @return
     */
    @Override
    public int compareTo(BV o, int dimension) {
        return new AABB2DComparator(dimension).compare(this, (AABB2D) o);
    }
    
    /**
     *
     * @param pto
     * @return
     */
    public boolean contains(Point2D pto){
        if (pto!=null) {
            return (this.p.getX()<=pto.getX()) &&
                    (pto.getX()<= this.q.getX()) &&
                    (this.p.getY()<=pto.getY()) &&
                    (pto.getY()<= this.q.getY());
        } else {
            return false;
        }
    }
}
