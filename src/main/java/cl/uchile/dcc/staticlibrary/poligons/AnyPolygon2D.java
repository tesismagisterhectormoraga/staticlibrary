/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.poligons;

import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import java.util.List;

/**
 *
 * @author hmoraga
 */
public class AnyPolygon2D extends Polygon2D {

    /**
     *
     * @param listaPuntos
     * @param rotate
     */
    public AnyPolygon2D(List<Point2D> listaPuntos, boolean rotate) {
        super(listaPuntos, rotate);
    }

    /**
     *
     * @param verticesNumber
     * @param rotate
     */
    public AnyPolygon2D(int verticesNumber, boolean rotate) {
        super(verticesNumber, rotate);
    }

    /**
     *
     * @param areaTotal
     * @return
     */
    @Override
    public double getScale(double areaTotal) {
        return 1.0;
    }

}
