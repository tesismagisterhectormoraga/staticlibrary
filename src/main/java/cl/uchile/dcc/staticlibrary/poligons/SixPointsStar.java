package cl.uchile.dcc.staticlibrary.poligons;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import static java.lang.Math.sqrt;
import static java.util.Arrays.asList;

/**
 * Clase Estrella que hereda de Polygon2D.
 *
 * @author hmoraga
 */
public class SixPointsStar extends Polygon2D {

    /**
     *
     * @param rotate
     */
    public SixPointsStar(boolean rotate) {
        super(asList(new Point2D[]{
            new Point2D(sqrt(3) / 3, 0.0),
            new Point2D(sqrt(3) / 2, 0.5),
            new Point2D(sqrt(3) / 6, 0.5),
            new Point2D(0.0, 1.0),
            new Point2D(-sqrt(3) / 6, 0.5),
            new Point2D(-sqrt(3) / 2, 0.5),
            new Point2D(-sqrt(3) / 3, 0.0),
            new Point2D(-sqrt(3) / 2, -0.5),
            new Point2D(-sqrt(3) / 6, -0.5),
            new Point2D(0.0, -1.0),
            new Point2D(sqrt(3) / 6, -0.5),
            new Point2D(sqrt(3) / 2, -0.5)}), rotate);
    }

    /**
     * Me genera una estrella regular de 6 puntas.
     *
     * @param areaFinal
     * @param rotate si se hace una rotacion al azar o no
     */
    public SixPointsStar(double areaFinal, boolean rotate) {
        this(rotate);
        super.amplificar(this.getScale(areaFinal));
    }

    /**
     *
     * @param areaFinal
     * @return
     */
    @Override
    public final double getScale(double areaFinal) {
        return sqrt(areaFinal / sqrt(3));
    }
}
