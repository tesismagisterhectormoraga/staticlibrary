package cl.uchile.dcc.staticlibrary.poligons;

import static java.lang.Math.PI;
import static java.lang.Math.atan2;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;
import static java.util.Arrays.asList;

/**
 *
 * @author hmoraga
 */
public class Rectangle extends Cuadrilateral {

    /**
     * Rectangulo inscrito en una circunferencia de radio 1
     *
     * @param ancho proporcion para el ancho
     * @param alto proporcion para el alto
     * @param rotate si se rota la figura al azar o no
     */
    public Rectangle(double ancho, double alto, boolean rotate) throws NumberFormatException {
        super(asList(new Double[]{
            atan2(alto, ancho),
            PI - atan2(alto, ancho),
            PI + atan2(alto, ancho),
            2 * PI - atan2(alto, ancho)}),
                rotate);
    }

    /**
     *
     * @param ancho
     * @param alto
     * @param areaTotal
     * @param rotate
     * @throws NumberFormatException
     */
    public Rectangle(double ancho, double alto, double areaTotal, boolean rotate) throws NumberFormatException {
        this(ancho, alto, rotate);
        super.amplificar(this.getScale(areaTotal));
    }

    /**
     *
     * @param areaTotal
     * @return
     */
    @Override
    public final double getScale(double areaTotal) {
        double alpha = atan2(this.listaPuntos.get(0).getY(), this.listaPuntos.get(0).getX());
        double area0 = 2 * sin(2 * alpha);
        return sqrt(areaTotal / area0);
    }
}
