package cl.uchile.dcc.staticlibrary.poligons;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import static java.lang.Math.sqrt;
import static java.util.Arrays.asList;

/**
 *
 * @author hmoraga
 */
public final class Boomerang extends Polygon2D {

    /**
     * Constructor del boomerang de lado 1
     *
     * @param rotate si se rota o no al azar el poligono
     */
    public Boomerang(boolean rotate) {
        super(asList(new Point2D[]{new Point2D(0, 0), new Point2D(sqrt(3) / 2, -0.5),
            new Point2D(0, 1), new Point2D(-sqrt(3) / 2, -0.5)}), rotate);
    }

    /**
     * Constructor del boomerang de lado 1
     *
     * @param areaFinal
     * @param rotate si se rota o no al azar el poligono
     */
    public Boomerang(double areaFinal, boolean rotate) {
        super(asList(new Point2D[]{new Point2D(0, 0), new Point2D(sqrt(3) / 2, -0.5),
            new Point2D(0, 1), new Point2D(-sqrt(3) / 2, -0.5)}), rotate);
        double k = this.getScale(areaFinal);
        super.amplificar(k);
    }

    /**
     *
     * @param areaFinal
     * @return
     */
    @Override
    public double getScale(double areaFinal) {
        double areaFiguraCirculoUnitario = 0.5 * sqrt(3);

        return sqrt(areaFinal / areaFiguraCirculoUnitario);
    }
}
