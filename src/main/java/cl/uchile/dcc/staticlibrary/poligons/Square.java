package cl.uchile.dcc.staticlibrary.poligons;

import static java.lang.Math.sqrt;

/**
 *
 * @author hmoraga
 */
public final class Square extends Polygon2D {

    /**
     * Me genera un cuadrado de area final dada
     *
     * @param areaFinal area final del cuadrado
     * @param rotate si se rota o no un valor aleatorio
     */
    public Square(double areaFinal, boolean rotate) {
        this(rotate);
        double scale = this.getScale(areaFinal);
        super.amplificar(scale);
    }

    /**
     * Me genera un cuadrado inscrito en una circunferencia de lado 1
     *
     * @param rotate si se rota o no un valor aleatorio
     */
    public Square(boolean rotate) {
        super(4, rotate);
    }

    /**
     *
     * @param areaFinal
     * @return
     */
    @Override
    public double getScale(double areaFinal) {
        return sqrt(areaFinal / 2);
    }
}
