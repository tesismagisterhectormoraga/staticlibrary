/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.poligons;

/**
 *
 * @author hmoraga
 */
public enum PolygonType {

    /**
     *
     */
    TRIANGULO, 

    /**
     *
     */
    CUADRADO, 

    /**
     *
     */
    PENTAGONO, 

    /**
     *
     */
    HEXAGONO, 

    /**
     *
     */
    ESTRELLA4, 

    /**
     *
     */
    ESTRELLA6, 

    /**
     *
     */
    BOOMERANG; // no incluyo rectangulo ya que tiene problemas
}
