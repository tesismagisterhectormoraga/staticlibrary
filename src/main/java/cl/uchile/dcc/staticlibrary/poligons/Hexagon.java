package cl.uchile.dcc.staticlibrary.poligons;

import static java.lang.Math.PI;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author hmoraga
 */
public final class Hexagon extends Polygon2D {

    /**
     * Me genera un hexagono de area final dada
     *
     * @param areaFinal area final del pentagono
     * @param rotate si se rota o no un valor aleatorio
     */
    public Hexagon(double areaFinal, boolean rotate) {
        this(rotate);
        super.amplificar(this.getScale(areaFinal));
    }

    /**
     * Me genera un pentagono inscrito en una circunferencia de lado 1
     *
     * @param rotate si se rota o no un valor aleatorio
     */
    public Hexagon(boolean rotate) {
        super(6, rotate);
    }

    /**
     *
     * @param areaFinal
     * @return
     */
    @Override
    public double getScale(double areaFinal) {
        double area0 = 3 * sin(PI / 3);

        return sqrt(areaFinal / area0);
    }
}
