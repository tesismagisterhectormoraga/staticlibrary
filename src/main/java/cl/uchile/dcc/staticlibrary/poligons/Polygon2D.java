package cl.uchile.dcc.staticlibrary.poligons;

import cl.uchile.dcc.staticlibrary.primitives.AABB2D;
import cl.uchile.dcc.staticlibrary.primitives.Edge2D;
import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import static java.lang.Math.PI;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.System.currentTimeMillis;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author hmoraga
 */
public abstract class Polygon2D {

    /**
     *
     */
    protected List<Point2D> listaPuntos;

    /**
     * Constructor de un poligono regular en 2D, inscrito en una circunferencia
     * de lado 1
     *
     * @param listaPuntos lista de puntos en orden
     * @param rotate verdadero si se desea rotar el poligono de forma aleatoria
     */
    public Polygon2D(List<Point2D> listaPuntos, boolean rotate) {
        assert (listaPuntos.size() >= 3);
        this.listaPuntos = new ArrayList<>(listaPuntos);

        Random rnd = new Random(currentTimeMillis());

        double rndAngle = (rotate) ? 2 * PI * rnd.nextDouble() : 0.0;

        this.rotar(rndAngle);
    }

    /**
     * Constructor de un n-agono regular en 2D
     *
     * @param verticesNumber cantidad de vertices de un poligono convexo regular
     * @param rotate verdadero si se desea rotar el poligono de forma aleatoria
     */
    public Polygon2D(int verticesNumber, boolean rotate) {
        assert (verticesNumber >= 3);

        this.listaPuntos = new ArrayList<>(verticesNumber);
        Random rnd = new Random(currentTimeMillis());

        double rndAngle = (rotate) ? 2 * PI * rnd.nextDouble() : 0.0;
        double centralAngle = 2 * PI / verticesNumber;

        for (int k = 0; k < verticesNumber; k++) {
            Point2D p = new Point2D(cos(centralAngle * k), sin(centralAngle * k));
            this.listaPuntos.add(p);
        }

        this.rotar(rndAngle);
    }

    /**
     *
     * @return
     */
    public List<Point2D> getVertices() {
        return this.listaPuntos;
    }

    /**
     *
     * @param x
     * @param y
     */
    public void desplazar(double x, double y) {
        this.listaPuntos.stream().forEach((vertice) -> {
            vertice.add(x, y);
        });
    }

    /**
     *
     * @param p
     */
    public void desplazar(Point2D p) {
        this.listaPuntos.stream().forEach((vertice) -> {
            vertice.add(p);
        });
    }

    /**
     *
     * @return
     */
    public double getArea() {
        //genero un punto al azar 
        Random rnd = new Random(currentTimeMillis());
        double x = 2 * rnd.nextDouble() - 1;
        double y = 2 * rnd.nextDouble() - 1;
        double area = 0;

        Point2D p = new Point2D(x, y);

        // calculo el area de cada triangulo entre la arista del poligono y
        // el punto creado al azar
        area = this.getEdgeList().stream().map((arista) -> arista.signed2DTriArea(p) / 2).reduce(area, (accumulator, _item) -> accumulator + _item); // sumo o resto el area de acuerdo a la regla de la mano derecha

        return area;
    }

    /**
     *
     * @return
     */
    public List<Double> getListX() {
        List<Double> listaCoordsX = new ArrayList<>();

        this.listaPuntos.stream().forEach((p) -> {
            listaCoordsX.add(p.getCoords(0));
        });

        return listaCoordsX;
    }

    /**
     *
     * @return
     */
    public List<Double> getListY() {
        List<Double> listaCoordsY = new ArrayList<>();

        this.listaPuntos.stream().forEach((p) -> {
            listaCoordsY.add(p.getCoords(1));
        });

        return listaCoordsY;
    }

    /**
     *
     * @return
     */
    public List<Edge2D> getEdgeList() {
        List<Edge2D> lista = new ArrayList<>();
        int n = this.listaPuntos.size();

        for (int i = 0; i < this.listaPuntos.size(); i++) {
            lista.add(new Edge2D(this.listaPuntos.get(i), this.listaPuntos.get((i + 1) % n)));
        }

        return lista;
    }

    /**
     *
     * @return
     */
    public AABB2D getBox() {
        if (!this.listaPuntos.isEmpty()) {
            double minX = this.listaPuntos.get(0).getX(), minY = this.listaPuntos.get(0).getY();
            double maxX = this.listaPuntos.get(0).getX(), maxY = this.listaPuntos.get(0).getY();

            for (Point2D p : this.listaPuntos) {
                if (p.getX() > maxX) {
                    maxX = p.getX();
                }
                if (p.getX() < minX) {
                    minX = p.getX();
                }
                if (p.getY() > maxY) {
                    maxY = p.getY();
                }
                if (p.getY() < minY) {
                    minY = p.getY();
                }
            }

            return new AABB2D(new Point2D(minX, minY), new Point2D(maxX, maxY));
        }
        return null;
    }

    private void rotar(double angle) {
        for (int i = 0; i < this.listaPuntos.size(); i++) {
            Point2D p = this.listaPuntos.get(i);
            this.listaPuntos.set(i, p.rotateBy(angle));
        }
    }

    /**
     *
     * @param scale
     */
    public void amplificar(double scale) {
        for (int i = 0; i < this.listaPuntos.size(); i++) {
            this.listaPuntos.set(i, this.listaPuntos.get(i).multipliedByScalar(scale));
        }
    }

    /**
     *
     * @param areaTotal
     * @return
     */
    public abstract double getScale(double areaTotal);

    /**
     *
     */
    public void clear() {
        this.listaPuntos.clear();
    }

    /**
     * Transformacion a un espacio tipo pantalla, donde el maximo vertical se
     * encuentra hacia abajo y el maximo horizontal a la derecha
     *
     * @param limitesEspacioSimulacion
     * @param limitesEspacioGrafico
     * @return
     */
    public List<Point2D> toScreenTransformation(List<Pair<Double>> limitesEspacioSimulacion, List<Pair<Integer>> limitesEspacioGrafico) {
        List<Point2D> listaPuntosLocal = new ArrayList<>();

        double Asimulacion = (limitesEspacioSimulacion.get(0).getSecond() - limitesEspacioSimulacion.get(0).getFirst());
        double Bsimulacion = (limitesEspacioSimulacion.get(1).getSecond() - limitesEspacioSimulacion.get(1).getFirst());
        double Ascreen = (limitesEspacioGrafico.get(0).getSecond() - limitesEspacioGrafico.get(0).getFirst());
        double Bscreen = (limitesEspacioGrafico.get(1).getSecond() - limitesEspacioGrafico.get(1).getFirst());

        this.getVertices().stream().forEach((p) -> {
            double xp = limitesEspacioGrafico.get(0).getFirst() + (p.getX() - limitesEspacioSimulacion.get(0).getFirst()) * Ascreen / Asimulacion;
            double yp = limitesEspacioGrafico.get(1).getSecond() - (p.getY() - limitesEspacioSimulacion.get(1).getFirst()) * Bscreen / Bsimulacion;

            listaPuntosLocal.add(new Point2D(xp, yp));
        });

        return listaPuntosLocal;
    }
}
