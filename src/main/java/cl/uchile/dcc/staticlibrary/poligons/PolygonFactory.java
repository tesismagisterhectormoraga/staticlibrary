/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.poligons;

/**
 *
 * @author hmoraga
 */
public class PolygonFactory {

    /**
     *
     * @param model
     * @param areaFinal
     * @param rotate
     * @return
     */
    public static Polygon2D getPolygon(PolygonType model, double areaFinal, boolean rotate) {
        Polygon2D poly = null;
        double scale;

        switch (model) {
            case TRIANGULO:
                poly = new Triangle(rotate);
                scale = poly.getScale(areaFinal);
                poly.amplificar(scale);
                break;
            case CUADRADO:
                poly = new Square(rotate);
                scale = poly.getScale(areaFinal);
                poly.amplificar(scale);
                break;
            case PENTAGONO:
                poly = new Pentagon(rotate);
                scale = poly.getScale(areaFinal);
                poly.amplificar(scale);
                break;
            case HEXAGONO:
                poly = new Hexagon(rotate);
                scale = poly.getScale(areaFinal);
                poly.amplificar(scale);
                break;
            case ESTRELLA4:
                poly = new FourPointsStar(rotate);
                scale = poly.getScale(areaFinal);
                poly.amplificar(scale);
                break;
            case ESTRELLA6:
                poly = new SixPointsStar(rotate);
                scale = poly.getScale(areaFinal);
                poly.amplificar(scale);
                break;
            /*case RECTANGULO:
                // tipos de rectangulo:
                // {1:1:sqrt(2); 3:4:5; 5:12:13; 7:24:25}
                Random rnd = new Random(System.currentTimeMillis());
                
                int numero = rnd.nextInt(4);
                int alto, ancho;
                
                switch (numero) {
                    case 0:
                        alto = 1;
                        ancho = 1;
                        break;
                    case 1:
                        alto = 3;
                        ancho = 4;
                        break;
                    case 2:
                        alto = 5;
                        ancho = 12;
                        break;
                    default:
                        alto = 7;
                        ancho = 24;
                        break;
                }

                poly = new Rectangulo(alto, ancho, rotate);
                scale = poly.getScale(areaFinal);
                poly.amplificar(scale);
                break; */
            case BOOMERANG:
                poly = new Boomerang(rotate);
                scale = poly.getScale(areaFinal);
                poly.amplificar(scale);
                break;
            default:
                break;
        }

        return poly;
    }

}
