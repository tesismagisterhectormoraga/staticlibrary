package cl.uchile.dcc.staticlibrary.poligons;

import static java.lang.Math.sqrt;

/**
 * Clase Triangulo, que hereda de la clase Polygon2D
 *
 * @author hmoraga
 */
public final class Triangle extends Polygon2D {

    /**
     * Genera un triangulo de areaFinal dada
     *
     * @param areaFinal valor del area final del objeto
     * @param rotate si se da una rotacion al azar o no
     */
    public Triangle(double areaFinal, boolean rotate) {
        this(rotate);   // triangulo generado en un circulo unitario
        // me devuelve el factor por el cual multiplicar cada punto para
        // tener un area final dada
        double scale = this.getScale(areaFinal);
        super.amplificar(scale);
    }

    /**
     * Genera un triangulo inscrito en una circunferencia de lado 1
     *
     * @param rotate si se da una rotacion al azar o no
     */
    public Triangle(boolean rotate) {
        super(3, rotate);
    }

    /**
     *
     * @param areaFinal
     * @return
     */
    @Override
    public double getScale(double areaFinal) {
        double area0 = 0.75 * sqrt(3);
        return sqrt(areaFinal / area0);
    }
}
