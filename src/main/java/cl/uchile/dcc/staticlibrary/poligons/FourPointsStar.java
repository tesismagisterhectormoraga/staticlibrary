/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.poligons;

import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import static java.lang.Math.sqrt;
import static java.util.Arrays.asList;

/**
 *
 * @author hmoraga
 */
public class FourPointsStar extends Polygon2D {

    /**
     *
     * @param rotate
     */
    public FourPointsStar(boolean rotate) {
        super(asList(new Point2D[]{new Point2D(1, 0),
            new Point2D(0.5, 0.25),
            new Point2D(0.25, 0.125),
            new Point2D(0.125, 0.25),
            new Point2D(0.25, 0.5),
            new Point2D(0, 1),
            new Point2D(-0.25, 0.5),
            new Point2D(-0.125, 0.25),
            new Point2D(-0.25, 0.125),
            new Point2D(-0.5, 0.25),
            new Point2D(-1, 0),
            new Point2D(-0.5, -0.25),
            new Point2D(-0.25, -0.125),
            new Point2D(-0.125, -0.25),
            new Point2D(-0.25, -0.5),
            new Point2D(0, -1),
            new Point2D(0.25, -0.5),
            new Point2D(0.125, -0.25),
            new Point2D(0.25, -0.125),
            new Point2D(0.5, -0.25)}), rotate);
    }

    /**
     *
     * @param areaFinal
     * @param rotate
     */
    public FourPointsStar(double areaFinal, boolean rotate) {
        this(rotate);
        super.amplificar(this.getScale(areaFinal));
    }

    /**
     *
     * @param areaFinal
     * @return
     */
    @Override
    public final double getScale(double areaFinal) {
        return sqrt(areaFinal * 32 / 35);
    }

}
