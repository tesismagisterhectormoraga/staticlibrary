package cl.uchile.dcc.staticlibrary.poligons;

import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import static java.lang.Math.PI;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;
import static java.util.Arrays.asList;
import java.util.List;

/**
 *
 * @author hmoraga
 */
public class Cuadrilateral extends Polygon2D {

    private double alfa, beta, gamma, delta;

    /**
     * se puede generar un cuadrilatero cualquiera<P>
     * para un rectangulo: anglesList={alpha, (Math.PI-alpha), (Math.PI+alpha,
     * 2*Math.PI-alpha}, donde: 0 < alpha < Math.PI/2<P>
     * para un cuadrado: anglesList={0; Math.PI/2; Math.PI; 3*Math.PI/2}
     * <P>
     * para un trapecio isosceles: anglesList={alpha, (Math.PI-alpha),
     * (Math.PI+beta, 2*Math.PI-beta}, donde: 0 < alpha, beta < Math.PI/2 y
     * alpha != beta<P>
     * @param listAngles
     * @param areaFinal
     * @param rotate
     *
     */
    public Cuadrilateral(List<Double> listAngles, double areaFinal, boolean rotate) throws NumberFormatException {
        this(listAngles, rotate);

        double scale = this.getScale(areaFinal);
        super.amplificar(scale);
    }

    /**
     *
     * @param listAngles
     * @param rotate
     * @throws NumberFormatException
     */
    public Cuadrilateral(List<Double> listAngles, boolean rotate) throws NumberFormatException {
        super(asList(new Point2D[]{
            new Point2D(cos(listAngles.get(0)), sin(listAngles.get(0))),
            new Point2D(cos(listAngles.get(1)), sin(listAngles.get(1))),
            new Point2D(cos(listAngles.get(2)), sin(listAngles.get(2))),
            new Point2D(cos(listAngles.get(3)), sin(listAngles.get(3)))}),
                rotate);
        assert (listAngles.get(3) <= 2 * PI);
        assert (listAngles.get(3) > listAngles.get(2));
        assert (listAngles.get(2) > listAngles.get(1));
        assert (listAngles.get(1) > listAngles.get(0));
        assert (listAngles.get(0) >= 0);

        this.alfa = listAngles.get(0);
        this.beta = listAngles.get(1) - listAngles.get(0);
        this.gamma = listAngles.get(2) - listAngles.get(1);
        this.delta = listAngles.get(3) - listAngles.get(2);
    }

    /**
     *
     * @param areaTotal
     * @return
     */
    @Override
    public double getScale(double areaTotal) {
        double areaLargoUno = (sin(this.beta / 2) * cos(this.beta / 2) + sin(this.gamma / 2) * cos(this.gamma / 2) + sin(this.delta / 2) * cos(this.delta / 2) + sin((2 * PI - this.beta - this.gamma - this.delta) / 2) * cos((2 * PI - this.beta - this.gamma - this.delta) / 2));

        return sqrt(areaTotal / areaLargoUno);
    }
}
