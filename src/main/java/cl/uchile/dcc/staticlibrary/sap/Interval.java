/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.sap;

import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.wrappers.AABB2DWrapper;


/**
 *
 * @author hmoraga
 */
public class Interval implements Comparable<Interval>{
    private Pair<Double> extremos;
    private AABB2DWrapper box;
    private Pair<Double> velocidad;

    /**
     *
     * @param extremos
     * @param box
     * @param velocidad
     */
    public Interval(Pair<Double> extremos, AABB2DWrapper box, Pair<Double> velocidad) {
        this.extremos = extremos;
        this.box = box;
        this.velocidad = velocidad;
    }

    /**
     *
     * @return
     */
    public Pair<Double> getExtremos() {
        return this.extremos;
    }

    /**
     *
     * @param extremos
     */
    public void setExtremos(Pair<Double> extremos) {
        this.extremos = extremos;
    }

    /**
     *
     * @return
     */
    public AABB2DWrapper getBox() {
        return this.box;
    }

    /**
     *
     * @param box
     */
    public void setBox(AABB2DWrapper box) {
        this.box = box;
    }

    /**
     *
     * @return
     */
    public Pair<Double> getVelocidad() {
        return this.velocidad;
    }

    /**
     *
     * @param velocidad
     */
    public void setVelocidad(Pair<Double> velocidad) {
        this.velocidad = velocidad;
    }
    
    /**
     * 
     * @param deltatime tiempo extresado como un delta de tiempo (NO ABSOLUTO)
     */
    public void setPosition(double deltatime){
        this.extremos.setFirst(this.velocidad.getFirst()*deltatime+extremos.getFirst());
        this.extremos.setSecond(this.velocidad.getSecond()*deltatime+extremos.getSecond());
    }

    /**
     *
     * @param other
     * @return
     */
    public boolean intersection(Interval other) {
        return !(this.getExtremos().getSecond() < other.getExtremos().getFirst() ||
                other.getExtremos().getSecond() < this.getExtremos().getFirst());
    }
    
    /**
     *
     * @param t
     * @return
     */
    @Override
    public int compareTo(Interval t) {
        if (this.extremos.getFirst() < t.extremos.getFirst()) {
            return -1;
        } else if (this.extremos.getFirst() > t.extremos.getFirst()) {
            return 1;
        } else {
            return 0;
        }
    }
    
    
}
