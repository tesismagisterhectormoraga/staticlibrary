/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.sap;

import cl.uchile.dcc.staticlibrary.comparators.AABB2DWrapperComparator;
import static java.lang.Double.NEGATIVE_INFINITY;
import java.util.ArrayList;
import java.util.List;
import cl.uchile.dcc.staticlibrary.poligons.AnyPolygon2D;
import cl.uchile.dcc.staticlibrary.primitives.AABB2D;
import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.PairInteger;
import cl.uchile.dcc.staticlibrary.primitives.PairList;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import cl.uchile.dcc.staticlibrary.wrappers.AABB2DWrapper;

/**
 * Suposicion INICIAL: tendremos los poligonos iniciales, los cuales tendrán
 * asociado un índice (dado por el orden en que aparecen en la lista) y sus
 * velocidades de desplazamiento (por segundo de simulacion), se necesita además
 * el intervalo de simulación y la cantidad de imágenes que tendrá la simulación
 * (o en su defecto los FPS).
 *
 * Como atributos estará la lista de cajas que representan a cada polígono, el
 * intervalo de simulacion, el timestep.
 *
 * Como atributos auxiliares están: la lista de valores extremos de cada caja
 * (por cada coordenada) y la lista de posiciones que tiene cada objeto en la
 * lista (por cada coordenada).
 *
 * @author hmoraga
 */
public class DirectSweepAndPrune2D {

    // un AABB2DWrapper es un AABB2D con un id asociado
    private List<List<AABB2DWrapper>> listaCajasPorDimension;
    private List<Pair<Double>> listaVelocidades;

    /**
     *
     * @param listaPoligonos
     */
    public DirectSweepAndPrune2D(List<List<Point2D>> listaPoligonos) {
        this.listaCajasPorDimension = new ArrayList<>(2*listaPoligonos.size());
        this.listaVelocidades = new ArrayList<>(listaPoligonos.size());
    }

    /**
     *
     * @return
     */
    public List<Pair<Double>> getSpeedList() {
        return this.listaVelocidades;
    }

    /**
     *
     * @param listaVelocidades
     */
    public void setSpeedList(List<Pair<Double>> listaVelocidades) {
        this.listaVelocidades = listaVelocidades;
    }

    /**
     * Retorna la lista de cajas de acuerdo al orden interno.
     *
     * @return lista de cajas para cada coordenada
     */
    public List<List<AABB2DWrapper>> getBoxesListByDimension() {
        return this.listaCajasPorDimension;
    }

    /**
     *
     * @param listaCajasPorDimension
     */
    public void setBoxesListByDimension(List<List<AABB2DWrapper>> listaCajasPorDimension) {
        this.listaCajasPorDimension = listaCajasPorDimension;
    }

    /* Algoritmo Sweep and Prune */
    // el máximo de colisiones de n cajas es n*(n-1)
    // se asume la mitad de eso como el tamaño reservado del ArrayList

    /**
     *
     * @return
     */
    public List<Pair<Integer>> getPairsListColliding() {
        List<Pair<Integer>> resultado = new ArrayList<>();
        PairList listaParcialX = new PairList();
        PairList listaParcialY = new PairList();

        for (int dim = 0; dim < 2; dim++) {
            if (this.listaCajasPorDimension.get(dim).size() <= 1) {
                break;
            } else {
                for (int i = 0; i < this.listaCajasPorDimension.get(dim).size() - 1; i++) {
                    AABB2DWrapper cajaIWrapper = this.listaCajasPorDimension.get(dim).get(i);
                    AABB2D boxI = cajaIWrapper.getBox();
                    
                    for (int j = i + 1; j < this.listaCajasPorDimension.get(dim).size(); j++) {
                        AABB2DWrapper cajaJWrapper = this.listaCajasPorDimension.get(dim).get(j);
                        AABB2D boxJ = cajaJWrapper.getBox();
                    
                        if (boxI.getMaximum(dim) < boxJ.getMinimum(dim)) {
                            break;
                        } else if (boxI.intersects(boxJ)) {
                            if (dim == 0) {
                                listaParcialX.addPair(new PairInteger(cajaIWrapper.getId(), cajaJWrapper.getId()));
                            } else {
                                listaParcialY.addPair(new PairInteger(cajaIWrapper.getId(), cajaJWrapper.getId()));
                            }
                        }
                    }
                }
            }
        }
        // reviso cuál de ambas listas es la más corta y según esa
        // se hace la lista de salida
        if (!listaParcialX.isEmpty() && !listaParcialY.isEmpty()) {
            //selecciono la lista de menos tamaño
            if (listaParcialX.size() < listaParcialY.size()) {
                listaParcialX.retainAll(listaParcialY);
                resultado.addAll(listaParcialX.obtainList());
            } else {
                listaParcialY.retainAll(listaParcialX);
                resultado.addAll(listaParcialY.obtainList());
            }
        }

        return resultado;
    }

    /**
     * Me ordena la lista de puntos de las cajas, es un algoritmo in-place y
     * utiliza un nodo centinela.
     */
    public final void insertionSort() {
        AABB2DWrapperComparator boxComparator;

        for (int dim = 0; dim < this.listaCajasPorDimension.size(); dim++) {
            this.listaCajasPorDimension.get(dim).add(0, new AABB2DWrapper(new AABB2D(new Point2D(NEGATIVE_INFINITY, NEGATIVE_INFINITY), new Point2D(NEGATIVE_INFINITY, NEGATIVE_INFINITY)), -1));
            boxComparator = new AABB2DWrapperComparator(dim);
            AABB2DWrapper temp;

            for (int i = 1; i < this.listaCajasPorDimension.get(dim).size(); i++) {
                temp = this.listaCajasPorDimension.get(dim).remove(i);
                int j = i;

                while (boxComparator.compare(this.listaCajasPorDimension.get(dim).get(j - 1), temp) > 0) {
                    j--;
                }
                this.listaCajasPorDimension.get(dim).add(j, temp);
            }
            this.listaCajasPorDimension.get(dim).remove(0);
        }
    }

    // Con ellas se obtendrá un AABB de cada objeto, en conjunto con su vector
    // de desplazamiento

    /**
     *
     * @param listaPoligonos
     */
    public void initializeStructures(List<List<Point2D>> listaPoligonos) {
        this.listaCajasPorDimension.clear();
        this.listaCajasPorDimension.add(new ArrayList<>(listaPoligonos.size()));
        this.listaCajasPorDimension.add(new ArrayList<>(listaPoligonos.size()));

        // agregar las cajas a la lista y de esa caja agregar los puntos
        // usando insertionSort
        for (int dim = 0; dim < 2; dim++) {
            List<AABB2DWrapper> listaCajas = this.listaCajasPorDimension.get(dim);

            int j = 0;
            for (List<Point2D> poly : listaPoligonos) {
                AABB2D caja = new AnyPolygon2D(poly, false).getBox();
                listaCajas.add(new AABB2DWrapper(caja, j++));
            }
        }
        this.insertionSort();
    }

    /**
     * Metodo que me altera la posicion actual de las cajas de los objetos, en
     * base a un delta de tiempo (dado por el timeStep).
     * <P>
     * Cada caja que envuelve a los objetos se mueve un factor:<br>
     * p*&Delta(timeStep)<br>
     * donde p es cada uno de los puntos de las cajas
     *
     * @param timeStep
     */
    public void updateBoxesPositions(double timeStep) {
        for (int dim = 0; dim < this.listaCajasPorDimension.size(); dim++) {
            for (int i = 0; i < this.listaCajasPorDimension.get(dim).size(); i++) {
                AABB2DWrapper boxWrap = this.listaCajasPorDimension.get(dim).get(i);
                int objId = boxWrap.getId();
                Pair<Double> velocidadTemp = this.listaVelocidades.get(objId);

                boxWrap.move(new Point2D(velocidadTemp.getFirst() * timeStep, velocidadTemp.getSecond() * timeStep));
            }
        }
    }
}
