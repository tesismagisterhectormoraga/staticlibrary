/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.sap;

import cl.uchile.dcc.staticlibrary.comparators.ElementComparator;
import cl.uchile.dcc.staticlibrary.poligons.AnyPolygon2D;
import cl.uchile.dcc.staticlibrary.poligons.Polygon2D;
import cl.uchile.dcc.staticlibrary.primitives.AABB2D;
import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.PairInteger;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import static java.lang.Math.min;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

/**
 * Suposicion INICIAL: tendremos los poligonos iniciales, los cuales tendrán
 * asociado un índice (dado por el orden en que aparecen en la lista) y sus
 * velocidades de desplazamiento (por segundo de simulacion), se necesita además
 * el intervalo de simulación y la cantidad de imágenes que tendrá la simulación
 * (o en su defecto los FPS).
 *
 * Como atributos estará la lista de cajas que representan a cada polígono, el
 * intervalo de simulacion, el timestep.
 *
 * Como atributos auxiliares están: la lista de valores extremos de cada caja
 * (por cada coordenada) y la lista de posiciones que tiene cada objeto en la
 * lista (por cada coordenada).
 *
 * @author hmoraga
 */
public class IncrementalSweepAndPrune {

    private List<AABB2D> listaCajas;
    private List<Pair<Double>> listaVelocidades;
    private List<Element> endPointsX, endPointsY;
    private List<Pair<Integer>> posicionesActualesX, posicionesActualesY;
    private HashMap<Integer, Set<Pair<Integer>>> listaColisionesX, listaColisionesY;

    /**
     *
     * @param listaPoligonos
     * @param listaVelocidades
     */
    public IncrementalSweepAndPrune(List<List<Point2D>> listaPoligonos, List<Pair<Double>> listaVelocidades) {
        listaCajas = new ArrayList<>(listaPoligonos.size());

        for (int i = 0; i < listaPoligonos.size(); i++) {
            Polygon2D poly = new AnyPolygon2D(listaPoligonos.get(i), false);
            AABB2D caja = poly.getBox();
            listaCajas.add(caja);
        }

        this.listaVelocidades = listaVelocidades;
        this.endPointsX = new ArrayList<>(2 * listaPoligonos.size());
        this.endPointsY = new ArrayList<>(2 * listaPoligonos.size());
        this.listaColisionesX = new HashMap<>();
        this.listaColisionesY = new HashMap<>();
    }

    public List<Pair<Double>> getSpeedList() {
        return listaVelocidades;
    }

    public void setSpeedList(List<Pair<Double>> listaVelocidades) {
        this.listaVelocidades = listaVelocidades;
    }

    public final void initializeStructures() {
        // creo la lista de cajas, dado el poligono
        posicionesActualesX = new ArrayList<>(listaCajas.size());
        posicionesActualesY = new ArrayList<>(listaCajas.size());

        // genero la lista de puntos finales
        for (int i = 0; i < listaCajas.size(); i++) {
            List<Point2D> listaPuntos = listaCajas.get(i).getPoints();
            this.endPointsX.add(new Element(listaPuntos.get(0).getX(), i, false)); // px
            this.endPointsX.add(new Element(listaPuntos.get(2).getX(), i, true));  // qx
            this.endPointsY.add(new Element(listaPuntos.get(0).getY(), i, false)); // py
            this.endPointsY.add(new Element(listaPuntos.get(2).getY(), i, true));  // qy
        }

        // ordeno los puntos finales
        Collections.sort(endPointsX, new ElementComparator());
        Collections.sort(endPointsY, new ElementComparator());

        // lleno la lista de posiciones iniciales
        for (int i = 0; i < listaCajas.size(); i++) {
            posicionesActualesX.add(new PairInteger(-1, -1));
            posicionesActualesY.add(new PairInteger(-1, -1));
        }

        //recorro la lista de extremos en X e Y para llenar la lista
        for (int i = 0; i < endPointsX.size(); i++) {
            Element elem = endPointsX.get(i);

            if (!elem.isIsFin()) {
                posicionesActualesX.get(elem.getId()).setFirst(i);
            } else {
                posicionesActualesX.get(elem.getId()).setSecond(i);
            }
        }

        for (int i = 0; i < endPointsY.size(); i++) {
            Element elem = endPointsY.get(i);

            if (!elem.isIsFin()) {
                posicionesActualesY.get(elem.getId()).setFirst(i);
            } else {
                posicionesActualesY.get(elem.getId()).setSecond(i);
            }
        }

        obtainPairLists();
    }

    /**
     *
     * @return
     */
    public List<Pair<Integer>> getCollisionsList() {
        List<Pair<Integer>> salida = new ArrayList<>();

        if (listaColisionesX.size() < listaColisionesY.size()) {
            Set<Integer> claves = listaColisionesX.keySet();

            claves.stream().filter((key) -> (listaColisionesY.containsKey(key))).map((key) -> {
                Set<Pair<Integer>> tempX = new TreeSet<>(listaColisionesX.get(key));
                tempX.retainAll(listaColisionesY.get(key));
                return tempX;
            }).forEachOrdered((tempX) -> {
                salida.addAll(tempX);
            });
        } else {
            Set<Integer> claves = listaColisionesY.keySet();

            claves.stream().filter((key) -> (listaColisionesX.containsKey(key))).map((key) -> {
                Set<Pair<Integer>> tempY = new TreeSet<>(listaColisionesY.get(key));
                tempY.retainAll(listaColisionesX.get(key));
                return tempY;
            }).forEachOrdered((tempY) -> {
                salida.addAll(tempY);
            });
        }
        return salida;
    }

    /**
     *
     * @param delta
     */
    public void updatePositions(double delta) {
        for (int i = 0; i < this.endPointsX.size(); i++) {
            int id = this.endPointsX.get(i).getId();
            Pair<Double> vels = this.listaVelocidades.get(id);
            this.endPointsX.get(i).setPosition(this.endPointsX.get(i).getPosition() + vels.getFirst() * delta);
        }

        for (int i = 0; i < this.endPointsY.size(); i++) {
            int id = this.endPointsY.get(i).getId();
            Pair<Double> vels = this.listaVelocidades.get(id);
            this.endPointsY.get(i).setPosition(this.endPointsY.get(i).getPosition() + vels.getSecond() * delta);
        }
    }

    /**
     *
     * @return
     */
    public boolean validateEndpointsLists() {
        ElementComparator elemCmp = new ElementComparator();

        for (int i = 1; i < this.endPointsX.size(); i++) {
            if (elemCmp.compare(this.endPointsX.get(i - 1), this.endPointsX.get(i)) > 0) {
                return false;
            }
        }

        for (int i = 1; i < this.endPointsY.size(); i++) {
            if (elemCmp.compare(this.endPointsY.get(i - 1), this.endPointsY.get(i)) > 0) {
                return false;
            }
        }

        return true;
    }

    /**
     *
     */
    public void insertionSort() {
        ElementComparator elemCmp = new ElementComparator();

        for (int i = 1; i < this.endPointsX.size(); i++) {
            if (elemCmp.compare(this.endPointsX.get(i - 1), this.endPointsX.get(i)) > 0) {
                // cambiar lista de posiciones de los extremos
                int id0 = this.endPointsX.get(i - 1).getId();
                boolean b0 = this.endPointsX.get(i - 1).isIsFin();
                int id1 = this.endPointsX.get(i).getId();
                boolean b1 = this.endPointsX.get(i).isIsFin();

                if (b0) {
                    this.posicionesActualesX.get(id0).setSecond(i);
                } else {
                    this.posicionesActualesX.get(id0).setFirst(i);
                }

                if (b1) {
                    this.posicionesActualesX.get(id1).setSecond(i - 1);
                } else {
                    this.posicionesActualesX.get(id1).setFirst(i - 1);
                }

                // cambio las posiciones en la lista de elementos
                Element aux = this.endPointsX.get(i - 1);
                this.endPointsX.set(i - 1, this.endPointsX.get(i));
                this.endPointsX.set(i, aux);

                // reviso cambios en las listas de colisiones
                // solo se hace un cambio válido cuando se produce entre
                // el inicio de un extremo y el final de otro etremo
                if ((b0 && !b1) || (!b0 && b1)) {
                    int clave = min(id0, id1);
                    if (this.listaColisionesX.containsKey(clave)) {
                        if (this.listaColisionesX.get(clave).contains(new PairInteger(id0, id1))) {
                            this.listaColisionesX.get(clave).remove(new PairInteger(id0, id1));
                        } else {
                            this.listaColisionesX.get(clave).add(new PairInteger(id0, id1));
                        }
                    } else {
                        Set<Pair<Integer>> conjunto = new TreeSet<>();
                        conjunto.add(new PairInteger(id0, id1));
                        this.listaColisionesX.put(clave, conjunto);
                    }
                }
            }
        }

        for (int i = 1; i < this.endPointsY.size(); i++) {
            if (elemCmp.compare(this.endPointsY.get(i - 1), this.endPointsY.get(i)) > 0) {
                // cambiar lista de posiciones de los extremos
                int id0 = this.endPointsY.get(i - 1).getId();
                boolean b0 = this.endPointsY.get(i - 1).isIsFin();
                int id1 = this.endPointsY.get(i).getId();
                boolean b1 = this.endPointsY.get(i).isIsFin();

                if (b0) {
                    this.posicionesActualesY.get(id0).setSecond(i);
                } else {
                    this.posicionesActualesY.get(id0).setFirst(i);
                }

                if (b1) {
                    this.posicionesActualesY.get(id1).setSecond(i - 1);
                } else {
                    this.posicionesActualesY.get(id1).setFirst(i - 1);
                }

                // cambio las posiciones en la lista de elementos
                Element aux = this.endPointsY.get(i - 1);
                this.endPointsY.set(i - 1, this.endPointsY.get(i));
                this.endPointsY.set(i, aux);

                // reviso cambios en las listas de colisiones
                // solo se hace un cambio válido cuando se produce entre
                // el inicio de un extremo y el final de otro etremo
                if ((b0 && !b1) || (!b0 && b1)) {
                    int clave = min(id0, id1);
                    if (this.listaColisionesY.containsKey(clave)) {
                        if (this.listaColisionesY.get(clave).contains(new PairInteger(id0, id1))) {
                            this.listaColisionesY.get(clave).remove(new PairInteger(id0, id1));
                        } else {
                            this.listaColisionesY.get(clave).add(new PairInteger(id0, id1));
                        }
                    } else {
                        Set<Pair<Integer>> conjunto = new TreeSet<>();
                        conjunto.add(new PairInteger(id0, id1));
                        this.listaColisionesY.put(clave, conjunto);
                    }
                }
            }
        }
    }

    private void obtainPairLists() {
        // obtener las listas de pares iniciales
        for (int i = 0; i < posicionesActualesX.size() - 1; i++) {
            Pair<Integer> parUno = posicionesActualesX.get(i);
            for (int j = i + 1; j < posicionesActualesX.size(); j++) {
                Pair<Integer> parDos = posicionesActualesX.get(j);

                if (parUno.intersects(parDos)) {
                    Integer tmp = min(i, j);
                    if (listaColisionesX.containsKey(tmp)) {
                        listaColisionesX.get(tmp).add(new PairInteger(i, j));
                    } else {
                        Set<Pair<Integer>> conjunto = new TreeSet<>();
                        conjunto.add(new PairInteger(i, j));
                        listaColisionesX.put(tmp, conjunto);
                    }
                }
            }
        }

        for (int i = 0; i < posicionesActualesY.size() - 1; i++) {
            Pair<Integer> parUno = posicionesActualesY.get(i);
            for (int j = i + 1; j < posicionesActualesY.size(); j++) {
                Pair<Integer> parDos = posicionesActualesY.get(j);

                if (parUno.intersects(parDos)) {
                    Integer tmp = min(i, j);
                    if (listaColisionesY.containsKey(tmp)) {
                        listaColisionesY.get(tmp).add(new PairInteger(i, j));
                    } else {
                        Set<Pair<Integer>> conjunto = new TreeSet<>();
                        conjunto.add(new PairInteger(i, j));
                        listaColisionesY.put(tmp, conjunto);
                    }
                }
            }
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(this.listaCajas);
        hash = 59 * hash + Objects.hashCode(this.listaVelocidades);
        hash = 59 * hash + Objects.hashCode(this.endPointsX);
        hash = 59 * hash + Objects.hashCode(this.endPointsY);
        hash = 59 * hash + Objects.hashCode(this.posicionesActualesX);
        hash = 59 * hash + Objects.hashCode(this.posicionesActualesY);
        hash = 59 * hash + Objects.hashCode(this.listaColisionesX);
        hash = 59 * hash + Objects.hashCode(this.listaColisionesY);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final IncrementalSweepAndPrune other = (IncrementalSweepAndPrune) obj;
        if (!Objects.equals(this.listaCajas, other.listaCajas)) {
            return false;
        }
        if (!Objects.equals(this.listaVelocidades, other.listaVelocidades)) {
            return false;
        }
        if (!Objects.equals(this.endPointsX, other.endPointsX)) {
            return false;
        }
        if (!Objects.equals(this.endPointsY, other.endPointsY)) {
            return false;
        }
        if (!Objects.equals(this.posicionesActualesX, other.posicionesActualesX)) {
            return false;
        }
        if (!Objects.equals(this.posicionesActualesY, other.posicionesActualesY)) {
            return false;
        }
        if (!Objects.equals(this.listaColisionesX, other.listaColisionesX)) {
            return false;
        }
        if (!Objects.equals(this.listaColisionesY, other.listaColisionesY)) {
            return false;
        }
        return true;
    }

    public List<AABB2D> getListaCajas() {
        return listaCajas;
    }

    public void setListaCajas(List<AABB2D> listaCajas) {
        this.listaCajas = listaCajas;
    }

    public List<Pair<Double>> getListaVelocidades() {
        return listaVelocidades;
    }

    public void setListaVelocidades(List<Pair<Double>> listaVelocidades) {
        this.listaVelocidades = listaVelocidades;
    }

    public List<Element> getEndPointsX() {
        return endPointsX;
    }

    public void setEndPointsX(List<Element> endPointsX) {
        this.endPointsX = endPointsX;
    }

    public List<Element> getEndPointsY() {
        return endPointsY;
    }

    public void setEndPointsY(List<Element> endPointsY) {
        this.endPointsY = endPointsY;
    }

    public List<Pair<Integer>> getPosicionesActualesX() {
        return posicionesActualesX;
    }

    public void setPosicionesActualesX(List<Pair<Integer>> posicionesActualesX) {
        this.posicionesActualesX = posicionesActualesX;
    }

    public List<Pair<Integer>> getPosicionesActualesY() {
        return posicionesActualesY;
    }

    public void setPosicionesActualesY(List<Pair<Integer>> posicionesActualesY) {
        this.posicionesActualesY = posicionesActualesY;
    }

    public HashMap<Integer, Set<Pair<Integer>>> getListaColisionesX() {
        return listaColisionesX;
    }

    public void setListaColisionesX(HashMap<Integer, Set<Pair<Integer>>> listaColisionesX) {
        this.listaColisionesX = listaColisionesX;
    }

    public HashMap<Integer, Set<Pair<Integer>>> getListaColisionesY() {
        return listaColisionesY;
    }

    public void setListaColisionesY(HashMap<Integer, Set<Pair<Integer>>> listaColisionesY) {
        this.listaColisionesY = listaColisionesY;
    }
    
    
}
