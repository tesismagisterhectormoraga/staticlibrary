/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.sap;

import cl.uchile.dcc.staticlibrary.primitives.Pair;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hmoraga
 */
public class StatisticsDirectSP implements Statistics {

    private Pair<Double> intervaloSimulacion;
    private List<Double> times;
    private List<List<Pair<Integer>>> collisionsByObject;
    private final int cantidadObjetos;
    private double areaTotal;
    private double areaOcupadaInicial;

    /**
     *
     * @param cantidadObjetos
     * @param intervaloSimulacion
     */
    public StatisticsDirectSP(int cantidadObjetos, Pair<Double> intervaloSimulacion) {
        this.times = new ArrayList<>();
        this.intervaloSimulacion = intervaloSimulacion;
        this.collisionsByObject = new ArrayList<>();
        this.cantidadObjetos = cantidadObjetos;
    }

    /**
     *
     * @param colisiones
     * @param time
     */
    public void addStatistics(List<Pair<Integer>> colisiones, double time) {
        this.times.add(time);
        this.collisionsByObject.add(colisiones);
    }

    /**
     *
     * @return
     */
    @Override
    public double getTotalArea() {
        return this.areaTotal;
    }

    /**
     *
     * @return
     */
    public double getTotalSimulatedTime() {
        return (this.times.isEmpty()) ? 0 : this.times.get(this.times.size() - 1) - this.times.get(0);
    }

    /**
     *
     * @return
     */
    @Override
    public Pair<Double> getSimulationInterval() {
        return this.intervaloSimulacion;
    }

    /**
     *
     * @return
     */
    @Override
    public double getInitialOcupatedArea() {
        return this.areaOcupadaInicial;
    }

    /**
     *
     * @param areaTotal
     */
    @Override
    public void setTotalArea(double areaTotal) {
        this.areaTotal = areaTotal;
    }

    /**
     *
     * @param areaOcupadaInicial
     */
    @Override
    public void setInitialOcupatedArea(double areaOcupadaInicial) {
        this.areaOcupadaInicial = areaOcupadaInicial;
    }

    /**
     *
     * @param intervaloSimulacion
     */
    @Override
    public void setSimulationInterval(Pair<Double> intervaloSimulacion) {
        this.intervaloSimulacion = intervaloSimulacion;
    }

    /**
     *
     * @return
     */
    @Override
    public List<String> getText() {
        List<String> str = new ArrayList<>();
        str.add("CantidadObjetos:" + this.cantidadObjetos);
        str.add("Area Inicial:" + this.areaTotal);
        str.add("Area Objetos:" + this.areaOcupadaInicial);
        str.add("Intervalo simulacion:[" + this.intervaloSimulacion.getFirst() + "," + this.intervaloSimulacion.getSecond() + "]");

        for (int i = 0; i < this.times.size(); i++) {
            Double t = this.times.get(i);
            str.add("t=" + t);
            str.add(this.collisionsByObject.get(i).toString());
        }
        return str;
    }

    /**
     *
     * @param arch
     * @throws IOException
     */
    @Override
    public void writeToFile(Path arch) throws IOException {
        try (FileWriter fw = new FileWriter(arch.toFile());
                BufferedWriter bw = new BufferedWriter(fw)) {

            for (String s : this.getText()) {
                bw.append(s);
                bw.newLine();
            }
        }
    }
}
