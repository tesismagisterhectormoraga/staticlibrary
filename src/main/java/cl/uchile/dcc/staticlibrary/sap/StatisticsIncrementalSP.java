/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.sap;

import cl.uchile.dcc.staticlibrary.primitives.Pair;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hmoraga
 */
public class StatisticsIncrementalSP implements Statistics {
    private Pair<Double> intervaloSimulacion;
    private List<Double> times;
    private List<List<Pair<Integer>>> listCollisions;
    private final int cantidadObjetos;
    private double areaTotal;
    private double areaOcupadaInicial;

    /**
     *
     * @param cantidadObjetos
     */
    public StatisticsIncrementalSP(int cantidadObjetos) {
        this.times = new ArrayList<>();
        this.listCollisions = new ArrayList<>();
        this.cantidadObjetos = cantidadObjetos;
    }

    /**
     *
     * @param time
     * @param colisiones
     */
    public void addStatistics(double time, List<Pair<Integer>> colisiones) {
        this.times.add(time);
        this.listCollisions.add(colisiones);
    }

    /**
     *
     * @return
     */
    @Override
    public double getTotalArea() {
        return this.areaTotal;
    }

    /**
     *
     * @return
     */
    @Override
    public Pair<Double> getSimulationInterval() {
        return this.intervaloSimulacion;
    }

    /**
     *
     * @param intervaloSimulacion
     */
    @Override
    public void setSimulationInterval(Pair<Double> intervaloSimulacion) {
        this.intervaloSimulacion = intervaloSimulacion;
    }

    /**
     *
     * @return
     */
    @Override
    public double getInitialOcupatedArea() {
        return this.areaOcupadaInicial;
    }

    /**
     *
     * @param areaTotal
     */
    @Override
    public void setTotalArea(double areaTotal) {
        this.areaTotal = areaTotal;
    }

    /**
     *
     * @param areaOcupadaInicial
     */
    @Override
    public void setInitialOcupatedArea(double areaOcupadaInicial) {
        this.areaOcupadaInicial = areaOcupadaInicial;
    }

    /**
     *
     * @return
     */
    @Override
    public List<String> getText() {
        List<String> str = new ArrayList<>(this.times.size() + 3);
        str.add("CantidadObjetos:" + this.cantidadObjetos);
        str.add("Area Inicial:" + this.areaTotal);
        str.add("Area Objetos:" + this.areaOcupadaInicial);

        for (int i = 0; i < this.times.size(); i++) {
            Double t = this.times.get(i);
            str.add("t=" + t);
            str.add(this.listCollisions.get(i).toString());
        }

        return str;
    }

    /**
     *
     * @param arch
     * @throws IOException
     */
    @Override
    public void writeToFile(Path arch) throws IOException {

    }
}
