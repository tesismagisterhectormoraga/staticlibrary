/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.sap;

import java.io.IOException;
import static java.lang.Integer.valueOf;
import static java.lang.System.out;
import java.nio.charset.Charset;
import static java.nio.charset.Charset.forName;
import static java.nio.file.Files.readAllLines;
import static java.nio.file.Paths.get;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static java.util.regex.Pattern.compile;
import cl.uchile.dcc.staticlibrary.poligons.AnyPolygon2D;
import cl.uchile.dcc.staticlibrary.primitives.AABB2D;
import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.PairDouble;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;

/**
 *
 * @author hmoraga
 */
public class IncrementalSAPWrapper {

    private int frameNumber, numDims;
    private List<AABB2D> listaCajas;
    private double areaObjetos, areaSimulacion;

    /**
     *
     * @param fileName
     * @param verbose
     * @throws IOException
     */
    public IncrementalSAPWrapper(String fileName, boolean verbose) throws IOException {
        this.obtainStructures(fileName, verbose);
    }

    private void obtainStructures(String fileName, boolean verbose) throws IOException {
        Pattern pattern;
        Matcher matcher;

        if (verbose) {
            out.println("Leyendo archivo " + fileName); // leo los datos del archivo
        }

        Charset utf8 = forName("UTF-8");
        List<String> lista = readAllLines(get(fileName), utf8);

        // frame, es crucial para la creacion del archivo de salida 
        pattern = compile("//frame:(\\d+)");
        matcher = pattern.matcher(lista.get(0));

        this.frameNumber = (matcher.find()) ? valueOf(matcher.group(1)) : 0;
        matcher.reset();

        pattern = compile(".*\\((-?\\d+.\\d+),(-?\\d+.\\d+)\\),\\((-?\\d+.\\d+),(-?\\d+.\\d+)\\)");
        matcher = pattern.matcher(lista.get(2));

        List<Pair<Double>> limitesLocal = new ArrayList<>(2);

        if (matcher.find()) {
            String grupo1 = matcher.group(1);
            String grupo2 = matcher.group(2);
            String grupo3 = matcher.group(3);
            String grupo4 = matcher.group(4);

            // dimensiones en x
            limitesLocal.add(new PairDouble(Double.valueOf(grupo1), Double.valueOf(grupo2)));
            // dimensiones en y
            limitesLocal.add(new PairDouble(Double.valueOf(grupo3), Double.valueOf(grupo4)));
        } else {
            // dimensiones en x
            limitesLocal.add(new PairDouble(-10.0, 10.0));
            // dimensiones en y
            limitesLocal.add(new PairDouble(-10.0, 10.0));
        }
        matcher.reset();

        pattern = compile("//dimensiones:(\\d+)");
        matcher = pattern.matcher(lista.get(3));

        if (matcher.find()) {
            String grupo1 = matcher.group(1);
            this.numDims = valueOf(grupo1);
        } else {
            this.numDims = 2;
        }
        matcher.reset();

        List<List<Point2D>> listaPuntosObjetosLocal = new ArrayList<>(lista.size() - 4);
        // proceso los datos que se repiten en todos los archivos
        for (String s : lista.subList(4, lista.size())) {
            // primer dato es la cantidad de puntos del objeto
            pattern = compile("^(\\d+)\\s+(.*)");
            matcher = pattern.matcher(s);

            if (matcher.find()) {
                int cantPuntos = valueOf(matcher.group(1));
                List<Point2D> listaPuntos = new ArrayList<>(cantPuntos);
                String[] aux = matcher.group(2).split("\\s+");

                assert (aux.length == this.numDims * cantPuntos);

                if (this.numDims == 2) {
                    for (int i = 0; i < cantPuntos; i++) {
                        double posX = valueOf(aux[2 * i]);
                        double posY = valueOf(aux[2 * i + 1]);

                        listaPuntos.add(new Point2D(posX, posY));
                    }

                    listaPuntosObjetosLocal.add(listaPuntos);
                }
            }
            matcher.reset();
        }

        // calculo el area inicial y la inicial de los objetos
        double area = 0.0;

        this.areaObjetos = listaPuntosObjetosLocal.stream().map((listaPuntos) -> new AnyPolygon2D(listaPuntos, false).getArea()).reduce(area, (accumulator, _item) -> accumulator + _item);
        this.areaSimulacion = (limitesLocal.get(0).getSecond() - limitesLocal.get(0).getFirst()) * (limitesLocal.get(1).getSecond() - limitesLocal.get(1).getFirst());
        this.listaCajas = new ArrayList<>(listaPuntosObjetosLocal.size());

        // calculo la lista de cajas
        listaPuntosObjetosLocal.stream().map((poly) -> new AnyPolygon2D(poly, false).getBox()).forEachOrdered((auxBox) -> {
            this.listaCajas.add(auxBox);
        });
    }

    /**
     *
     * @return
     */
    public int getFrameNumber() {
        return this.frameNumber;
    }

    /**
     *
     * @param frameNumber
     */
    public void setFrameNumber(int frameNumber) {
        this.frameNumber = frameNumber;
    }

    /**
     *
     * @return
     */
    public int getNumDims() {
        return this.numDims;
    }

    /**
     *
     * @param numDims
     */
    public void setNumDims(int numDims) {
        this.numDims = numDims;
    }

    /**
     *
     * @return
     */
    public List<AABB2D> getBoxesList() {
        return this.listaCajas;
    }

    /**
     *
     * @param listaCajas
     */
    public void setBoxesList(List<AABB2D> listaCajas) {
        this.listaCajas = listaCajas;
    }

    /**
     *
     * @return
     */
    public double getObjectsArea() {
        return this.areaObjetos;
    }

    /**
     *
     * @param listaPuntosObjetos
     */
    public void setObjectsArea(List<List<Point2D>> listaPuntosObjetos) {
        double suma = 0.0;

        suma = listaPuntosObjetos.stream().map((poly) -> (new AnyPolygon2D(poly, false).getArea())).reduce(suma, (accumulator, _item) -> accumulator + _item);

        this.areaObjetos = suma;
    }

    /**
     *
     * @return
     */
    public double getSimulationArea() {
        return this.areaSimulacion;
    }

    /**
     *
     * @param dimensiones
     */
    public void setSimulationArea(List<Pair<Double>> dimensiones) {
        this.areaSimulacion = (dimensiones.get(0).getSecond() - dimensiones.get(0).getFirst()) * (dimensiones.get(1).getSecond() - dimensiones.get(1).getFirst());
    }
}
