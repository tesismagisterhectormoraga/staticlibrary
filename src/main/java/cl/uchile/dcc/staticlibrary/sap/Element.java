/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.sap;

import static java.lang.Double.doubleToLongBits;

/**
 *
 * @author hmoraga
 */
public class Element {
    private double position;
    private int id;
    private boolean isFin;
    
    /**
     *
     * @param position
     * @param id
     * @param isFin
     */
    public Element(double position, int id, boolean isFin) {
        this.position = position;
        this.id = id;
        this.isFin = isFin;
    }

    /**
     *
     * @return
     */
    public double getPosition() {
        return this.position;
    }

    /**
     *
     * @param position
     */
    public void setPosition(double position) {
        this.position = position;
    }

    /**
     *
     * @return
     */
    public int getId() {
        return this.id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public boolean isIsFin() {
        return this.isFin;
    }

    /**
     *
     * @param isFin
     */
    public void setIsFin(boolean isFin) {
        this.isFin = isFin;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return (this.isFin)?"q_"+this.id+"="+this.position:"p_"+this.id+"="+this.position;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (int) (doubleToLongBits(this.position) ^ (doubleToLongBits(this.position) >>> 32));
        hash = 29 * hash + this.id;
        hash = 29 * hash + (this.isFin ? 1 : 0);
        return hash;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final Element other = (Element) obj;
        if (doubleToLongBits(this.position) != doubleToLongBits(other.position)) {
            return false;
        }
        if (this.id != other.id) {
            return false;
        }
        return this.isFin == other.isFin;
    }    
}
