/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.sap;

import cl.uchile.dcc.staticlibrary.primitives.Pair;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

/**
 *
 * @author hmoraga
 */
public interface Statistics {

    /**
     *
     * @return
     */
    public double getTotalArea();

    /**
     *
     * @return
     */
    public Pair<Double> getSimulationInterval();

    /**
     *
     * @return
     */
    public double getInitialOcupatedArea();

    /**
     *
     * @param areaTotal
     */
    public void setTotalArea(double areaTotal);

    /**
     *
     * @param intervaloTiempo
     */
    public void setSimulationInterval(Pair<Double> intervaloTiempo);

    /**
     *
     * @param areaOcupadaInicial
     */
    public void setInitialOcupatedArea(double areaOcupadaInicial);

    /**
     *
     * @return
     */
    public List<String> getText();

    /**
     *
     * @param arch
     * @throws IOException
     */
    public void writeToFile(Path arch) throws IOException;
}
