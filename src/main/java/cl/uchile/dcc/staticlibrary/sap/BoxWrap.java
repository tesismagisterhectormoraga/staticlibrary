/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.sap;

import cl.uchile.dcc.staticlibrary.primitives.BV;
import java.util.Objects;

/**
 *
 * @author hmoraga
 */
public class BoxWrap implements Cloneable {

    private BV box;
    private int id;

    /**
     *
     * @param box
     * @param id
     */
    public BoxWrap(BV box, int id) {
        this.box = box;
        this.id = id;
    }

    /**
     *
     * @return
     */
    public BV getBox() {
        return this.box;
    }

    /**
     *
     * @param box
     */
    public void setBox(BV box) {
        this.box = box;
    }

    /**
     *
     * @return
     */
    public int getId() {
        return this.id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @param other
     * @param i
     * @return
     */
    public int compareTo(BoxWrap other, int i) {
        return this.box.compareTo(other.getBox(), i);
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.box);
        return hash;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final BoxWrap other = (BoxWrap) obj;
        return Objects.equals(this.box, other.box);
    }

    /**
     *
     * @return
     * @throws CloneNotSupportedException
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        super.clone();
        BoxWrap nuevaCaja = new BoxWrap(this.box, this.id);

        return nuevaCaja;
    }
}
