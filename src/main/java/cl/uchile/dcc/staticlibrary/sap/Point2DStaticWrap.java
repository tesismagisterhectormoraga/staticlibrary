/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.sap;

import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import java.util.Objects;

/**
 *
 * @author hmoraga
 */
public class Point2DStaticWrap {

    private Point2D punto;
    private int id;
    private boolean fin;

    /**
     *
     * @param punto
     * @param id
     * @param fin
     */
    public Point2DStaticWrap(Point2D punto, int id, boolean fin) {
        this.punto = punto;
        this.id = id;
        this.fin = fin;
    }

    /**
     *
     * @return
     */
    public Point2D getPunto() {
        return this.punto;
    }

    /**
     *
     * @param punto
     */
    public void setPunto(Point2D punto) {
        this.punto = punto;
    }

    /**
     *
     * @return
     */
    public int getId() {
        return this.id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public boolean isFin() {
        return this.fin;
    }

    /**
     *
     * @param fin
     */
    public void setFin(boolean fin) {
        this.fin = fin;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.punto);
        hash = 37 * hash + this.id;
        hash = 37 * hash + (this.fin ? 1 : 0);
        return hash;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final Point2DStaticWrap other = (Point2DStaticWrap) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.fin != other.fin) {
            return false;
        }
        return Objects.equals(this.punto, other.punto);
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return (this.fin) ? "q" + this.id : "p" + this.id;
    }

}
