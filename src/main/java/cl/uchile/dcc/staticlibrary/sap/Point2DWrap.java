/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.sap;

import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;


/**
 *
 * @author hmoraga
 */
public class Point2DWrap {
    private Point2D puntoInicial;
    private int id;
    private boolean fin;

    /**
     *
     * @param puntoInicial
     * @param id
     * @param fin
     */
    public Point2DWrap(Point2D puntoInicial, int id, boolean fin) {
        this.puntoInicial = puntoInicial;
        this.id = id;
        this.fin = fin;
    }

    /**
     *
     * @param velocidades
     * @param time
     * @return
     */
    public Point2D currentPosition(Pair<Double> velocidades, double time) {
        double posX = this.puntoInicial.getX(), posY = this.puntoInicial.getY();

        if (time != 0.0) {
            posX += velocidades.getFirst() * time;
            posY += velocidades.getSecond() * time;
        }

        return new Point2D(posX, posY);
    }

    /**
     *
     * @return
     */
    public Point2D getPuntoInicial() {
        return this.puntoInicial;
    }

    /**
     *
     * @param puntoInicial
     */
    public void setPuntoInicial(Point2D puntoInicial) {
        this.puntoInicial = puntoInicial;
    }

    /**
     *
     * @return
     */
    public int getId() {
        return this.id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public boolean isFin() {
        return this.fin;
    }

    /**
     *
     * @param fin
     */
    public void setFin(boolean fin) {
        this.fin = fin;
    }
}
