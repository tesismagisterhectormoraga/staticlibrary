/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.comparators;

import cl.uchile.dcc.staticlibrary.wrappers.Edge2DWrapper;
import java.util.Comparator;

/**
 *
 * @author hmoraga
 */
public class Edge2DWrapperComparator implements Comparator<Edge2DWrapper> {
    private int dim;

    /**
     *
     * @param dim
     */
    public Edge2DWrapperComparator(int dim) {
        assert (dim >= 0 && dim <= 1);
        this.dim = dim;
    }

    /**
     *
     * @param t
     * @param tt
     * @return
     */
    @Override
    public int compare(Edge2DWrapper t, Edge2DWrapper tt) {
        if (t.getArista().getMinValue(this.dim) < tt.getArista().getMinValue(this.dim)) {
            return -1;
        } else if (t.getArista().getMinValue(this.dim) > tt.getArista().getMinValue(this.dim)) {
            return 1;
        } else {
            if (t.getId() > tt.getId()) {
                return 1;
            } else if (t.getId() < tt.getId()) {
                return -1;
            } else {
                return 0;
            }
        }
    }
}
