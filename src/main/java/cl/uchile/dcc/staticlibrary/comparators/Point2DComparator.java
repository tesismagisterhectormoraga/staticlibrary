/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.comparators;

import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import java.util.Comparator;

/**
 *
 * @author hmoraga
 */
public class Point2DComparator implements Comparator<Point2D> {
    private final int dimension;

    /**
     *
     * @param dimension
     */
    public Point2DComparator(int dimension) {
        this.dimension = dimension;
    }

    /**
     *
     * @param o1
     * @param o2
     * @return
     */
    @Override
    public int compare(Point2D o1, Point2D o2) {
        if (o1.getCoords(this.dimension) > o2.getCoords(this.dimension)) {
            return 1;
        } else if (o1.getCoords(this.dimension) < o2.getCoords(this.dimension)) {
            return -1;
        } else {
            return 0;
        }
    }
}
