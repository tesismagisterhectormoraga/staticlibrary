/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.comparators;

import java.util.Comparator;
import cl.uchile.dcc.staticlibrary.sap.Element;

/**
 *
 * @author hmoraga
 */
public class ElementComparator implements Comparator<Element> {

    /**
     *
     * @param o1
     * @param o2
     * @return
     */
    @Override
    public int compare(Element o1, Element o2) {
        if (o1.getPosition() < o2.getPosition()) {
            return -1;
        } else if (o1.getPosition() > o2.getPosition()) {
            return 1;
        } else {
            boolean fin1 = o1.isIsFin();
            boolean fin2 = o2.isIsFin();
            int id1 = o1.getId();
            int id2 = o2.getId();
            
            if (fin1 && fin2){
                if (id1 < id2){
                    return 1;
                } else if (id1 > id2){
                    return -1;
                }
            } else if (!fin1 && !fin2){
                if (id1 < id2){
                    return -1;
                } else if (id1 > id2){
                    return 1;
                }
            } else if (fin1 && !fin2){
                return 1;
            } else if (!fin1 && fin2){
                return -1;
            }
        }
        return 0;
    }
}
