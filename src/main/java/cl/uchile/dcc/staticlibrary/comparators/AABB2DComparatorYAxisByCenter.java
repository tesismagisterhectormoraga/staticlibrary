/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.comparators;

import cl.uchile.dcc.staticlibrary.primitives.AABB2D;
import java.util.Comparator;

/**
 *
 * @author hmoraga
 */
public class AABB2DComparatorYAxisByCenter implements Comparator<AABB2D> {

    /**
     *
     * @param o1
     * @param o2
     * @return
     */
    @Override
    public int compare(AABB2D o1, AABB2D o2) {
        Point2DComparator comparador = new Point2DComparator(1);

        return comparador.compare(o1.getCentro(), o2.getCentro());
    }
}
