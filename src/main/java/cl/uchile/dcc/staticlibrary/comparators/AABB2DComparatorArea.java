/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.comparators;

import cl.uchile.dcc.staticlibrary.primitives.AABB2D;
import java.util.Comparator;

/**
 *
 * @author hmoraga
 */
public class AABB2DComparatorArea implements Comparator<AABB2D> {

    /**
     *
     * @param o1
     * @param o2
     * @return
     */
    @Override
    public int compare(AABB2D o1, AABB2D o2) {
        if (o1.getArea() > o2.getArea()) {
            return 1;
        } else if (o1.getArea() < o2.getArea()) {
            return -1;
        } else {
            return 0;
        }
    }
}
