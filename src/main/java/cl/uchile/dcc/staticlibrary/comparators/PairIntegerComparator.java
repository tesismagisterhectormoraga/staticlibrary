/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.comparators;

import cl.uchile.dcc.staticlibrary.primitives.Pair;
import java.util.Comparator;

/**
 *
 * @author hmoraga
 */
public class PairIntegerComparator implements Comparator<Pair<Integer>>{

    /**
     *
     * @param p1
     * @param p2
     * @return
     */
    @Override
    public int compare(Pair<Integer> p1, Pair<Integer> p2) {
        if (p1.getFirst()< p2.getFirst()){
            return -1;
        } else if (p1.getFirst()> p2.getFirst()){
            return 1;
        } else {
            if (p1.getSecond()< p2.getSecond()) {
                return -1;
            } else if (p1.getSecond()> p2.getSecond()) {
                return 1;
            } else {
                return 0;
            }
        }
    }
    
}
