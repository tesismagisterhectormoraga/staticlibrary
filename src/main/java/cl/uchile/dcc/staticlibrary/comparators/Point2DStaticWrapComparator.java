/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.comparators;

import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import cl.uchile.dcc.staticlibrary.sap.Point2DStaticWrap;
import java.util.Comparator;

/**
 *
 * @author hmoraga
 */
public class Point2DStaticWrapComparator implements Comparator<Point2DStaticWrap> {

    private final int dimension;

    /**
     *
     * @param dimension
     */
    public Point2DStaticWrapComparator(int dimension) {
        this.dimension = dimension;
    }

    /**
     *
     * @param o1
     * @param o2
     * @return
     */
    @Override
    public int compare(Point2DStaticWrap o1, Point2DStaticWrap o2) {
        Point2D thisPoint = o1.getPunto();
        Point2D thatPoint = o2.getPunto();

        if (thisPoint.getCoords(this.dimension) > thatPoint.getCoords(this.dimension)) {
            return 1;
        } else if (thisPoint.getCoords(this.dimension) < thatPoint.getCoords(this.dimension)) {
            return -1;
        } else {
            if (!o1.isFin() && !o2.isFin()) {
                if (o1.getId() < o2.getId()) {
                    return -1;
                } else if (o1.getId() > o2.getId()) {
                    return 1;
                } else {
                    return 0;
                }
            } else if (!o1.isFin() && o2.isFin()) {
                return -1;
            } else if (o1.isFin() && !o2.isFin()) {
                return 1;
            } else {
                if (o1.getId() < o2.getId()) {
                    return 1;
                } else if (o1.getId() > o2.getId()) {
                    return -1;
                } else {
                    return 0;
                }
            }
        }
    }
}
