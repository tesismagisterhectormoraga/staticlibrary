/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.comparators;

import cl.uchile.dcc.staticlibrary.primitives.AABB2D;
import java.util.Comparator;

/**
 * Implementa el comparador natural del AABB2D, indicando por cual coordenada
 * hay que ordenar.
 *
 * @author hmoraga
 */
public class AABB2DComparator implements Comparator<AABB2D> {
    private final int dimension;

    /**
     *
     */
    public AABB2DComparator() {
        this.dimension = 0;
    }

    /**
     *
     * @param dimension
     */
    public AABB2DComparator(int dimension) {
        this.dimension = dimension;
    }

    /**
     * Metodo que me compara en base al punto mas a la izquierda y mas abajo en
     * ambos.
     *
     * @param o1 primera caja
     * @param o2 segunda caja
     * @return
     */
    @Override
    public int compare(AABB2D o1, AABB2D o2) {
        Point2DComparator pc = new Point2DComparator(this.dimension);
        
        return pc.compare(o1.getPoints().get(0), o2.getPoints().get(0));
    }
}
