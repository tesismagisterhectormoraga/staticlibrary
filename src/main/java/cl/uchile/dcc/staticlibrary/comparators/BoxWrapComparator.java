/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.comparators;

import cl.uchile.dcc.staticlibrary.primitives.AABB2D;
import cl.uchile.dcc.staticlibrary.sap.BoxWrap;
import java.util.Comparator;

/**
 *
 * @author hmoraga
 */
public class BoxWrapComparator implements Comparator<BoxWrap> {

    private final int dimension;

    /**
     *
     */
    public BoxWrapComparator() {
        this.dimension = 0;
    }

    /**
     *
     * @param dimension
     */
    public BoxWrapComparator(int dimension) {
        this.dimension = dimension;
    }

    /**
     * Metodo que me compara en base al punto mas a la izquierda y mas abajo en
     * ambos.
     *
     * @param o1 primera caja
     * @param o2 segunda caja
     * @return
     */
    @Override
    public int compare(BoxWrap o1, BoxWrap o2) {
        return new AABB2DComparator(this.dimension).compare((AABB2D) o1.getBox(), (AABB2D) o2.getBox());
    }
}
