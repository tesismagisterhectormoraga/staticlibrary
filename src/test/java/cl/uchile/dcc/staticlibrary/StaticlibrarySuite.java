package cl.uchile.dcc.staticlibrary;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import cl.uchile.dcc.staticlibrary.comparators.ComparatorsSuite;
import cl.uchile.dcc.staticlibrary.poligons.PoligonsSuite;
import cl.uchile.dcc.staticlibrary.primitives.PrimitivesSuite;
import cl.uchile.dcc.staticlibrary.sap.SapSuite;
import cl.uchile.dcc.staticlibrary.wrappers.WrappersSuite;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author hmoraga
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({PoligonsSuite.class, SapSuite.class, PrimitivesSuite.class, ComparatorsSuite.class, WrappersSuite.class})
public class StaticlibrarySuite {

    /**
     *
     * @throws Exception
     */
    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    /**
     *
     * @throws Exception
     */
    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    /**
     *
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     *
     * @throws Exception
     */
    @After
    public void tearDown() throws Exception {
    }

}
