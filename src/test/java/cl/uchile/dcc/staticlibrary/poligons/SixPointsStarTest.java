/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.poligons;

import static java.lang.Math.sqrt;
import static java.lang.System.out;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class SixPointsStarTest {
    private Polygon2D estrella1, estrella2;

    /**
     *
     */
    public SixPointsStarTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.estrella1 = new SixPointsStar(false);
        this.estrella2 = new SixPointsStar(12, false);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     *
     */
    @Test
    public void testGetArea_1() {
        out.println("Area de una estrella de 6 puntas inscrita en una circunferencia de radio 1");
        assertEquals(sqrt(3), this.estrella1.getArea(), 0.0001);
    }

    /**
     *
     */
    @Test
    public void testGetArea_2() {
        out.println("Estrella de 6 puntas de area dada");
        assertEquals(12, this.estrella2.getArea(), 0.0001);
    }

    /**
     *
     */
    @Test
    public void testGetScale() {
        out.println("getScale");
        double scale = this.estrella1.getScale(36 * sqrt(3));
        assertEquals(6.0, scale, 0.0001);
    }

}
