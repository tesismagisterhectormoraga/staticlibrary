/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.poligons;

import cl.uchile.dcc.staticlibrary.primitives.AABB2D;
import cl.uchile.dcc.staticlibrary.primitives.Edge2D;
import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.PairDouble;
import cl.uchile.dcc.staticlibrary.primitives.PairInteger;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import static java.lang.Math.PI;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;
import static java.lang.System.out;
import java.util.ArrayList;
import static java.util.Arrays.asList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class Polygon2DTest {

    private Polygon2D poly1, poly2, poly3;
    private final List<Point2D> listaPuntos;

    /**
     *
     */
    public Polygon2DTest() {
        this.listaPuntos = new ArrayList<>();

        this.listaPuntos.add(new Point2D(1.0, 0.0));
        this.listaPuntos.add(new Point2D(2.0, 0.0));
        this.listaPuntos.add(new Point2D(1.0, 1.0));
        this.listaPuntos.add(new Point2D(-1.0, 1.0));
        this.listaPuntos.add(new Point2D(-2.0, 0.0));
        this.listaPuntos.add(new Point2D(-2.0, -2.0));
        this.listaPuntos.add(new Point2D(0.0, -1.0));
        this.listaPuntos.add(new Point2D(-1.0, -3.0));
        this.listaPuntos.add(new Point2D(1.0, -3.0));
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        // poligono 1: lista de puntos cualquiera (polígono no convexo)
        this.poly1 = new AnyPolygon2D(this.listaPuntos, false);

        // poligono 2: triangulo rotado irregular
        List<Point2D> listas = new ArrayList<>();
        listas.add(new Point2D(1, 0));
        listas.add(new Point2D(cos(5 * PI / 6), sin(5 * PI / 6)));
        listas.add(new Point2D(cos(4 * PI / 3), sin(4 * PI / 3)));

        this.poly2 = new AnyPolygon2D(listas, false);
        this.poly2.amplificar(5);

        // poligono 3: cuadrado sin rotar
        this.poly3 = new Square(false);
        this.poly3.amplificar(2);
    }

    /**
     *
     */
    @After
    public void tearDown() {
        this.poly1.clear();
        this.poly2.clear();
        this.poly3.clear();
    }

    /**
     *
     */
    @Test
    public void testGetVertices() {
        out.println("getVertices");

        Polygon2D instance = this.poly1;
        List<Point2D> expResult = this.listaPuntos;
        List<Point2D> result = instance.getVertices();
        assertEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testDesplazamiento_double_double() {
        out.println("desplazamiento");
        List<Point2D> listaVertices = new ArrayList<>();
        listaVertices.add(new Point2D(3.0, 2.0));
        listaVertices.add(new Point2D(1.0, 4.0));
        listaVertices.add(new Point2D(-1.0, 2.0));
        listaVertices.add(new Point2D(1.0, 0.0));

        double x = 1.0;
        double y = 2.0;
        Polygon2D instance = this.poly3;
        instance.desplazar(x, y);
        int i = 0;

        for (Point2D p : instance.getVertices()) {
            assertEquals(p.distanceSquaredTo(listaVertices.get(i++)), 0.0, 0.00001);
        }

    }

    /**
     *
     */
    @Test
    public void testDesplazamiento_Point2D() {
        out.println("desplazamiento");
        double x = 1.0;
        double y = 2.0;
        this.poly1.desplazar(new Point2D(x, y));
        int i = 0;

        for (Point2D p : this.poly1.getVertices()) {
            this.listaPuntos.get(i).add(new Point2D(x, y));
            assertTrue(p.distanceSquaredTo(this.listaPuntos.get(i++)) < 0.00001);
        }
    }

    /**
     *
     */
    @Test
    public void testGetEdgeList() {
        out.println("getEdgeList");
        Polygon2D instance = this.poly3;
        List<Edge2D> expResult = new ArrayList<>(this.poly3.getEdgeList());

        List<Edge2D> result = instance.getEdgeList();
        assertEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testGetBox() {
        out.println("getBox");
        Polygon2D instance = this.poly1;
        AABB2D expResult = new AABB2D(this.listaPuntos);
        AABB2D result = instance.getBox();
        assertEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testGetArea() {
        out.println("getArea");
        Polygon2D instance = this.poly1;
        double expResult = 10.0;
        double result = instance.getArea();
        assertEquals(expResult, result, 0.00001);
    }

    /**
     * Test of desplazar method, of class Polygon2D.
     */
    @Test
    public void testDesplazar_double_double() {
        out.println("desplazar");
        double x = 3.0;
        double y = -2.0;
        Polygon2D instance = new Square(false);
        instance.desplazar(x, y);
        assertEquals(instance.getVertices().get(0).distanceSquaredTo(new Point2D(4.0, -2.0)), 0.0, 0.00001);
        assertEquals(instance.getVertices().get(1).distanceSquaredTo(new Point2D(3.0, -1.0)), 0.0, 0.00001);
        assertEquals(instance.getVertices().get(2).distanceSquaredTo(new Point2D(2.0, -2.0)), 0.0, 0.00001);
        assertEquals(instance.getVertices().get(3).distanceSquaredTo(new Point2D(3.0, -3.0)), 0.0, 0.00001);
    }

    /**
     * Test of desplazar method, of class Polygon2D.
     */
    @Test
    public void testDesplazar_Point2D() {
        out.println("desplazar");
        Point2D p = new Point2D(1.0, -1.0);
        Polygon2D instance = new Triangle(false);
        instance.desplazar(p);
        assertEquals(instance.getVertices().get(0).distanceSquaredTo(new Point2D(2.0, -1.0)), 0.0, 0.00001);
        assertEquals(instance.getVertices().get(1).distanceSquaredTo(new Point2D(0.5, sqrt(3) / 2 - 1)), 0.0, 0.00001);
        assertEquals(instance.getVertices().get(2).distanceSquaredTo(new Point2D(0.5, -sqrt(3) / 2 - 1)), 0.0, 0.00001);
    }

    /**
     * Test of amplificar method, of class Polygon2D.
     */
    @Test
    public void testAmplificar() {
        out.println("amplificar");
        double scale = 2.0;
        Polygon2D instance = new Triangle(false);
        instance.amplificar(scale);
        assertEquals(instance.getArea(), 3 * sqrt(3), 0.00001);
        assertEquals(instance.getVertices().get(0).distanceSquaredTo(new Point2D(2.0, 0.0)), 0.0, 0.00001);
        assertEquals(instance.getVertices().get(1).distanceSquaredTo(new Point2D(-1.0, sqrt(3.0))), 0.0, 0.00001);
        assertEquals(instance.getVertices().get(2).distanceSquaredTo(new Point2D(-1.0, -sqrt(3.0))), 0.0, 0.00001);
    }

    /**
     * Test of getScale method, of class Polygon2D.
     */
    @Test
    public void testGetScale() {
        out.println("getScale");
        double areaTotal = 4.0;
        Polygon2D instance = new Square(true);
        double expResult = sqrt(2.0);
        double result = instance.getScale(areaTotal);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of clear method, of class Polygon2D.
     */
    @Test
    public void testClear() {
        out.println("clear");
        Polygon2D instance = new Triangle(false);
        instance.clear();
        assertTrue(instance.getVertices().isEmpty());
    }

    /**
     * Test of getListX method, of class Polygon2D.
     */
    @Test
    public void testGetListX() {
        out.println("getListX");
        Polygon2D instance = this.poly1;
        List<Double> expResult = asList(new Double[]{1.0, 2.0, 1.0, -1.0, -2.0, -2.0, 0.0, -1.0, 1.0});

        List<Double> result = instance.getListX();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListY method, of class Polygon2D.
     */
    @Test
    public void testGetListY() {
        out.println("getListY");
        Polygon2D instance = this.poly2;

        double[] expResult = new double[]{0.0, 5 * 0.5, 5 * -sqrt(3) / 2};
        double[] result = new double[instance.getListY().size()];

        for (int i = 0; i < result.length; i++) {
            result[i] = instance.getListY().get(i);
        }

        assertArrayEquals(expResult, result, 0.00001);
    }

    /**
     * Test of linearTransformation method, of class Polygon2D.
     */
    @Test
    public void testToScreenTransformation() {
        out.println("toScreenTransformation");
        List<Pair<Double>> limitesEspacioSimulacion = new ArrayList<>();
        limitesEspacioSimulacion.add(new PairDouble(-10.0, 10.0));
        limitesEspacioSimulacion.add(new PairDouble(-10.0, 10.0));

        List<Pair<Integer>> limitesEspacioGrafico = new ArrayList<>();
        limitesEspacioGrafico.add(new PairInteger(-2, 2));
        limitesEspacioGrafico.add(new PairInteger(-2, 2));

        Polygon2D instance = this.poly3;
        List<Point2D> expResult = new ArrayList<>();
        expResult.add(new Point2D(0.4, 0.0));
        expResult.add(new Point2D(0.0, -0.4));
        expResult.add(new Point2D(-0.4, 0.0));
        expResult.add(new Point2D(0.0, 0.4));

        List<Point2D> result = instance.toScreenTransformation(limitesEspacioSimulacion, limitesEspacioGrafico);

        for (int i = 0; i < result.size(); i++) {
            Point2D p = result.get(i);
            assertTrue(p.distanceSquaredTo(expResult.get(i)) <= 0.00001);
        }
    }

    public class Polygon2DImpl extends Polygon2D {

        public Polygon2DImpl() {
            super(null, false);
        }

        @Override
        public double getScale(double areaTotal) {
            return 0.0;
        }
    }
}
