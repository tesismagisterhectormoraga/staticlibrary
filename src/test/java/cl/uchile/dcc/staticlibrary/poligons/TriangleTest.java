/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.poligons;

import static java.lang.Math.sqrt;
import static java.lang.System.out;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class TriangleTest {
    private final Polygon2D triangulo1 = new Triangle(false);
    private final Polygon2D triangulo2 = new Triangle(sqrt(5), false);

    /**
     *
     */
    public TriangleTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     *
     */
    @Test
    public void testGetArea_1() {
        out.println("Area Triangulo inscrito en circulo unitario");
        assertEquals(0.75 * sqrt(3), this.triangulo1.getArea(), 0.0001);
    }

    /**
     *
     */
    @Test
    public void testGetArea_2() {
        out.println("Triangulo de un area dada");
        assertEquals(sqrt(5), this.triangulo2.getArea(), 0.0001);
    }

    /**
     * Test of getScale method, of class Triangulo.
     */
    @Test
    public void testGetScale() {
        out.println("getScale");
        double areaFinal = 40.0;
        Polygon2D instance = new Triangle(areaFinal, true);

        double expResult = 5.5490552670504229511054467768954;
        double result = instance.getScale(areaFinal);
        assertEquals(expResult, result, 0.00001);
    }

}
