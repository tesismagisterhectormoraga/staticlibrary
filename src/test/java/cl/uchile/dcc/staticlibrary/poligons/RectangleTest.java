/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.poligons;

import static java.lang.Math.atan2;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;
import static java.lang.System.out;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class RectangleTest {

    private Polygon2D rectangulo1, rectangulo2, rectangulo3, rectangulo4;

    /**
     *
     */
    public RectangleTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.rectangulo1 = new Rectangle(1.0, 1.0, false); // lados iguales, inscrito en circunferencia de radio 1
        this.rectangulo2 = new Rectangle(4.0, 3.0, false);  // 3:4:5 proporcion de los lados, area dada=5
        this.rectangulo3 = new Rectangle(5.0, 12.0, false);  // 5:12:13  proporcion de los lados, area dada=5
        this.rectangulo4 = new Rectangle(7.0, 24.0, false);  // 7:24:25  proporcion de los lados, area dada=5
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     *
     */
    @Test
    public void testGetArea_1() {
        out.println("Rectangulo inscrito en una circunferencia unitaria");
        assertEquals(2, this.rectangulo1.getArea(), 0.0001);
    }

    /**
     *
     */
    @Test
    public void testGetArea_2() {
        out.println("Rectangulo de area dada");
        assertEquals(2 * sin(2 * atan2(3, 4)), this.rectangulo2.getArea(), 0.0001);
        assertEquals(2 * sin(2 * atan2(5, 12)), this.rectangulo3.getArea(), 0.0001);
        assertEquals(2 * sin(2 * atan2(7, 24)), this.rectangulo4.getArea(), 0.0001);
    }

    /**
     *
     */
    @Test
    public void testGetScale() {
        out.println("getScale");
        double scale = this.rectangulo1.getScale(5.0);
        assertEquals(sqrt(2.5), scale, 0.00001);
        scale = this.rectangulo2.getScale(5.0);
        assertEquals(sqrt(5 / (2 * sin(2 * atan2(3, 4)))), scale, 0.00001);
        scale = this.rectangulo3.getScale(5.0);
        assertEquals(sqrt(5 / (2 * sin(2 * atan2(5, 12)))), scale, 0.00001);
        scale = this.rectangulo4.getScale(5.0);
        assertEquals(sqrt(5 / (2 * sin(2 * atan2(7, 24)))), scale, 0.00001);
    }
}
