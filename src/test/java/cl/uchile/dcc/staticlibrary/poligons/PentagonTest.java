/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.poligons;

import static java.lang.Math.PI;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;
import static java.lang.System.out;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class PentagonTest {

    Polygon2D pentagono1 = new Pentagon(false);
    Polygon2D pentagono2 = new Pentagon(5, false);

    /**
     *
     */
    public PentagonTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     *
     */
    @Test
    public void testGetArea_1() {
        out.println("Pentagono inscrito en circulo unitario");
        double area0 = 0.5 * 5 * sin(2 * PI / 5);
        assertEquals(area0, this.pentagono1.getArea(), 0.0001);
    }

    /**
     *
     */
    @Test
    public void testGetArea_2() {
        out.println("Pentagono de un area dada");
        assertEquals(5, this.pentagono2.getArea(), 0.0001);
    }

    /**
     * Test of getScale method, of class Pentagono.
     */
    @Test
    public void testGetScale() {
        out.println("getScale");
        double areaFinal = 3.0;
        Polygon2D instance = this.pentagono1;
        double expResult = sqrt(3.0 / (2.5 * sin(2 * PI / 5)));
        double result = instance.getScale(areaFinal);
        assertEquals(expResult, result, 0.00001);
    }

}
