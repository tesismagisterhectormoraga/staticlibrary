/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.poligons;

import static cl.uchile.dcc.staticlibrary.poligons.PolygonType.*;
import static java.lang.System.out;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class PolygonTypeTest {

    /**
     *
     */
    public PolygonTypeTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     *
     */
    @Test
    public void testValues() {
        out.println("values");
        PolygonType[] expResult = new PolygonType[]{TRIANGULO, CUADRADO, PENTAGONO, HEXAGONO, ESTRELLA4, ESTRELLA6, BOOMERANG};
        PolygonType[] result = values();
        assertArrayEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testValueOf() {
        out.println("valueOf");
        String name = "CUADRADO";
        PolygonType expResult = CUADRADO;
        PolygonType result = valueOf(name);
        assertEquals(expResult, result);
    }
}
