/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.poligons;

import static java.lang.Math.sqrt;
import static java.lang.System.out;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class BoomerangTest {

    Polygon2D boomerang1 = new Boomerang(true), boomerang2 = new Boomerang(5, false);

    /**
     *
     */
    public BoomerangTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     *
     */
    @Test
    public void testGetArea_1() {
        out.println("Area dentro del circulo unitario");
        assertEquals(sqrt(3) / 2, this.boomerang1.getArea(), 0.0001);
    }

    /**
     *
     */
    @Test
    public void testGetArea_2() {
        out.println("Area dada");
        assertEquals(5, this.boomerang2.getArea(), 0.0001);
    }

    /**
     * Test of getScale method, of class Boomerang.
     */
    @Test
    public void testGetScale() {
        out.println("getScale");
        double areaFinal = 5.0 * sqrt(3);
        Boomerang instance = (Boomerang) this.boomerang1;
        double expResult = sqrt(10);
        double result = instance.getScale(areaFinal);
        assertEquals(expResult, result, 0.00001);
    }

}
