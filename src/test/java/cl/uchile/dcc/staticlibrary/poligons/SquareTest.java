/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.poligons;

import static java.lang.Math.sqrt;
import static java.lang.System.out;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class SquareTest {
    private final Polygon2D cuadrado1 = new Square(false);
    private final Polygon2D cuadrado2 = new Square(3, false);

    /**
     *
     */
    public SquareTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     *
     */
    @Test
    public void testGetArea_1() {
        out.println("Cuadrado inscrito en circulo unitario");
        assertEquals(2, this.cuadrado1.getArea(), 0.0001);
    }

    /**
     *
     */
    @Test
    public void testGetArea_2() {
        out.println("Cuadrado de area dada");
        assertEquals(3, this.cuadrado2.getArea(), 0.0001);
    }

    /**
     * Test of getScale method, of class Cuadrado.
     */
    @Test
    public void testGetScale() {
        out.println("getScale");
        double areaFinal = 5.0;
        Square instance = (Square)this.cuadrado2;
        double expResult = sqrt(2.5);
        double result = instance.getScale(areaFinal);
        assertEquals(expResult, result, 0.0);
    }

}
