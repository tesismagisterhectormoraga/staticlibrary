/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.poligons;

import static java.lang.Math.sqrt;
import static java.lang.System.out;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class HexagonTest {

    Polygon2D hexagono1 = new Hexagon(true);
    Polygon2D hexagono2 = new Hexagon(6, true);

    /**
     *
     */
    public HexagonTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     *
     */
    @Test
    public void testGetArea_1() {
        out.println("Hexagono inscrito en circulo unitario");
        assertEquals(1.5 * sqrt(3), this.hexagono1.getArea(), 0.0001);
    }

    /**
     *
     */
    @Test
    public void testGetArea_2() {
        out.println("Hexagono de area dada");
        assertEquals(6, this.hexagono2.getArea(), 0.0001);
    }

    /**
     * Test of getScale method, of class Triangulo.
     */
    @Test
    public void testGetScale() {
        out.println("getScale");
        double areaFinal = 36.0;
        Polygon2D instance = new Hexagon(true);

        double expResult = sqrt(8 * sqrt(3));
        double result = instance.getScale(areaFinal);
        assertEquals(expResult, result, 0.00001);
    }

}
