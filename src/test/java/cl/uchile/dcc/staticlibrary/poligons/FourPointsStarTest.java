/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.poligons;

import static java.lang.Math.sqrt;
import static java.lang.System.out;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class FourPointsStarTest {
    private Polygon2D estrella1, estrella2;

    /**
     *
     */
    public FourPointsStarTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.estrella1 = new FourPointsStar(false);
        this.estrella2 = new FourPointsStar(4, true);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     *
     */
    @Test
    public void testGetArea_1() {
        out.println("Area de una estrella de 4 puntas inscrita en circulo unitario");
        assertEquals(35.0 / 32.0, this.estrella1.getArea(), 0.0001);
    }

    /**
     *
     */
    @Test
    public void testGetArea_2() {
        out.println("Area de una estrella de 4 puntas de area dada");
        assertEquals(4.0, this.estrella2.getArea(), 0.0001);
    }

    /**
     *
     */
    public void testGetScale() {
        out.println("getScale");
        double scale = this.estrella1.getScale(4);
        assertEquals(8 * sqrt(2 / 35), scale, 0.0001);
    }

}
