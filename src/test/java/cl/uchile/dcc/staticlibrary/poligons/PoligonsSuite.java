/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.poligons;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author hmoraga
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({TriangleTest.class, PolygonTypeTest.class, RectangleTest.class, RegularPolygonFactoryTest.class, PentagonTest.class, PolygonFactoryTest.class, HexagonTest.class, SixPointsStarTest.class, AnyPolygon2DTest.class, Polygon2DTest.class, SquareTest.class, FourPointsStarTest.class, BoomerangTest.class, CuadrilateralTest.class})
public class PoligonsSuite {

    /**
     *
     * @throws Exception
     */
    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    /**
     *
     * @throws Exception
     */
    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    /**
     *
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     *
     * @throws Exception
     */
    @After
    public void tearDown() throws Exception {
    }

}
