/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.poligons;

import static java.lang.Math.PI;
import static java.lang.Math.sqrt;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class CuadrilateralTest {
    private List<Double> angles;
    private Polygon2D cuadrilatero;

    /**
     *
     */
    public CuadrilateralTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.angles = new ArrayList<>();
        this.angles.add(PI / 6);
        this.angles.add(5 * PI / 6);
        this.angles.add(7 * PI / 6);
        this.angles.add(11 * PI / 6);

        this.cuadrilatero = new Cuadrilateral(this.angles, false);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     *
     */
    @Test
    public void testGetArea() {
        out.println("getArea");
        assertEquals(sqrt(3), this.cuadrilatero.getArea(), 0.0001);
    }

    /**
     * Test of getScale method, of class Cuadrilatero.
     */
    @Test
    public void testGetScale() {
        out.println("getScale");
        double areaTotal = 5.0;
        Polygon2D instance = this.cuadrilatero;
        double expResult = sqrt(5 / sqrt(3));
        double result = instance.getScale(areaTotal);
        assertEquals(expResult, result, 0.00001);
    }
}
