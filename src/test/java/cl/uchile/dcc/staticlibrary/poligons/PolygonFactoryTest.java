/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.poligons;

import static cl.uchile.dcc.staticlibrary.poligons.PolygonFactory.getPolygon;
import static cl.uchile.dcc.staticlibrary.poligons.PolygonType.BOOMERANG;
import static cl.uchile.dcc.staticlibrary.poligons.PolygonType.HEXAGONO;
import static cl.uchile.dcc.staticlibrary.poligons.PolygonType.TRIANGULO;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import static java.lang.Math.sqrt;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class PolygonFactoryTest {

    /**
     *
     */
    public PolygonFactoryTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getPolygon method, of class PolygonFactory.
     */
    @Test
    public void testGetPolygon_1() {
        out.println("getPolygon");
        PolygonType model = BOOMERANG;
        double areaFinal = sqrt(3) / 2;
        boolean rotate = false;
        Polygon2D expResult = new Boomerang(rotate);
        Polygon2D result = getPolygon(model, areaFinal, rotate);
        assertEquals(expResult.getVertices(), result.getVertices());
    }

    /**
     *
     */
    @Test
    public void testGetPolygon_2() {
        out.println("getPolygon");
        PolygonType model = TRIANGULO;
        double areaFinal = 0.75 * sqrt(3);
        boolean rotate = false;
        Polygon2D expResult = new Triangle(rotate);
        Polygon2D result = getPolygon(model, areaFinal, rotate);
        assertEquals(expResult.getVertices(), result.getVertices());
    }

    /*@Test
    public void testGetPolygon_3() {
        System.out.println("getPolygon");
        PolygonType model = PolygonType.RECTANGULO;
        double areaFinal = Math.sqrt(5.0);
        boolean rotate = false;
        Polygon2D expResult = new Rectangulo(12.0, 5.0, areaFinal, rotate);
        Polygon2D result = PolygonFactory.getPolygon(model, areaFinal, rotate);
        assertEquals(expResult.getArea(), result.getArea(), 0.00001);
    }*/
    /**
     * Test of getPolygon method, of class PolygonFactory.
     */
    @Test
    public void testGetPolygon() {
        out.println("getPolygon");
        PolygonType model = HEXAGONO;
        double areaFinal = 3 * sqrt(3);
        boolean rotate = false;

        List<Point2D> listaPuntos = new ArrayList<>();
        listaPuntos.add(new Point2D(sqrt(2), 0));
        listaPuntos.add(new Point2D(sqrt(6) / 2, sqrt(2) / 2));
        listaPuntos.add(new Point2D(-sqrt(6) / 2, sqrt(2) / 2));
        listaPuntos.add(new Point2D(-sqrt(2), 0));
        listaPuntos.add(new Point2D(-sqrt(6) / 2, -sqrt(2) / 2));
        listaPuntos.add(new Point2D(sqrt(6) / 2, -sqrt(2) / 2));

        Polygon2D expResult = new Hexagon(areaFinal, rotate);
        Polygon2D result = getPolygon(model, areaFinal, rotate);
        assertEquals(expResult.getArea(), result.getArea(), 0.00001);
        assertArrayEquals(expResult.listaPuntos.toArray(), result.listaPuntos.toArray());
    }

}
