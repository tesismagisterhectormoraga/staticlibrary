/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.sap;

import cl.uchile.dcc.staticlibrary.primitives.AABB2D;
import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.PairDouble;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import static org.junit.rules.ExpectedException.none;
import org.junit.rules.TemporaryFolder;

/**
 *
 * @author hmoraga
 */
public class IncrementalSAPWrapperTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    private IncrementalSAPWrapper wrsp;
    private FileWriter fw;

    /**
     *
     */
    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    /**
     *
     */
    @Rule
    public ExpectedException thrown = none();

    /**
     *
     */
    public IncrementalSAPWrapperTest() {
    }

    /**
     *
     * @throws IOException
     */
    @Before
    public void setUp() throws IOException {
        File output = this.temporaryFolder.newFile("output.txt");
        this.fw = new FileWriter(output.getPath(), true);

        this.fw.write("//frame:0" + '\n');
        this.fw.write("//time:0.0" + '\n');
        this.fw.write("//marco:(-5.0,5.0),(-5.0,5.0)" + '\n');
        this.fw.write("//dimensiones:2" + '\n');
        this.fw.write("3 1 -1 2 0 1 1" + '\n');
        this.fw.write("3 4 0 4 3 3 2" + '\n');
        this.fw.write("3 3 -3 4 -2 3 0" + '\n');
        this.fw.close();

        this.wrsp = new IncrementalSAPWrapper(output.getPath(), true);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getFrameNumber method, of class WrapperParaRealSAP.
     */
    @Test
    public void testGetFrameNumber() {
        out.println("getFrameNumber");
        IncrementalSAPWrapper instance = this.wrsp;
        int expResult = 0;
        int result = instance.getFrameNumber();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFrameNumber method, of class WrapperParaRealSAP.
     */
    @Test
    public void testSetFrameNumber() {
        out.println("setFrameNumber");
        int frameNumber = 2;
        IncrementalSAPWrapper instance = this.wrsp;
        instance.setFrameNumber(frameNumber);
        assertEquals(frameNumber, instance.getFrameNumber());
    }

    /**
     * Test of getNumDims method, of class WrapperParaRealSAP.
     */
    @Test
    public void testGetNumDims() {
        out.println("getNumDims");
        IncrementalSAPWrapper instance = this.wrsp;
        int expResult = 2;
        int result = instance.getNumDims();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNumDims method, of class WrapperParaRealSAP.
     */
    @Test
    public void testSetNumDims() {
        out.println("setNumDims");
        int numDims = 3;
        IncrementalSAPWrapper instance = this.wrsp;
        instance.setNumDims(numDims);
        assertEquals(numDims, instance.getNumDims());
    }

    /**
     * Test of getListaCajas method, of class WrapperParaRealSAP.
     */
    @Test
    public void testGetListaCajas() {
        out.println("getListaCajas");
        IncrementalSAPWrapper instance = this.wrsp;
        List<AABB2D> expResult = new ArrayList<>(3);
        List<Point2D> lista0, lista1, lista2;
        lista0 = new ArrayList<>(3);
        lista0.add(new Point2D(1, -1));
        lista0.add(new Point2D(2, 0));
        lista0.add(new Point2D(1, 1));

        lista1 = new ArrayList<>(3);
        lista1.add(new Point2D(4, 0));
        lista1.add(new Point2D(4, 3));
        lista1.add(new Point2D(3, 2));

        lista2 = new ArrayList<>(3);
        lista2.add(new Point2D(3, -3));
        lista2.add(new Point2D(4, -2));
        lista2.add(new Point2D(3, 0));

        expResult.add(new AABB2D(lista0));
        expResult.add(new AABB2D(lista1));
        expResult.add(new AABB2D(lista2));

        List<AABB2D> result = instance.getBoxesList();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListaCajas method, of class WrapperParaRealSAP.
     */
    @Test
    public void testSetListaCajas() {
        out.println("setListaCajas");
        List<AABB2D> expResult = new ArrayList<>(3);
        List<Point2D> lista0, lista1, lista2;
        lista0 = new ArrayList<>(3);
        lista0.add(new Point2D(2, -1));
        lista0.add(new Point2D(3, 0));
        lista0.add(new Point2D(2, 1));

        lista1 = new ArrayList<>(3);
        lista1.add(new Point2D(4, -1));
        lista1.add(new Point2D(4, 2));
        lista1.add(new Point2D(3, 1));

        lista2 = new ArrayList<>(3);
        lista2.add(new Point2D(3, -2));
        lista2.add(new Point2D(4, -1));
        lista2.add(new Point2D(3, 1));

        expResult.add(new AABB2D(lista0));
        expResult.add(new AABB2D(lista1));
        expResult.add(new AABB2D(lista2));

        IncrementalSAPWrapper instance = this.wrsp;
        instance.setBoxesList(expResult);
        assertEquals(expResult, instance.getBoxesList());
    }

    /**
     * Test of getAreaObjetos method, of class WrapperParaRealSAP.
     */
    @Test
    public void testGetObjectsArea() {
        out.println("getObjectsArea");
        IncrementalSAPWrapper instance = this.wrsp;
        double expResult = 4.0;
        double result = instance.getObjectsArea();
        assertEquals(expResult, result, 0.00001);
    }

    /**
     * Test of setAreaObjetos method, of class WrapperParaRealSAP.
     */
    @Test
    public void testSetObjectsArea() {
        out.println("setObjectsArea");
        List<List<Point2D>> listaPuntosObjetos = new ArrayList<>();
        IncrementalSAPWrapper instance = this.wrsp;
        instance.setObjectsArea(listaPuntosObjetos);
        assertEquals(0.0, instance.getObjectsArea(), 0.0);
    }

    /**
     * Test of getAreaSimulacion method, of class WrapperParaRealSAP.
     */
    @Test
    public void testGetSimulationArea() {
        out.println("getSimulationArea");
        IncrementalSAPWrapper instance = this.wrsp;
        double expResult = 100.0;
        double result = instance.getSimulationArea();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setAreaSimulacion method, of class WrapperParaRealSAP.
     */
    @Test
    public void testSetSimulationArea() {
        out.println("setSimulationArea");
        List<Pair<Double>> dimensiones = new ArrayList<>(2);
        dimensiones.add(new PairDouble(0.0, 5.0));
        dimensiones.add(new PairDouble(-5.0, 5.0));
        IncrementalSAPWrapper instance = this.wrsp;
        instance.setSimulationArea(dimensiones);
        double result = instance.getSimulationArea();
        assertEquals(50.0, result, 0.0);
    }

    /**
     * Test of getBoxesList method, of class IncrementalSAPWrapper.
     */
    @Test
    public void testGetBoxesList() {
        out.println("getBoxesList");
        IncrementalSAPWrapper instance = this.wrsp;
        List<AABB2D> expResult = new ArrayList<>();
        expResult.add(new AABB2D(new Point2D(1,-1), new Point2D(2,1)));
        expResult.add(new AABB2D(new Point2D(3,0), new Point2D(4,3)));
        expResult.add(new AABB2D(new Point2D(3,-3), new Point2D(4,0)));
        List<AABB2D> result = instance.getBoxesList();
        assertEquals(expResult, result);
    }

    /**
     * Test of setBoxesList method, of class IncrementalSAPWrapper.
     */
    @Test
    public void testSetBoxesList() {
        out.println("setBoxesList");
        List<AABB2D> listaCajas = null;
        IncrementalSAPWrapper instance = wrsp;
        instance.setBoxesList(listaCajas);
        assertNull(wrsp.getBoxesList());
    }
}
