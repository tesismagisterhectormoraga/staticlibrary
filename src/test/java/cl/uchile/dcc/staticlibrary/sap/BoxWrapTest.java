/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.sap;

import cl.uchile.dcc.staticlibrary.primitives.AABB2D;
import cl.uchile.dcc.staticlibrary.primitives.BV;
import cl.uchile.dcc.staticlibrary.primitives.Edge2D;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import static java.lang.System.out;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class BoxWrapTest {
    private AABB2D caja0, caja1, caja2;
    private BoxWrap boxWrapper0, boxWrapper1, boxWrapper2;

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    public BoxWrapTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.caja0 = new AABB2D(new Point2D(6.5, -1.5), 1, 1);
        this.caja1 = new AABB2D(new Point2D(3.75, -5.25), 1.5, 1.5);
        this.caja2 = new AABB2D(new Point2D(1, -6), 1, 3);

        this.boxWrapper0 = new BoxWrap(this.caja0, 1);
        this.boxWrapper1 = new BoxWrap(this.caja1, 3);
        this.boxWrapper2 = new BoxWrap(this.caja2, 0);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getBox method, of class BoxWrap.
     */
    @Test
    public void testGetBox() {
        out.println("getBox");
        BoxWrap instance = new BoxWrap(new AABB2D(new Point2D(2, -1), 3, 5), 3);
        BV expResult0 = new AABB2D(new Edge2D(new Point2D(0.5, -3.5), new Point2D(3.5, 1.5)), 1);
        BV expResult1 = new AABB2D(new Point2D(2, -1), 3, 5);
        BV result = instance.getBox();
        assertNotEquals(expResult0, result);
        assertEquals(expResult1, result);
    }

    /**
     * Test of setBox method, of class BoxWrap.
     */
    @Test
    public void testSetBox() {
        out.println("setBox");
        BV box0 = new AABB2D(new Point2D(6, 4), 3, 5);
        BV box1 = new AABB2D(new Point2D(2, -1), 3, 5);
        BoxWrap instance = new BoxWrap(box0, 3);
        instance.setBox(box1);
        BV expResult0 = new AABB2D(new Edge2D(new Point2D(0.5, -3.5), new Point2D(3.5, 1.5)), 1);
        BV expResult1 = new AABB2D(new Point2D(2, -1), 3, 5);
        BV result = instance.getBox();
        assertNotEquals(expResult0, result);
        assertEquals(expResult1, result);
    }

    /**
     * Test of getId method, of class BoxWrap.
     */
    @Test
    public void testGetId() {
        out.println("getId");
        BoxWrap instance = new BoxWrap(new AABB2D(new Point2D(2, -1), 3, 5), 3);
        int expResult = 3;
        int result = instance.getId();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setId method, of class BoxWrap.
     */
    @Test
    public void testSetId() {
        out.println("setId");
        BoxWrap instance = new BoxWrap(new AABB2D(new Point2D(2, -1), 1, 5), 0);
        int expResult = 3;
        instance.setId(3);
        assertEquals(expResult, instance.getId());
    }

    /**
     * Test of compareTo method, of class BoxWrap.
     */
    @Test
    public void testCompareTo() {
        out.println("compareTo");
        assertTrue(this.boxWrapper0.compareTo(this.boxWrapper1, 0) == 1);
        assertTrue(this.boxWrapper0.compareTo(this.boxWrapper2, 0) == 1);
        assertTrue(this.boxWrapper1.compareTo(this.boxWrapper2, 0) == 1);

        assertTrue(this.boxWrapper0.compareTo(this.boxWrapper1, 1) == 1);
        assertTrue(this.boxWrapper0.compareTo(this.boxWrapper2, 1) == 1);
        assertTrue(this.boxWrapper1.compareTo(this.boxWrapper2, 1) == 1);
    }

    /**
     * Test of clone method, of class BoxWrap.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testClone() throws Exception {
        out.println("clone");
        BV box1 = new AABB2D(new Point2D(2, -1), 3, 5);
        BV box2 = new AABB2D(new Edge2D(new Point2D(0.5, -3.5), new Point2D(3.5, 1.5)), 2);
        BoxWrap instance1 = new BoxWrap(box1, 3);
        BoxWrap instance2 = new BoxWrap(box2, 3);
        Object expResult1 = new BoxWrap(new AABB2D(new Point2D(2, -1), 3, 5), 3);
        Object expResult2 = new BoxWrap(new AABB2D(new Edge2D(new Point2D(0.5, -3.5), new Point2D(3.5, 1.5)), 2), 3);
        Object result1 = instance1.clone();
        Object result2 = instance2.clone();
        // BoxWrap: los resultados se dan por el compareTo por defecto, el cual
        // revisa las coordenadas X e Y del punto mas a la izq y mas inferior
        assertEquals(expResult1, result1);
        assertNotEquals(expResult1, result2);
        assertEquals(expResult2, result2);
        assertNotEquals(expResult2, result1);
        // AABB2D
        assertNotEquals(((BoxWrap) expResult1).getBox(), ((BoxWrap) result2).getBox());
        assertNotEquals(((BoxWrap) expResult2).getBox(), ((BoxWrap) result1).getBox());
    }

    /**
     * Test of hashCode method, of class BoxWrap.
     */
    @Test
    public void testHashCode() {
        out.println("hashCode");
        BV box1 = new AABB2D(new Point2D(2, -1), 3, 5);
        BoxWrap instance = new BoxWrap(box1, 3);
        int expResult = 0;
        int result = instance.hashCode();
        assertNotEquals(expResult, result);
    }

    /**
     * Test of equals method, of class BoxWrap.
     */
    @Test
    public void testEquals() {
        out.println("equals");
        BV box1 = new AABB2D(new Point2D(2, -1), 3, 5);
        Object obj = new BoxWrap(new AABB2D(new Point2D(2, -1), 3, 5), 3);
        BoxWrap instance = new BoxWrap(box1, 3);

        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
}
