/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.sap;

import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.PairDouble;
import cl.uchile.dcc.staticlibrary.primitives.PairInteger;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class IncrementalSweepAndPruneTest {
    private List<Pair<Double>> listaVelocidades;
    private IncrementalSweepAndPrune rsp;
    private List<List<Point2D>> listaPoligonos;
    private List<Point2D> listaPuntos0, listaPuntos1, listaPuntos2;

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    public IncrementalSweepAndPruneTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.listaPoligonos = new ArrayList<>(3);

        this.listaPuntos0 = new ArrayList<>();
        this.listaPuntos0.add(new Point2D(1, 0));
        this.listaPuntos0.add(new Point2D(2, 1));
        this.listaPuntos0.add(new Point2D(1, 3));
        this.listaPuntos0.add(new Point2D(0, 3));

        this.listaPuntos1 = new ArrayList<>();
        this.listaPuntos1.add(new Point2D(4, 4));
        this.listaPuntos1.add(new Point2D(5, 6));
        this.listaPuntos1.add(new Point2D(3, 6));

        this.listaPuntos2 = new ArrayList<>();
        this.listaPuntos2.add(new Point2D(4, -3));
        this.listaPuntos2.add(new Point2D(3, -1));
        this.listaPuntos2.add(new Point2D(1, -2));

        this.listaPoligonos.add(this.listaPuntos0);
        this.listaPoligonos.add(this.listaPuntos1);
        this.listaPoligonos.add(this.listaPuntos2);

        this.listaVelocidades = new ArrayList<>(3);
        this.listaVelocidades.add(new PairDouble(1.0, -1.0));
        this.listaVelocidades.add(new PairDouble(0.0, -1.0));
        this.listaVelocidades.add(new PairDouble(-1.0, 1.0));

        this.rsp = new IncrementalSweepAndPrune(listaPoligonos, listaVelocidades);
        //rsp.setSpeedList(listaVelocidades);
    }

    /**
     *
     */
    @After
    public void tearDown() {
        this.listaPoligonos.clear();
        //this.listaVelocidades.clear();
        this.listaPuntos0.clear();
        this.listaPuntos1.clear();
        this.listaPuntos2.clear();
    }

    /**
     * Test of getListaParesEfectivosColisionando method, of class IncrementalSP.
     */
    @Test
    public void testGetCollisionsList() {
        out.println("getColisionsList");
        List<Point2D> listaPuntos3, listaPuntos4;
        List<List<Point2D>> listaObjetos = new ArrayList<>();

        this.listaPuntos0 = new ArrayList<>();
        this.listaPuntos1 = new ArrayList<>();
        this.listaPuntos2 = new ArrayList<>();
        listaPuntos3 = new ArrayList<>();
        listaPuntos4 = new ArrayList<>();

        this.listaPuntos0.add(new Point2D(-1, -1));
        this.listaPuntos0.add(new Point2D(1, 1));
        this.listaPuntos0.add(new Point2D(-1, 1));
        listaObjetos.add(this.listaPuntos0);

        this.listaPuntos1.add(new Point2D(-2, 2));
        this.listaPuntos1.add(new Point2D(0, 0));
        this.listaPuntos1.add(new Point2D(0, 2));
        listaObjetos.add(this.listaPuntos1);

        this.listaPuntos2.add(new Point2D(0, 0));
        this.listaPuntos2.add(new Point2D(2, -2));
        this.listaPuntos2.add(new Point2D(2, 0));
        listaObjetos.add(this.listaPuntos2);

        listaPuntos3.add(new Point2D(0.5, 1.5));
        listaPuntos3.add(new Point2D(1.5, 0.5));
        listaPuntos3.add(new Point2D(0.5, 0.5));
        listaObjetos.add(listaPuntos3);

        listaPuntos4.add(new Point2D(-1.5, -0.5));
        listaPuntos4.add(new Point2D(-0.5, -1.5));
        listaPuntos4.add(new Point2D(-0.5, -0.5));
        listaObjetos.add(listaPuntos4);

        IncrementalSweepAndPrune instance = new IncrementalSweepAndPrune(listaObjetos, listaVelocidades);
        //instance.setSpeedList(listaVelocidades);
        instance.initializeStructures();
        
        List<Pair<Integer>> expResult = new ArrayList<>();
        expResult.add(new PairInteger(0, 1));
        expResult.add(new PairInteger(0, 2));
        expResult.add(new PairInteger(0, 3));
        expResult.add(new PairInteger(0, 4));
        expResult.add(new PairInteger(1, 2));

        instance.insertionSort();
        List<Pair<Integer>> result = instance.getCollisionsList();

        assertTrue(!result.isEmpty());
        assertEquals(expResult, result);
    }

    /**
     * Test of insertionSort method, of class IncrementalSweepAndPrune.
     */
    @Test
    public void testInsertionSort() {
        out.println("insertionSort");
        IncrementalSweepAndPrune instance = rsp;
        //instance.initializeStructures();
        //instance.setSpeedList(listaVelocidades);
        instance.insertionSort();
        assertTrue(instance.validateEndpointsLists());
    }

    /**
     * Test of getPairsListCollisions method, of class IncrementalSweepAndPrune.
     */
    @Test
    public void testGetPairsListCollisions() {
        out.println("getPairsListCollisions");
        IncrementalSweepAndPrune instance = new IncrementalSweepAndPrune(listaPoligonos, listaVelocidades);
        instance.initializeStructures();
        List<Pair<Integer>> expResult = new ArrayList<>();
        List<Pair<Integer>> result = instance.getCollisionsList();
        assertEquals(expResult, result);
        instance.insertionSort();
        result = instance.getCollisionsList();
        assertEquals(expResult, result);
    }
}
