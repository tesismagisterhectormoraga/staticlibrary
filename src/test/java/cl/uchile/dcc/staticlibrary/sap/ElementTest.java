/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.sap;

import static java.lang.System.out;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class ElementTest {
    
    public ElementTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getPosicion method, of class Element.
     */
    @Test
    public void testGetPosition() {
        out.println("getPosition");
        Element instance = new Element(0.1823, 0, true);
        double expResult = 0.1823;
        double result = instance.getPosition();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setPosicion method, of class Element.
     */
    @Test
    public void testSetPosition() {
        out.println("setPosition");
        double position = 0.0;
        Element instance = new Element(3.45, 3, false);
        instance.setPosition(position);
        assertEquals(instance.getPosition(), position, 0.0);
    }

    /**
     * Test of getId method, of class Element.
     */
    @Test
    public void testGetId() {
        out.println("getId");
        Element instance = new Element(3.45, 3, false);
        int expResult = 3;
        int result = instance.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setId method, of class Element.
     */
    @Test
    public void testSetId() {
        out.println("setId");
        int id = 0;
        Element instance = new Element(3.45, 3, false);
        instance.setId(id);
        assertEquals(id, instance.getId());
    }

    /**
     * Test of isIsFin method, of class Element.
     */
    @Test
    public void testIsIsFin() {
        out.println("isIsFin");
        Element instance = new Element(3.45, 3, true);
        boolean expResult = true;
        boolean result = instance.isIsFin();
        assertEquals(expResult, result);
    }

    /**
     * Test of setIsFin method, of class Element.
     */
    @Test
    public void testSetIsFin() {
        out.println("setIsFin");
        boolean isFin = false;
        Element instance = new Element(3.45, 3, true);
        instance.setIsFin(isFin);
        boolean expResult = false;
        boolean result = instance.isIsFin();
        assertEquals(expResult, result);        
    }

    /**
     * Test of toString method, of class Element.
     */
    @Test
    public void testToString() {
        out.println("toString");
        Element instance = new Element(-3.9105, 6, true);
        String expResult = "q_6=-3.9105";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of hashCode method, of class Element.
     */
    @Test
    public void testHashCode() {
        out.println("hashCode");
        Element instance = new Element(-3.9105, 6, true);
        int expResult = 0;
        int result = instance.hashCode();
        assertNotEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Element.
     */
    @Test
    public void testEquals() {
        out.println("equals");
        Object obj = new Element(-0.41, 2, true);
        Element instance0 = new Element(-0.41, 2, true);
        Element instance1 = new Element(-0.41, 1, true);
        Element instance2 = new Element(-0.42, 2, true);
        Element instance3 = new Element(-0.41, 2, false);
        boolean expResult0 = true;
        boolean expResult1 = false;
        boolean expResult2 = false;
        boolean expResult3 = false;
        boolean result0 = instance0.equals(obj);
        boolean result1 = instance1.equals(obj);
        boolean result2 = instance2.equals(obj);
        boolean result3 = instance3.equals(obj);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);
    }
    
}
