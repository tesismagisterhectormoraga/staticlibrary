/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.sap;

import cl.uchile.dcc.staticlibrary.primitives.AABB2D;
import cl.uchile.dcc.staticlibrary.primitives.Edge2D;
import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.PairDouble;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import cl.uchile.dcc.staticlibrary.wrappers.AABB2DWrapper;
import static java.lang.System.out;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class IntervalTest {
    private Interval inter1, inter2, inter3, inter4;
    
    public IntervalTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        inter1 = new Interval(new PairDouble(-2, 6.7), new AABB2DWrapper(new AABB2D(new Edge2D(new Point2D(-3.7, 5.6), new Point2D(-0.3, 7.8)), 0), 0), new PairDouble(0.5, 0));
        inter2 = new Interval(new PairDouble(0.6, 3.9), new AABB2DWrapper(new AABB2D(new Edge2D(new Point2D(-0.9, 1.6), new Point2D(2.1, 6.2)), 1), 1), new PairDouble(-0.5, 0));
        inter3 = new Interval(new PairDouble(-3.4, -3.6), new AABB2DWrapper(new AABB2D(new Edge2D(new Point2D(-2.5, -5), new Point2D(-4.3, -2.2)), 2), 2), new PairDouble(0, 0.5));
        inter4 = new Interval(new PairDouble(4.8, 7), new AABB2DWrapper(new AABB2D(new Edge2D(new Point2D(4.6, 6), new Point2D(5, 8)), 3), 3), new PairDouble(-0.5, -0.5));
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getExtremos method, of class Interval.
     */
    @Test
    public void testGetExtremos() {
        out.println("getExtremos");
        Interval instance = inter1;
        Pair<Double> expResult = new PairDouble(-2, 6.7);
        Pair<Double> result = instance.getExtremos();
        assertEquals(expResult, result);
    }

    /**
     * Test of setExtremos method, of class Interval.
     */
    @Test
    public void testSetExtremos() {
        out.println("setExtremos");
        Pair<Double> extremos = new PairDouble(-10, 10);
        Interval instance = inter1;
        instance.setExtremos(extremos);
        assertEquals(new PairDouble(-10, 10), instance.getExtremos());
    }

    /**
     * Test of getBox method, of class Interval.
     */
    @Test
    public void testGetBox() {
        out.println("getBox");
        Interval instance = inter2;
        AABB2DWrapper expResult = new AABB2DWrapper(new AABB2D(new Edge2D(new Point2D(-0.9, 1.6), new Point2D(2.1, 6.2)), 1), 1);
        AABB2DWrapper result = instance.getBox();
        assertEquals(expResult, result);
    }

    /**
     * Test of setBox method, of class Interval.
     */
    @Test
    public void testSetBox() {
        out.println("setBox");
        AABB2DWrapper box = inter2.getBox();
        Interval instance = inter3;
        instance.setBox(box);
        assertEquals(box, instance.getBox());
    }

    /**
     * Test of getVelocidad method, of class Interval.
     */
    @Test
    public void testGetVelocidad() {
        out.println("getVelocidad");
        Interval instance = inter4;
        Pair<Double> expResult = new PairDouble(-0.5, -0.5);
        Pair<Double> result = instance.getVelocidad();
        assertEquals(expResult, result);
    }

    /**
     * Test of setVelocidad method, of class Interval.
     */
    @Test
    public void testSetVelocidad() {
        out.println("setVelocidad");
        Pair<Double> velocidad = new PairDouble(0.5, 0.5);
        Interval instance = inter3;
        instance.setVelocidad(velocidad);
        Pair<Double> result = instance.getVelocidad();
        assertEquals(velocidad, result);
    }

    /**
     * Test of setPosition method, of class Interval.
     */
    @Test
    public void testSetPosition() {
        out.println("setPosition");
        double deltatime = 0.1;
        Interval instance = inter1;
        instance.setPosition(deltatime);
        assertEquals(-1.95,inter1.getExtremos().getFirst(), 0.0);
        assertEquals(6.7,inter1.getExtremos().getSecond(), 0.0);
    }

    /**
     * Test of intersection method, of class Interval.
     */
    @Test
    public void testIntersection() {
        out.println("intersection");
        Interval other = inter1;
        Interval instance0 = inter2;
        Interval instance1 = inter3;
        
        boolean expResult0 = true;
        boolean result0 = instance0.intersection(other);
        boolean expResult1 = false;
        boolean result1 = instance1.intersection(other);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of compareTo method, of class Interval.
     */
    @Test
    public void testCompareTo() {
        out.println("compareTo");
        Interval t = inter1;
        Interval instance = inter2;
        int expResult0 = 1;
        int expResult1 = -1;
        int result0 = instance.compareTo(t);
        int result1 = t.compareTo(instance);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }
    
}
