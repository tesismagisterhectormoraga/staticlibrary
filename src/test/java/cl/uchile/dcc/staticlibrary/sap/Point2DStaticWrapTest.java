/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.sap;

import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import static java.lang.System.out;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class Point2DStaticWrapTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    public Point2DStaticWrapTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getPunto method, of class Point2DStaticWrap.
     */
    @Test
    public void testGetPunto() {
        out.println("getPunto");
        Point2DStaticWrap instance = new Point2DStaticWrap(new Point2D(), 0, true);
        Point2D expResult = new Point2D();
        Point2D result = instance.getPunto();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPunto method, of class Point2DStaticWrap.
     */
    @Test
    public void testSetPunto() {
        out.println("setPunto");
        Point2D punto = new Point2D(2, -1);
        Point2DStaticWrap instance = new Point2DStaticWrap(new Point2D(), 1, false);
        assertNotEquals(punto, instance.getPunto());
        instance.setPunto(punto);
        assertEquals(punto, instance.getPunto());
    }

    /**
     * Test of getId method, of class Point2DStaticWrap.
     */
    @Test
    public void testGetId() {
        out.println("getId");
        Point2DStaticWrap instance = new Point2DStaticWrap(new Point2D(), 1, false);
        int expResult = 1;
        int result = instance.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setId method, of class Point2DStaticWrap.
     */
    @Test
    public void testSetId() {
        out.println("setId");
        int id = 3;
        Point2DStaticWrap instance = new Point2DStaticWrap(new Point2D(), 1, false);
        assertEquals(1, instance.getId());
        instance.setId(id);
        assertEquals(id, instance.getId());
    }

    /**
     * Test of isFin method, of class Point2DStaticWrap.
     */
    @Test
    public void testIsFin() {
        out.println("isFin");
        Point2DStaticWrap instance0 = new Point2DStaticWrap(new Point2D(), 1, false);
        Point2DStaticWrap instance1 = new Point2DStaticWrap(new Point2D(3, 4), 2, true);
        boolean expResult0 = false, expResult1 = true;
        boolean result0 = instance0.isFin();
        boolean result1 = instance1.isFin();
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of setFin method, of class Point2DStaticWrap.
     */
    @Test
    public void testSetFin() {
        out.println("setFin");
        boolean fin = true;
        Point2DStaticWrap instance = new Point2DStaticWrap(new Point2D(), 1, false);
        assertFalse(instance.isFin());
        instance.setFin(fin);
        assertTrue(instance.isFin());
    }

    /**
     * Test of hashCode method, of class Point2DStaticWrap.
     */
    @Test
    public void testHashCode() {
        out.println("hashCode");
        Point2DStaticWrap instance = new Point2DStaticWrap(new Point2D(1, -2), 4, false);
        int expResult = 0;
        int result = instance.hashCode();
        assertNotEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Point2DStaticWrap.
     */
    @Test
    public void testEquals() {
        out.println("equals");
        Object o0 = new Point2DStaticWrap(new Point2D(1, -2), 4, false);
        Object o1 = new Point2DStaticWrap(new Point2D(1, -2), 4, true);
        Object o2 = new Point2DStaticWrap(new Point2D(1, -2), 3, false);
        Object o3 = new Point2DStaticWrap(new Point2D(1, 2), 4, false);
        Object o4 = new Point2DStaticWrap(new Point2D(-1, -2), 4, false);
        Point2DStaticWrap instance = new Point2DStaticWrap(new Point2D(1, -2), 4, false);
        boolean expResult0 = true;
        boolean expResult1 = false;
        boolean expResult2 = false;
        boolean expResult3 = false;
        boolean expResult4 = false;
        boolean expResult5 = false;
        boolean result0 = instance.equals(o0);
        boolean result1 = instance.equals(o1);
        boolean result2 = instance.equals(o2);
        boolean result3 = instance.equals(o3);
        boolean result4 = instance.equals(o4);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);
        assertEquals(expResult4, result4);
    }

    /**
     * Test of toString method, of class Point2DStaticWrap.
     */
    @Test
    public void testToString() {
        out.println("toString");
        Point2DStaticWrap instance = new Point2DStaticWrap(new Point2D(4.5, 1.5), 8, true);
        String expResult = "q8";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
}
