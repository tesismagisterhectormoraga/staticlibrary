/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.sap;

import cl.uchile.dcc.staticlibrary.primitives.PairDouble;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import static java.lang.System.out;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class Point2DWrapTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    private Point2D p0, p1, p2, p3;
    private PairDouble v0, v1, v2, v3;
    private Point2DWrap pw0, pw1, pw2, pw3;

    /**
     *
     */
    public Point2DWrapTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.p0 = new Point2D(3, 4);
        this.p1 = new Point2D(-2, 2);
        this.p2 = new Point2D(1, -3);
        this.p3 = new Point2D(-4, -4);

        this.v0 = new PairDouble(-1.0, 1.0);
        this.v1 = new PairDouble(1.0, -1.0);
        this.v2 = new PairDouble(-1.0, -1.0);
        this.v3 = new PairDouble(1.0, 1.0);

        this.pw0 = new Point2DWrap(this.p0, 0, false);
        this.pw1 = new Point2DWrap(this.p1, 1, false);
        this.pw2 = new Point2DWrap(this.p2, 2, false);
        this.pw3 = new Point2DWrap(this.p3, 3, false);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of currentPosition method, of class Point2DWrap.
     */
    @Test
    public void testCurrentPosition() {
        out.println("currentPosition");
        double time = 1.0;
        Point2D expResult0 = new Point2D(2.0, 5.0);
        Point2D expResult1 = new Point2D(-1.0, 1.0);
        Point2D expResult2 = new Point2D(0.0, -4.0);
        Point2D expResult3 = new Point2D(-3.0, -3.0);

        Point2D result0 = this.pw0.currentPosition(this.v0, time);
        Point2D result1 = this.pw1.currentPosition(this.v1, time);
        Point2D result2 = this.pw2.currentPosition(this.v2, time);
        Point2D result3 = this.pw3.currentPosition(this.v3, time);

        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);
    }

    /**
     * Test of getPuntoInicial method, of class Point2DWrap.
     */
    @Test
    public void testGetPuntoInicial() {
        out.println("getPuntoInicial");
        Point2DWrap instance = this.pw0;
        Point2D expResult = this.p0;
        Point2D result = instance.getPuntoInicial();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPuntoInicial method, of class Point2DWrap.
     */
    @Test
    public void testSetPuntoInicial() {
        out.println("setPuntoInicial");
        Point2D puntoInicial = this.p1;
        Point2DWrap instance = this.pw0;
        instance.setPuntoInicial(puntoInicial);
        assertEquals(this.p1, instance.getPuntoInicial());
    }

    /**
     * Test of getId method, of class Point2DWrap.
     */
    @Test
    public void testGetId() {
        out.println("getId");
        Point2DWrap instance = this.pw3;
        int expResult = 3;
        int result = instance.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setId method, of class Point2DWrap.
     */
    @Test
    public void testSetId() {
        out.println("setId");
        int id = 0;
        Point2DWrap instance = this.pw3;
        instance.setId(id);
        assertEquals(id, this.pw3.getId());
    }

    /**
     * Test of isFin method, of class Point2DWrap.
     */
    @Test
    public void testIsFin() {
        out.println("isFin");
        Point2DWrap instance = this.pw1;
        boolean expResult = false;
        boolean result = instance.isFin();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFin method, of class Point2DWrap.
     */
    @Test
    public void testSetFin() {
        out.println("setFin");
        boolean fin = true;
        Point2DWrap instance = this.pw2;
        instance.setFin(fin);
        assertEquals(fin, this.pw2.isFin());
    }

}
