/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.sap;

import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.PairDouble;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import static java.lang.System.out;
import java.nio.file.Path;
import static java.nio.file.Paths.get;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

/**
 *
 * @author hmoraga
 */
public class StatisticsTest {
    private Statistics estadisticas;

    /**
     *
     */
    @Rule
    public TemporaryFolder folder= new TemporaryFolder();
    
    /**
     *
     */
    public StatisticsTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     * @throws IOException
     */
    @Before
    public void setUp() throws IOException {
        this.estadisticas = new StatisticsDirectSP(3, new PairDouble(0.0, 1.0));
        this.folder.create();
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
        this.folder.delete();
    }

    /**
     * Test of getAreaTotal method, of class Statistics.
     */
    @Test
    public void testGetTotalArea() {
        out.println("getTotalArea");
        Statistics instance = this.estadisticas;
        double expResult = 0.0;
        double result = instance.getTotalArea();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getIntervaloSimulacion method, of class Statistics.
     */
    @Test
    public void testGetSimulationInterval() {
        out.println("getSimulationInterval");
        Statistics instance = this.estadisticas;
        Pair<Double> expResult = new PairDouble(0.0, 1.0);
        Pair<Double> result = instance.getSimulationInterval();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAreaOcupadaInicial method, of class Statistics.
     */
    @Test
    public void testGetInitialOcupatedArea() {
        out.println("getInitialOcupatedArea");
        Statistics instance = this.estadisticas;
        double expResult = 0.0;
        double result = instance.getInitialOcupatedArea();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setAreaTotal method, of class Statistics.
     */
    @Test
    public void testSetTotalArea() {
        out.println("setTotalArea");
        double areaTotal = 10.0;
        Statistics instance = this.estadisticas;
        instance.setTotalArea(areaTotal);
        assertEquals(10.0, instance.getTotalArea(), 0.0);
    }

    /**
     * Test of setIntervaloSimulacion method, of class Statistics.
     */
    @Test
    public void testsetSimulationInterval() {
        out.println("setSimulationInterval");
        Pair<Double> IntervaloTiempo = new PairDouble(0.0, 3.0);
        Statistics instance = this.estadisticas;
        instance.setSimulationInterval(IntervaloTiempo);
        assertEquals(instance.getSimulationInterval(), new PairDouble(0.0, 3.0));
    }

    /**
     * Test of setAreaOcupadaInicial method, of class Statistics.
     */
    @Test
    public void testSetInitialOcupatedArea() {
        out.println("setInitialOcupatedArea");
        double areaOcupadaInicial = 40.0;
        Statistics instance = this.estadisticas;
        instance.setInitialOcupatedArea(areaOcupadaInicial);
        assertEquals(instance.getInitialOcupatedArea(), 40.0, 0.0);
    }

    /**
     * Test of getText method, of class Statistics.
     */
    @Test
    public void testGetText() {
        out.println("getText");
        Statistics instance = this.estadisticas;
        List<String> expResult = new ArrayList<>(3);
        expResult.add("CantidadObjetos:3");
        expResult.add("Area Inicial:0.0");
        expResult.add("Area Objetos:0.0");
        expResult.add("Intervalo simulacion:[0.0,1.0]");
        
        List<String> result = instance.getText();
        assertEquals(expResult, result);
    }

    /**
     * Test of escribirArchivo method, of class Statistics.
     * @throws java.lang.Exception
     */
    @Test
    public void testWriteToFile() throws Exception {
        out.println("WriteToFile");
        // Create a temporary file.
        final File tempFile = this.folder.newFile("tempFile.txt");
        final Path arch = get(tempFile.toURI());
        List<String> expResult = new ArrayList<>();
        expResult.add("CantidadObjetos:3");
        expResult.add("Area Inicial:0.0");
        expResult.add("Area Objetos:0.0");
        expResult.add("Intervalo simulacion:[0.0,1.0]");
        
        Statistics instance = this.estadisticas;
        instance.writeToFile(arch);

        // Read it from temp file
        int i=0;
        try (BufferedReader br = new BufferedReader(new FileReader(tempFile))) {
            String line;
            while ((line = br.readLine()) != null) {
                assertEquals(expResult.get(i++), line);
            }
        }   
    }

    /**
     * Test of setSimulationInterval method, of class Statistics.
     */
    @Test
    public void testSetSimulationInterval() {
        out.println("setSimulationInterval");
        Pair<Double> intervaloTiempo = new PairDouble(0, 2.5);
        Statistics instance = estadisticas;
        instance.setSimulationInterval(intervaloTiempo);
        assertEquals(intervaloTiempo, estadisticas.getSimulationInterval());
    }
}
