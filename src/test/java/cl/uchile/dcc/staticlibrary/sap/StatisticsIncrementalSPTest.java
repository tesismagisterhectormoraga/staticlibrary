/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.sap;

import cl.uchile.dcc.staticlibrary.primitives.AABB2D;
import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.PairDouble;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import java.io.File;
import java.io.IOException;
import static java.lang.System.out;
import java.nio.file.Path;
import static java.nio.file.Paths.get;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

/**
 *
 * @author hmoraga
 */
public class StatisticsIncrementalSPTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    private File file;
    private List<List<Point2D>> listaObjetos;
    private List<AABB2D> listaCajas;
    private List<Pair<Double>> listaVelocidades;
    private StatisticsIncrementalSP statistics;
    private IncrementalSweepAndPrune isp;

    /**
     *
     */
    public StatisticsIncrementalSPTest() {
    }

    /**
     *
     * @throws IOException
     */
    @Before
    public void setUp() throws IOException {
        // Creo una situacion de ejemplo
        this.file = this.folder.newFile();

        this.listaObjetos = new ArrayList<>();
        List<Point2D> obj1, obj2, obj3, obj4, obj5;

        obj1 = new ArrayList<>();
        obj2 = new ArrayList<>();
        obj3 = new ArrayList<>();
        obj4 = new ArrayList<>();
        obj5 = new ArrayList<>();

        obj1.add(new Point2D(-1, -1));
        obj1.add(new Point2D(1, 1));
        obj1.add(new Point2D(-1, 1));

        obj2.add(new Point2D(0, 0));
        obj2.add(new Point2D(0, 2));
        obj2.add(new Point2D(-2, 2));

        obj3.add(new Point2D(0, 0));
        obj3.add(new Point2D(2, -2));
        obj3.add(new Point2D(2, 0));

        obj4.add(new Point2D(0.5, 0.5));
        obj4.add(new Point2D(1.5, 0.5));
        obj4.add(new Point2D(0.5, 1.5));

        obj5.add(new Point2D(-1.5, -0.5));
        obj5.add(new Point2D(-0.5, -1.5));
        obj5.add(new Point2D(-0.5, -0.5));

        this.listaObjetos.add(obj1);
        this.listaObjetos.add(obj2);
        this.listaObjetos.add(obj3);
        this.listaObjetos.add(obj4);
        this.listaObjetos.add(obj5);

        this.listaVelocidades = new ArrayList<>(5);
        this.listaVelocidades.add(new PairDouble(0.0, 0.0));
        this.listaVelocidades.add(new PairDouble(-1.0, 0.0));
        this.listaVelocidades.add(new PairDouble(1.0, 0.0));
        this.listaVelocidades.add(new PairDouble(-1.0, -1.0));
        this.listaVelocidades.add(new PairDouble(1.0, 1.0));

        this.listaCajas = new ArrayList<>();
        this.listaCajas.add(new AABB2D(obj1));
        this.listaCajas.add(new AABB2D(obj2));
        this.listaCajas.add(new AABB2D(obj3));
        this.listaCajas.add(new AABB2D(obj4));
        this.listaCajas.add(new AABB2D(obj5));

        this.isp = new IncrementalSweepAndPrune(this.listaObjetos, this.listaVelocidades);
        isp.initializeStructures();
        this.statistics = new StatisticsIncrementalSP(this.listaCajas.size());
        this.statistics.setInitialOcupatedArea(7.0);
        this.statistics.setTotalArea(36.0);
        this.statistics.setSimulationInterval(new PairDouble(0.0, 0.2));
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of agregarEstadisticas method, of class RealSPStatistics.
     */
    @Test
    public void testAgregarEstadisticas() {
        out.println("agregarEstadisticas");
        //isp.initializeStructures(listaObjetos);
        isp.insertionSort();
        List<Pair<Integer>> colisiones = this.isp.getCollisionsList();
        
        double time = 0.0;
        StatisticsIncrementalSP instance = this.statistics;
        instance.addStatistics(time, colisiones);
        // TODO review the generated test code and remove the default call to fail.
        // buscar las estadisticas y comparar el archivo
        List<String> texto = instance.getText();
        assertTrue(texto.get(0).equals("CantidadObjetos:5"));
        assertTrue(texto.get(1).equals("Area Inicial:36.0"));
        assertTrue(texto.get(2).equals("Area Objetos:7.0"));
        assertTrue(texto.get(3).equals("t=0.0"));
        assertTrue(texto.get(4).equals("[(0,1), (0,2), (0,3), (0,4), (1,2)]"));
    }

    /**
     * Test of getAreaTotal method, of class RealSPStatistics.
     */
    @Test
    public void testGetTotalArea() {
        out.println("getTotalArea");
        StatisticsIncrementalSP instance = this.statistics;
        double expResult = 36.0;
        double result = instance.getTotalArea();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getAreaOcupadaInicial method, of class RealSPStatistics.
     */
    @Test
    public void testGetInitialOcupatedArea() {
        out.println("getInitialOcupatedArea");
        StatisticsIncrementalSP instance = this.statistics;
        double expResult = 7.0;
        double result = instance.getInitialOcupatedArea();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setAreaTotal method, of class RealSPStatistics.
     */
    @Test
    public void testSetTotalArea() {
        out.println("setTotalArea");
        double areaTotal = 1.0;
        StatisticsIncrementalSP instance = this.statistics;
        instance.setTotalArea(areaTotal);
        assertEquals(areaTotal, instance.getTotalArea(), 0.0);
    }

    /**
     * Test of setAreaOcupadaInicial method, of class RealSPStatistics.
     */
    @Test
    public void testSetInitialOcupatedArea() {
        out.println("setInitialOcupatedArea");
        double areaOcupadaInicial = 0.0;
        StatisticsIncrementalSP instance = this.statistics;
        instance.setInitialOcupatedArea(areaOcupadaInicial);
        assertTrue(areaOcupadaInicial == instance.getInitialOcupatedArea());
    }

    /**
     * Test of getText method, of class RealSPStatistics.
     */
    @Test
    public void testGetText() {
        out.println("getText");
        StatisticsIncrementalSP instance = this.statistics;
        List<String> expResult = new ArrayList<>();
        expResult.add("CantidadObjetos:5");
        expResult.add("Area Inicial:36.0");
        expResult.add("Area Objetos:7.0");
        List<String> result = instance.getText();
        assertEquals(expResult, result);
    }

    /**
     * Test of getIntervaloSimulacion method, of class StatisticsIncrementalSP.
     */
    @Test
    public void testGetIntervaloSimulacion() {
        out.println("getIntervaloSimulacion");
        StatisticsIncrementalSP instance = this.statistics;
        Pair<Double> expResult = new PairDouble(0.0, 0.2);
        Pair<Double> result = instance.getSimulationInterval();
        assertEquals(expResult, result);
    }

    /**
     * Test of setIntervaloSimulacion method, of class StatisticsIncrementalSP.
     */
    @Test
    public void testSetIntervaloSimulacion() {
        out.println("setIntervaloSimulacion");
        Pair<Double> intervaloSimulacion = new PairDouble(0.0, 1.5);
        StatisticsIncrementalSP instance = this.statistics;
        instance.setSimulationInterval(intervaloSimulacion);
        Pair<Double> result = instance.getSimulationInterval();
        assertEquals(new PairDouble(0.0, 1.5), result);
    }

    /**
     * Test of escribirArchivo method, of class StatisticsIncrementalSP.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testWriteToFile() throws Exception {
        out.println("writeToFile");
        Path arch = get(this.file.getAbsolutePath());
        StatisticsIncrementalSP instance = this.statistics;
        instance.writeToFile(arch);
    }

    /**
     * Test of addStatistics method, of class StatisticsIncrementalSP.
     */
    @Test
    public void testAddStatistics() {
        out.println("addStatistics");
        double time = 0.1;
        isp.updatePositions(time);
        List<Pair<Integer>> colisiones = new ArrayList<>(isp.getCollisionsList());
        
        StatisticsIncrementalSP instance = statistics;
        instance.addStatistics(time, colisiones);
    }

    /**
     * Test of getSimulationInterval method, of class StatisticsIncrementalSP.
     */
    @Test
    public void testGetSimulationInterval() {
        out.println("getSimulationInterval");
        StatisticsIncrementalSP instance = statistics;
        Pair<Double> expResult = new PairDouble(0.0, 0.2);
        Pair<Double> result = instance.getSimulationInterval();
        assertEquals(expResult, result);
    }

    /**
     * Test of setSimulationInterval method, of class StatisticsIncrementalSP.
     */
    @Test
    public void testSetSimulationInterval() {
        out.println("setSimulationInterval");
        Pair<Double> intervaloSimulacion = new PairDouble(5.7, 8.5);
        StatisticsIncrementalSP instance = statistics;
        instance.setSimulationInterval(intervaloSimulacion);
        assertEquals(intervaloSimulacion, instance.getSimulationInterval());
    }
}
