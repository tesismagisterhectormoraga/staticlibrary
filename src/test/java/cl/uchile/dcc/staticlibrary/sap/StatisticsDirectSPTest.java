/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.sap;

import cl.uchile.dcc.staticlibrary.primitives.AABB2D;
import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.PairDouble;
import cl.uchile.dcc.staticlibrary.primitives.PairInteger;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import java.io.File;
import java.io.IOException;
import static java.lang.System.out;
import static java.nio.file.Files.readAllLines;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

/**
 *
 * @author hmoraga
 */
public class StatisticsDirectSPTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    private File file;
    private List<List<Point2D>> listaObjetos;
    private List<AABB2D> listaCajas;
    private StatisticsDirectSP statistics;

    /**
     *
     */
    public StatisticsDirectSPTest() {
    }

    /**
     *
     * @throws IOException
     */
    @Before
    public void setUp() throws IOException {
        // Creo una situacion de ejemplo
        this.file = this.folder.newFile();

        this.listaObjetos = new ArrayList<>();
        List<Point2D> obj1, obj2, obj3, obj4, obj5;

        obj1 = new ArrayList<>();
        obj2 = new ArrayList<>();
        obj3 = new ArrayList<>();
        obj4 = new ArrayList<>();
        obj5 = new ArrayList<>();

        obj1.add(new Point2D(-1, -1));
        obj1.add(new Point2D(1, 1));
        obj1.add(new Point2D(-1, 1));

        obj2.add(new Point2D(0, 0));
        obj2.add(new Point2D(0, 2));
        obj2.add(new Point2D(-2, 2));

        obj3.add(new Point2D(0, 0));
        obj3.add(new Point2D(2, -2));
        obj3.add(new Point2D(2, 0));

        obj4.add(new Point2D(0.5, 0.5));
        obj4.add(new Point2D(1.5, 0.5));
        obj4.add(new Point2D(0.5, 1.5));

        obj5.add(new Point2D(-1.5, -0.5));
        obj5.add(new Point2D(-0.5, -1.5));
        obj5.add(new Point2D(-0.5, -0.5));

        this.listaObjetos.add(obj1);
        this.listaObjetos.add(obj2);
        this.listaObjetos.add(obj3);
        this.listaObjetos.add(obj4);
        this.listaObjetos.add(obj5);

        this.listaCajas = new ArrayList<>();
        this.listaCajas.add(new AABB2D(obj1));
        this.listaCajas.add(new AABB2D(obj2));
        this.listaCajas.add(new AABB2D(obj3));
        this.listaCajas.add(new AABB2D(obj4));
        this.listaCajas.add(new AABB2D(obj5));

        DirectSweepAndPrune2D dsp = new DirectSweepAndPrune2D(listaObjetos);
        dsp.initializeStructures(this.listaObjetos);
        this.statistics = new StatisticsDirectSP(this.listaCajas.size(), new PairDouble(0.0, 0.2));
        this.statistics.setTotalArea(36.0);
        this.statistics.setInitialOcupatedArea(7.0);
        this.statistics.addStatistics(dsp.getPairsListColliding(), 0);
    }

    /**
     *
     */
    @After
    public void tearDown() {
        this.file.deleteOnExit();
    }

    /**
     * Test of getAreaTotal method, of class StaticSAPStats.
     */
    @Test
    public void testGetTotalArea() {
        out.println("getTotalArea");
        StatisticsDirectSP instance = this.statistics;
        double expResult = 36.0;
        double result = instance.getTotalArea();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setAreaTotal method, of class StaticSAPStats.
     */
    @Test
    public void testSetTotalArea() {
        out.println("setTotalArea");
        double areaTotal = 25.0;
        StatisticsDirectSP instance = this.statistics;
        instance.setTotalArea(areaTotal);
        assertTrue(instance.getTotalArea() == areaTotal);
    }

    /**
     * Test of getAreaOcupadaInicial method, of class StaticSAPStats.
     */
    @Test
    public void testGetInitialOcupatedArea() {
        out.println("getInitialOcupatedArea");
        StatisticsDirectSP instance = this.statistics;
        double expResult = 7.0;
        double result = instance.getInitialOcupatedArea();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setAreaOcupadaInicial method, of class StaticSAPStats.
     */
    @Test
    public void testSetInitialOcupatedArea() {
        out.println("SetInitialOcupatedArea");
        double areaOcupadaInicial = 1.0;
        StatisticsDirectSP instance = this.statistics;
        instance.setInitialOcupatedArea(areaOcupadaInicial);
        assertTrue(instance.getInitialOcupatedArea() == areaOcupadaInicial);
    }

    /**
     * Test of getTiempoTotalSimulado method, of class StaticSAPStats.
     */
    @Test
    public void testGetSimulationInterval() {
        out.println("getSimulationInterval");
        StatisticsDirectSP instance = this.statistics;
        Pair<Double> expResult = new PairDouble(0.0, 0.2);
        Pair<Double> result = instance.getSimulationInterval();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTiempoTotalSimulado method, of class StaticSAPStats.
     */
    @Test
    public void testSetSimulationInterval() {
        out.println("setSimulationInterval");
        Pair<Double> IntervaloTiempo = new PairDouble(1.0, 1.5);
        StatisticsDirectSP instance = this.statistics;
        instance.setSimulationInterval(IntervaloTiempo);
        assertEquals(new PairDouble(1.0, 1.5), instance.getSimulationInterval());
    }

    /**
     * Test of agregarEstadisticas method, of class StaticSAPStats.
     */
    @Test
    public void testAddStatistics() {
        out.println("addStatistics");
        List<Pair<Integer>> colisiones = new ArrayList<>(4);
        colisiones.add(new PairInteger(0, 1));
        colisiones.add(new PairInteger(0, 2));
        colisiones.add(new PairInteger(0, 3));
        colisiones.add(new PairInteger(0, 4));

        double time = 0.1;
        StatisticsDirectSP instance = this.statistics;
        instance.addStatistics(colisiones, time);

        List<String> listaTexto = instance.getText();
        assertEquals("CantidadObjetos:5", listaTexto.get(0));
        assertEquals("Area Inicial:36.0", listaTexto.get(1));
        assertEquals("Area Objetos:7.0", listaTexto.get(2));
        assertEquals("Intervalo simulacion:[0.0,0.2]", listaTexto.get(3));
        assertEquals("t=0.0", listaTexto.get(4));
        assertEquals("[(0,1), (0,2), (0,3), (0,4), (1,2)]", listaTexto.get(5));
        assertEquals("t=0.1", listaTexto.get(6));
        assertEquals("[(0,1), (0,2), (0,3), (0,4)]", listaTexto.get(7));
    }

    /**
     * Test of getText method, of class StaticSAPStats.
     */
    @Test
    public void testGetText() {
        out.println("getText");
        StatisticsDirectSP instance = this.statistics;
        List<String> expResult = new ArrayList<>();
        expResult.add("CantidadObjetos:5");
        expResult.add("Area Inicial:36.0");
        expResult.add("Area Objetos:7.0");
        expResult.add("Intervalo simulacion:[0.0,0.2]");
        expResult.add("t=0.0");
        expResult.add("[(0,1), (0,2), (0,3), (0,4), (1,2)]");
        List<String> result = instance.getText();
        assertEquals(expResult.get(0), result.get(0));
        assertEquals(expResult.get(1), result.get(1));
        assertEquals(expResult.get(2), result.get(2));
        assertEquals(expResult.get(3), result.get(3));
        assertEquals(expResult.get(4), result.get(4));
        assertEquals(expResult.get(5), result.get(5));
    }

    /**
     * Test of getTiempoTotalSimulado method, of class StatisticsDirectSP.
     */
    @Test
    public void testGetTotalSimulatedTime() {
        out.println("getTotalSimulatedTime");
        StatisticsDirectSP instance = this.statistics;
        double expResult = 0.0;
        double result = instance.getTotalSimulatedTime();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of escribirArchivo method, of class StatisticsDirectSP.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testWriteToFile() throws Exception {
        out.println("writeToFile");
        Path arch = this.file.toPath();
        StatisticsDirectSP instance = this.statistics;
        instance.writeToFile(arch);
        List<String> listaTexto = readAllLines(arch);

        List<String> expResult = new ArrayList<>();
        expResult.add("CantidadObjetos:5");
        expResult.add("Area Inicial:36.0");
        expResult.add("Area Objetos:7.0");
        expResult.add("Intervalo simulacion:[0.0,0.2]");
        expResult.add("t=0.0");
        expResult.add("[(0,1), (0,2), (0,3), (0,4), (1,2)]");

        for (int i = 0; i < expResult.size(); i++) {
            assertEquals(expResult.get(i), listaTexto.get(i));
        }
    }
}
