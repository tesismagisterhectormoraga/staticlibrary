/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.sap;

import cl.uchile.dcc.staticlibrary.primitives.AABB2D;
import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.PairDouble;
import cl.uchile.dcc.staticlibrary.primitives.PairInteger;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import cl.uchile.dcc.staticlibrary.wrappers.AABB2DWrapper;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class DirectSweepAndPrune2DTest {

    private List<List<Point2D>> listaObjetos;
    List<Point2D> lista0, lista1, lista2, lista3, lista4;
    List<AABB2D> listaCajas;
    List<Pair<Double>> velocidadCajas;
    //private List<Point2DStaticWrap> listaX, listaY;
    private DirectSweepAndPrune2D sp2D;

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    public DirectSweepAndPrune2DTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.lista0 = new ArrayList<>(3);
        this.lista0.add(new Point2D(-1, -1));
        this.lista0.add(new Point2D(1, 1));
        this.lista0.add(new Point2D(-1, 1));

        this.lista1 = new ArrayList<>(3);
        this.lista1.add(new Point2D(0, 0));
        this.lista1.add(new Point2D(0, 2));
        this.lista1.add(new Point2D(-2, 2));

        this.lista2 = new ArrayList<>(3);
        this.lista2.add(new Point2D(2, -2));
        this.lista2.add(new Point2D(2, 0));
        this.lista2.add(new Point2D(0, 0));

        this.lista3 = new ArrayList<>(3);
        this.lista3.add(new Point2D(0.5, 0.5));
        this.lista3.add(new Point2D(1.5, 0.5));
        this.lista3.add(new Point2D(0.5, 1.5));

        this.lista4 = new ArrayList<>(3);
        this.lista4.add(new Point2D(-0.5, -1.5));
        this.lista4.add(new Point2D(-0.5, -0.5));
        this.lista4.add(new Point2D(-1.5, -0.5));

        /*listaX = new ArrayList<>(10);

        listaX.add(new Point2DStaticWrap(new Point2D(-2, 0), 1, false));
        listaX.add(new Point2DStaticWrap(new Point2D(-1.5, -1.5), 4, false));
        listaX.add(new Point2DStaticWrap(new Point2D(-1, -1), 0, false));
        listaX.add(new Point2DStaticWrap(new Point2D(-0.5, -0.5), 4, true));
        listaX.add(new Point2DStaticWrap(new Point2D(0, -2), 2, false));
        listaX.add(new Point2DStaticWrap(new Point2D(0, 2), 1, true));
        listaX.add(new Point2DStaticWrap(new Point2D(0.5, 0.5), 3, false));
        listaX.add(new Point2DStaticWrap(new Point2D(1, 1), 0, true));
        listaX.add(new Point2DStaticWrap(new Point2D(1.5, 1.5), 3, true));
        listaX.add(new Point2DStaticWrap(new Point2D(2, 0), 2, true));

        listaY = new ArrayList<>(10);

        listaY.add(new Point2DStaticWrap(new Point2D(0, -2), 2, false));
        listaY.add(new Point2DStaticWrap(new Point2D(-1.5, -1.5), 4, false));
        listaY.add(new Point2DStaticWrap(new Point2D(-1, -1), 0, false));
        listaY.add(new Point2DStaticWrap(new Point2D(-0.5, -0.5), 4, true));
        listaY.add(new Point2DStaticWrap(new Point2D(-2, 0), 1, false));
        listaY.add(new Point2DStaticWrap(new Point2D(2, 0), 2, true));
        listaY.add(new Point2DStaticWrap(new Point2D(0.5, 0.5), 3, false));
        listaY.add(new Point2DStaticWrap(new Point2D(1, 1), 0, true));
        listaY.add(new Point2DStaticWrap(new Point2D(1.5, 1.5), 3, true));
        listaY.add(new Point2DStaticWrap(new Point2D(0, 2), 1, true));*/

        this.listaObjetos = new ArrayList<>();

        this.listaObjetos.add(this.lista0);
        this.listaObjetos.add(this.lista1);
        this.listaObjetos.add(this.lista2);
        this.listaObjetos.add(this.lista3);
        this.listaObjetos.add(this.lista4);

        this.sp2D = new DirectSweepAndPrune2D(listaObjetos);
        //listaCajas = new ArrayList<>();

        /*listaObjetos.stream().forEach((obj) -> {
            listaCajas.add(new AABB2D(obj));
        });*/

        this.velocidadCajas = new ArrayList<>();
        this.velocidadCajas.add(new PairDouble(0, 0));
        this.velocidadCajas.add(new PairDouble(1, 0));
        this.velocidadCajas.add(new PairDouble(0, 1));
        this.velocidadCajas.add(new PairDouble(-1, 0));
        this.velocidadCajas.add(new PairDouble(0, -1));
        
        this.sp2D.initializeStructures(this.listaObjetos);
        this.sp2D.setSpeedList(this.velocidadCajas);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getListaParesEfectivosColisionando method, of class SAP2Dv2.
     */
    @Test
    public void testGetPairsListColliding() {
        out.println("getPairsListColliding");
        DirectSweepAndPrune2D instance = this.sp2D;

        List<Pair<Integer>> expResult = new ArrayList<>();
        expResult.add(new PairInteger(0, 1));
        expResult.add(new PairInteger(0, 2));
        expResult.add(new PairInteger(0, 3));
        expResult.add(new PairInteger(0, 4));
        expResult.add(new PairInteger(1, 2));

        List<Pair<Integer>> result = instance.getPairsListColliding();
        assertEquals(expResult, result);
    }

    /**
     * Test of inicializarEstructuras method, of class SAP2Dv2.
     */
    @Test
    public void testInitializeStructures() {
        out.println("initializeStructures");
        DirectSweepAndPrune2D instance = new DirectSweepAndPrune2D(listaObjetos);
        instance.initializeStructures(this.listaObjetos);
        List<Pair<Integer>> listaColisiones = instance.getPairsListColliding();

        List<Pair<Integer>> expResult = new ArrayList<>();
        expResult.add(new PairInteger(0, 1));
        expResult.add(new PairInteger(0, 2));
        expResult.add(new PairInteger(0, 3));
        expResult.add(new PairInteger(0, 4));
        expResult.add(new PairInteger(1, 2));

        assertArrayEquals(expResult.toArray(), listaColisiones.toArray());
    }

    /**
     * Test of getVelocidadCajas method, of class DirectSweepAndPrune2D.
     */
    @Test
    public void testGetSpeedList() {
        out.println("getSpeedList");
        DirectSweepAndPrune2D instance = this.sp2D;
        List<Pair<Double>> expResult = this.velocidadCajas;
        List<Pair<Double>> result = instance.getSpeedList();
        assertEquals(expResult, result);
    }

    /**
     * Test of setVelocidadCajas method, of class DirectSweepAndPrune2D.
     */
    @Test
    public void testSetSpeedList() {
        out.println("setSpeedList");
        List<Pair<Double>> nvaVelocidadCajas = new ArrayList<>(5);
        for (int i=0; i<5; i++) {
            nvaVelocidadCajas.add(new PairDouble(0, 0));
        }
        
        DirectSweepAndPrune2D instance = this.sp2D;
        instance.setSpeedList(nvaVelocidadCajas);
        assertEquals(this.sp2D.getSpeedList(), nvaVelocidadCajas);
    }

    /**
     * Test of getListaCajasPorDimension method, of class DirectSweepAndPrune2D.
     */
    @Test
    public void testGetListaCajasPorDimension() {
        out.println("getListaCajasPorDimension");
        DirectSweepAndPrune2D instance = this.sp2D;
        List<List<AABB2DWrapper>> expResult =new ArrayList<>(2);
        expResult.add(new ArrayList<>());
        expResult.add(new ArrayList<>());
        
        expResult.get(0).add(new AABB2DWrapper(new AABB2D(new Point2D(-1,1), 2, 2), 1));
        expResult.get(0).add(new AABB2DWrapper(new AABB2D(new Point2D(-1,-1), 1, 1), 4));
        expResult.get(0).add(new AABB2DWrapper(new AABB2D(new Point2D(0,0), 2, 2), 0));
        expResult.get(0).add(new AABB2DWrapper(new AABB2D(new Point2D(1,-1), 2, 2), 2));
        expResult.get(0).add(new AABB2DWrapper(new AABB2D(new Point2D(1,1), 1, 1), 3));
        
        expResult.get(1).add(new AABB2DWrapper(new AABB2D(new Point2D(1,-1), 2, 2), 2));
        expResult.get(1).add(new AABB2DWrapper(new AABB2D(new Point2D(-1,-1), 1, 1), 4));
        expResult.get(1).add(new AABB2DWrapper(new AABB2D(new Point2D(0,0), 2, 2), 0));
        expResult.get(1).add(new AABB2DWrapper(new AABB2D(new Point2D(-1,1), 2, 2), 1));
        expResult.get(1).add(new AABB2DWrapper(new AABB2D(new Point2D(1,1), 1, 1), 3));
        
        List<List<AABB2DWrapper>> result = instance.getBoxesListByDimension();
        assertArrayEquals(expResult.toArray(), result.toArray());
    }

    /**
     * Test of setListaCajasPorDimension method, of class DirectSweepAndPrune2D.
     */
    @Test
    public void testSetBoxesListByDimension() {
        out.println("setBoxesListByDimension()");
        List<List<AABB2DWrapper>> listaCajasPorDimension = new ArrayList<>();
        DirectSweepAndPrune2D instance = this.sp2D;
        instance.setBoxesListByDimension(listaCajasPorDimension);
        assertTrue(instance.getBoxesListByDimension().isEmpty());
    }

    /**
     * Test of insertionSort method, of class DirectSweepAndPrune2D.
     */
    @Test
    public void testInsertionSort() {
        out.println("insertionSort");
        DirectSweepAndPrune2D instance = this.sp2D;
        instance.insertionSort();

        List<List<AABB2DWrapper>> result = new ArrayList<>(2);
        result.add(new ArrayList<>());
        result.add(new ArrayList<>());        
        
        result.get(0).add(new AABB2DWrapper(new AABB2D(new Point2D(-2, 0), new Point2D(0, 2)), 1));
        result.get(0).add(new AABB2DWrapper(new AABB2D(new Point2D(-1.5, -1.5), new Point2D(-0.5, -0.5)), 4));
        result.get(0).add(new AABB2DWrapper(new AABB2D(new Point2D(-1, -1), new Point2D(1, 1)), 0));
        result.get(0).add(new AABB2DWrapper(new AABB2D(new Point2D(0, -2), new Point2D(2, 0)), 2));
        result.get(0).add(new AABB2DWrapper(new AABB2D(new Point2D(0.5, 0.5), new Point2D(1.5, 1.5)), 3));
        
        result.get(1).add(new AABB2DWrapper(new AABB2D(new Point2D(0, -2), new Point2D(2, 0)), 2));
        result.get(1).add(new AABB2DWrapper(new AABB2D(new Point2D(-1.5, -1.5), new Point2D(-0.5, -0.5)), 4));
        result.get(1).add(new AABB2DWrapper(new AABB2D(new Point2D(-1, -1), new Point2D(1, 1)), 0));
        result.get(1).add(new AABB2DWrapper(new AABB2D(new Point2D(-2, 0), new Point2D(0, 2)), 1));
        result.get(1).add(new AABB2DWrapper(new AABB2D(new Point2D(0.5, 0.5), new Point2D(1.5, 1.5)), 3));
        
        assertArrayEquals(instance.getBoxesListByDimension().get(0).toArray(), result.get(0).toArray());
        assertArrayEquals(instance.getBoxesListByDimension().get(1).toArray(), result.get(1).toArray());
    }

    /**
     * Test of updateBoxesPositions method, of class DirectSweepAndPrune2D.
     */
    @Test
    public void testUpdateBoxesPositions() {
        out.println("updateBoxesPositions");
        double timeStep = 1.0;
        List<List<AABB2DWrapper>> expResult =new ArrayList<>(2);
        expResult.add(new ArrayList<>());
        expResult.add(new ArrayList<>());
        // el orden resultante es el dado en t=0, no hay un orden previo en t=1,
        // por eso el resultado "erroneo"
        expResult.get(0).add(new AABB2DWrapper(new AABB2D(new Point2D(0,1), 2, 2), 1));
        expResult.get(0).add(new AABB2DWrapper(new AABB2D(new Point2D(-1,-2), 1, 1), 4));
        expResult.get(0).add(new AABB2DWrapper(new AABB2D(new Point2D(0,0), 2, 2), 0));
        expResult.get(0).add(new AABB2DWrapper(new AABB2D(new Point2D(1,0), 2, 2), 2));
        expResult.get(0).add(new AABB2DWrapper(new AABB2D(new Point2D(0,1), 1, 1), 3));
        
        expResult.get(1).add(new AABB2DWrapper(new AABB2D(new Point2D(1,0), 2, 2), 2));        
        expResult.get(1).add(new AABB2DWrapper(new AABB2D(new Point2D(-1,-2), 1, 1), 4));
        expResult.get(1).add(new AABB2DWrapper(new AABB2D(new Point2D(0,0), 2, 2), 0));
        expResult.get(1).add(new AABB2DWrapper(new AABB2D(new Point2D(0,1), 2, 2), 1));
        expResult.get(1).add(new AABB2DWrapper(new AABB2D(new Point2D(0,1), 1, 1), 3));

        /*        
        velocidadCajas.add(new PairDouble(0, 0));
        velocidadCajas.add(new PairDouble(1, 0));
        velocidadCajas.add(new PairDouble(0, 1));
        velocidadCajas.add(new PairDouble(-1, 0));
        velocidadCajas.add(new PairDouble(0, -1));
        */
        
        DirectSweepAndPrune2D instance = this.sp2D;
        instance.updateBoxesPositions(timeStep);
        List<List<AABB2DWrapper>> result = instance.getBoxesListByDimension();
        assertEquals(expResult.get(0).get(0), result.get(0).get(0));
        assertEquals(expResult.get(0).get(1), result.get(0).get(1));
        assertEquals(expResult.get(0).get(2), result.get(0).get(2));
        assertEquals(expResult.get(0).get(3), result.get(0).get(3));
        assertEquals(expResult.get(0).get(4), result.get(0).get(4));
        
        assertEquals(expResult.get(1).get(0), result.get(1).get(0));
        assertEquals(expResult.get(1).get(1), result.get(1).get(1));
        assertEquals(expResult.get(1).get(2), result.get(1).get(2));
        assertEquals(expResult.get(1).get(3), result.get(1).get(3));
        assertEquals(expResult.get(1).get(4), result.get(1).get(4));
    }
}
