/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.wrappers;

import cl.uchile.dcc.staticlibrary.primitives.Edge2D;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import static java.lang.System.out;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class Edge2DWrapperTest {

    /**
     *
     */
    public Edge2DWrapperTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getArista method, of class Edge2DWrapper.
     */
    @Test
    public void testGetArista() {
        out.println("getArista");
        Edge2DWrapper instance = new Edge2DWrapper(new Edge2D(new Point2D(-3, 6), new Point2D(4, -5)), 1);
        Edge2D expResult = new Edge2D(new Point2D(-3, 6), new Point2D(4, -5));
        Edge2D result = instance.getArista();
        assertEquals(expResult, result);
    }

    /**
     * Test of getId method, of class Edge2DWrapper.
     */
    @Test
    public void testGetId() {
        out.println("getId");
        Edge2DWrapper instance = new Edge2DWrapper(new Edge2D(new Point2D(-3, 6), new Point2D(4, -5)), 1);
        int expResult = 1;
        int result = instance.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of hashCode method, of class Edge2DWrapper.
     */
    @Test
    public void testHashCode() {
        out.println("hashCode");
        Edge2DWrapper instance = new Edge2DWrapper(new Edge2D(new Point2D(-3, 6), new Point2D(4, -5)), 1);
        int expResult = 0;
        int result = instance.hashCode();
        assertNotEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Edge2DWrapper.
     */
    @Test
    public void testEquals() {
        out.println("equals");
        Object obj0 = new Edge2DWrapper(new Edge2D(new Point2D(-3, 6), new Point2D(4, -5)), 1);
        Object obj1 = new Edge2DWrapper(new Edge2D(new Point2D(-3, 6), new Point2D(4, -5)), 2);
        Object obj2 = new Edge2DWrapper(new Edge2D(new Point2D(-3, 6), new Point2D(3, -5)), 1);
        Edge2DWrapper instance = new Edge2DWrapper(new Edge2D(new Point2D(-3, 6), new Point2D(4, -5)), 1);
        boolean expResult0 = true;
        boolean expResult1 = false;
        boolean expResult2 = false;
        boolean result0 = instance.equals(obj0);
        boolean result1 = instance.equals(obj1);
        boolean result2 = instance.equals(obj2);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
    }

}
