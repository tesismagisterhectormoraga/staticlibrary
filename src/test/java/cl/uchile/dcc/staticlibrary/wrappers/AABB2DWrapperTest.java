/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.wrappers;

import static java.lang.System.out;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import cl.uchile.dcc.staticlibrary.primitives.AABB2D;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;

/**
 *
 * @author hmoraga
 */
public class AABB2DWrapperTest {

    private AABB2DWrapper boxWrap;

    /**
     *
     */
    public AABB2DWrapperTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        List<Point2D> listaPuntos = new ArrayList<>();
        listaPuntos.add(new Point2D(4, 1));
        listaPuntos.add(new Point2D(7, 3));
        listaPuntos.add(new Point2D(4, 5));
        listaPuntos.add(new Point2D(1, 3));

        AABB2D box = new AABB2D(listaPuntos);
        this.boxWrap = new AABB2DWrapper(box, 0);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getBox method, of class AABB2DWrapper.
     */
    @Test
    public void testGetBox() {
        out.println("getBox");
        AABB2DWrapper instance = this.boxWrap;

        AABB2D expResult = new AABB2D(new Point2D(4, 3), 6, 4);
        AABB2D result = instance.getBox();
        assertEquals(expResult, result);
    }

    /**
     * Test of setBox method, of class AABB2DWrapper.
     */
    @Test
    public void testSetBox() {
        out.println("setBox");
        List<Point2D> listaPuntos = new ArrayList<>();
        listaPuntos.add(new Point2D(4, 0));
        listaPuntos.add(new Point2D(6, 3));
        listaPuntos.add(new Point2D(4, 6));
        listaPuntos.add(new Point2D(2, 3));

        AABB2D box = new AABB2D(listaPuntos);
        AABB2DWrapper instance = new AABB2DWrapper(box, 0);
        instance.setBox(box);
        AABB2D result = instance.getBox();
        assertEquals(box, result);
    }

    /**
     * Test of getId method, of class AABB2DWrapper.
     */
    @Test
    public void testGetId() {
        out.println("getId");
        List<Point2D> listaPuntos = new ArrayList<>();
        listaPuntos.add(new Point2D(4, 0));
        listaPuntos.add(new Point2D(6, 3));
        listaPuntos.add(new Point2D(4, 6));
        listaPuntos.add(new Point2D(2, 3));

        AABB2D box = new AABB2D(listaPuntos);
        AABB2DWrapper instance = new AABB2DWrapper(box, 5);

        int expResult = 5;
        int result = instance.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setId method, of class AABB2DWrapper.
     */
    @Test
    public void testSetId() {
        out.println("setId");
        int id = 3;
        List<Point2D> listaPuntos = new ArrayList<>();
        listaPuntos.add(new Point2D(4, 0));
        listaPuntos.add(new Point2D(6, 3));
        listaPuntos.add(new Point2D(4, 6));
        listaPuntos.add(new Point2D(2, 3));

        AABB2D box = new AABB2D(listaPuntos);
        AABB2DWrapper instance = new AABB2DWrapper(box, 5);

        instance.setId(id);
        int result = instance.getId();
        assertEquals(id, result);
    }

    /**
     * Test of hashCode method, of class AABB2DWrapper.
     */
    @Test
    public void testHashCode() {
        out.println("hashCode");
        AABB2DWrapper instance = this.boxWrap;
        int expResult = 0;
        int result = instance.hashCode();
        assertNotEquals(expResult, result);
    }

    /**
     * Test of equals method, of class AABB2DWrapper.
     */
    @Test
    public void testEquals() {
        out.println("equals");
        AABB2D box0 = new AABB2D(new Point2D(4, 3), 6, 4);
        AABB2D box1 = new AABB2D(new Point2D(6, 2), 6, 4);
        Object obj0 = new AABB2DWrapper(box1, 0);
        Object obj1 = new AABB2DWrapper(box0, 1);
        Object obj2 = new AABB2DWrapper(box0, 0);
        AABB2DWrapper instance = this.boxWrap;
        boolean expResult0 = false, expResult1 = false, expResult2 = true;
        boolean result0 = instance.equals(obj0);
        boolean result1 = instance.equals(obj1);
        boolean result2 = instance.equals(obj2);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
    }

    /**
     * Test of move method, of class AABB2DWrapper.
     */
    @Test
    public void testMove() {
        out.println("move");
        Point2D p = new Point2D(2, -1);
        AABB2DWrapper instance = this.boxWrap;
        instance.move(p);
        AABB2D expResult = new AABB2D(new Point2D(6, 2), 6, 4);
        AABB2D result = instance.getBox();
        assertEquals(expResult, result);
    }
}
