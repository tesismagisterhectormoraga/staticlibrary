/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.comparators;

import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import static java.lang.System.out;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class Point2DComparatorTest {
    private Point2D p0, p1, p2;

    /**
     *
     */
    public Point2DComparatorTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of XAxis method, of class Point2D.
     */
    @Test
    public void testCompare_1() {
        out.println("compare XAxis");
        this.p0 = new Point2D(2, 3);
        this.p1 = new Point2D(2.1, 3.1);
        this.p2 = new Point2D(2, 3);

        Point2DComparator instance = new Point2DComparator(0);
        assertEquals(instance.compare(this.p0, this.p1), -1);
        assertEquals(instance.compare(this.p1, this.p0), 1);
        assertEquals(instance.compare(this.p0, this.p2), 0);
    }

    /**
     * Comparator eje X
     */
    @Test
    public void testComparatorX() {
        out.println("Comparator X axis");
        Point2D p = new Point2D(0.5, 1.0);
        Point2D q = new Point2D(-0.5, 1.0);
        Point2D r = new Point2D(0.5, 1.0);
        Point2DComparator instance = new Point2DComparator(0);
        assertTrue(instance.compare(p, q) == 1);
        assertTrue(instance.compare(q, p) == -1);
        assertTrue(instance.compare(p, r) == 0);
    }
    
    /**
     * Test of compare method, of class Point2DComparatorYaxis.
     */
    @Test
    public void testCompare_2() {
        out.println("compare YAxis");
        this.p0 = new Point2D(2, 3);
        this.p1 = new Point2D(2.1, 3.1);
        this.p2 = new Point2D(2, 3);

        Point2DComparator instance = new Point2DComparator(1);

        assertEquals(instance.compare(this.p0, this.p1), -1);
        assertEquals(instance.compare(this.p1, this.p0), 1);
        assertEquals(instance.compare(this.p0, this.p2), 0);
    }

    /**
     * Comparator eje Y
     */
    @Test
    public void testComparatorY() {
        out.println("Comparator Y axis");
        Point2D p = new Point2D(0.5, -1.0);
        Point2D q = new Point2D(0.5, 1.0);
        Point2D r = new Point2D(0.5, -1.0);

        Point2DComparator instance = new Point2DComparator(1);

        assertTrue(instance.compare(p, q) == -1);
        assertTrue(instance.compare(q, p) == 1);
        assertTrue(instance.compare(p, r) == 0);
    }

    /**
     * Test of compare method, of class Point2DComparator.
     */
    @Test
    public void testCompare() {
        out.println("compare");
        Point2D o1 = new Point2D(0, 0);
        Point2D o2 = new Point2D(0, 0);
        Point2D o3 = new Point2D(0, 1);
        Point2D o4 = new Point2D(1, 0);
        Point2DComparator instanceX = new Point2DComparator(0);
        Point2DComparator instanceY = new Point2DComparator(1);
        
        int expResult0 = 0;
        int result0 = instanceX.compare(o1, o2);
        assertEquals(expResult0, result0);
        int expResult1 = 0;
        int result1 = instanceX.compare(o1, o3);
        assertEquals(expResult1, result1);
        int expResult2 = -1;
        int result2 = instanceX.compare(o1, o4);
        assertEquals(expResult2, result2);
        
        int expResult3 = 0;
        int result3 = instanceY.compare(o1, o2);
        assertEquals(expResult3, result3);
        int expResult4 = -1;
        int result4 = instanceY.compare(o1, o3);
        assertEquals(expResult4, result4);
        int expResult5 = 0;
        int result5 = instanceY.compare(o1, o4);
        assertEquals(expResult5, result5);
    }
    
}
