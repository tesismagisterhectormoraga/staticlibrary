/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.comparators;

import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.PairInteger;
import static java.lang.System.out;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class PairIntegerComparatorTest {

    private PairIntegerComparator cmp1, cmp2, cmp3;
    private Pair<Integer> par1, par2, par3;

    /**
     *
     */
    public PairIntegerComparatorTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.par1 = new PairInteger(0, 5);
        this.par2 = new PairInteger(5, 0);
        this.par3 = new PairInteger(2, 5);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of compare method, of class InterseccionesComparator.
     */
    @Test
    public void testCompare() {
        out.println("compare");
        Pair<Integer> o1 = new PairInteger(0, 5);
        Pair<Integer> o2 = new PairInteger(1, 5);
        Pair<Integer> o3 = new PairInteger(2, 4);

        PairIntegerComparator instance = new PairIntegerComparator();
        int expResult0 = 0;
        int expResult1 = -1;
        int expResult2 = -1;
        int expResult3 = 0;
        int expResult4 = -1;
        int expResult5 = -1;
        int expResult6 = 0;
        int expResult7 = -1;
        int expResult8 = -1;
        int expResult9 = 1;
        int expResult10 = 1;
        int expResult11 = 1;

        int result0 = instance.compare(this.par1, this.par2);
        int result1 = instance.compare(this.par1, this.par3);
        int result2 = instance.compare(this.par2, this.par3);
        int result3 = instance.compare(this.par1, o1);
        int result4 = instance.compare(this.par1, o2);
        int result5 = instance.compare(this.par1, o3);
        int result6 = instance.compare(this.par2, o1);
        int result7 = instance.compare(this.par2, o2);
        int result8 = instance.compare(this.par2, o3);
        int result9 = instance.compare(this.par3, o1);
        int result10 = instance.compare(this.par3, o2);
        int result11 = instance.compare(this.par3, o3);

        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);
        assertEquals(expResult4, result4);
        assertEquals(expResult5, result5);
        assertEquals(expResult6, result6);
        assertEquals(expResult7, result7);
        assertEquals(expResult8, result8);
        assertEquals(expResult9, result9);
        assertEquals(expResult10, result10);
        assertEquals(expResult11, result11);
    }

}
