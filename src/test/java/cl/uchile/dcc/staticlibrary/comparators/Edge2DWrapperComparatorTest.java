/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.comparators;

import cl.uchile.dcc.staticlibrary.primitives.Edge2D;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import cl.uchile.dcc.staticlibrary.wrappers.Edge2DWrapper;
import static java.lang.System.out;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class Edge2DWrapperComparatorTest {

    /**
     *
     */
    public Edge2DWrapperComparatorTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of compare method, of class Edge2DWrapperComparator.
     */
    @Test
    public void testCompare() {
        out.println("compare");
        Edge2DWrapper o0 = new Edge2DWrapper(new Edge2D(new Point2D(6, 1), new Point2D(7, 4)), 1);
        Edge2DWrapper o1 = new Edge2DWrapper(new Edge2D(new Point2D(6, -7), new Point2D(9, 1)), 3);
        
        int dimension0 = 0;
        int dimension1 = 1;
        
        Edge2DWrapperComparator instance0 = new Edge2DWrapperComparator(dimension0);
        Edge2DWrapperComparator instance1 = new Edge2DWrapperComparator(dimension1);
        
        int expResult0 = -1;
        int expResult1 = 1;
        
        int result0 = instance0.compare(o0, o1);
        int result1 = instance1.compare(o0, o1);
        
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

}
