/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.comparators;

import static java.lang.System.out;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import cl.uchile.dcc.staticlibrary.sap.Element;

/**
 *
 * @author hmoraga
 */
public class ElementComparatorTest {

    /**
     *
     */
    public ElementComparatorTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of compare method, of class ElementComparator.
     */
    @Test
    public void testCompare() {
        out.println("compare");
        ElementComparator comparator = new ElementComparator();

        Element o = new Element(3.5, 1, true);
        Element instance0 = new Element(3.5, 1, true);  //i0 = o
        Element instance1 = new Element(2.5, 1, true);  //i1 < o
        Element instance2 = new Element(3.5, 0, true);  //i2 > o
        Element instance3 = new Element(3.5, 1, false); //i3 < o
        int expResult0 = 0, expResult1 = -1, expResult2 = 1, expResult3 = -1;

        assertEquals(comparator.compare(o, instance0), expResult0);
        assertEquals(comparator.compare(instance1, o), expResult1);
        assertEquals(comparator.compare(instance2, o), expResult2);
        assertEquals(comparator.compare(instance3, o), expResult3);
        assertEquals(comparator.compare(o, instance3), -expResult3);
    }
}
