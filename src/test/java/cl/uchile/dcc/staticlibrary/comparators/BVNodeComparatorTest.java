/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.comparators;

import cl.uchile.dcc.staticlibrary.primitives.AABB2D;
import cl.uchile.dcc.staticlibrary.primitives.BVNode;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import static java.lang.System.out;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class BVNodeComparatorTest {

    /**
     *
     */
    public BVNodeComparatorTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of compare method, of class BVNodeComparator.
     */
    @Test
    public void testCompare() {
        out.println("compare");
        // usa varios parametros de comparacion
        // primero el area menor y luego los puntos X y luego los Y para
        // comparar cual es el menor

        BVNode o0 = new BVNode(new AABB2D(new Point2D(2, 2), 2, 2));
        BVNode o1 = new BVNode(new AABB2D(new Point2D(4.5, 1.5), 1, 1));
        BVNode o2 = new BVNode(new AABB2D(new Point2D(3, 3), 2, 2));
        BVNode o3 = new BVNode(new AABB2D(new Point2D(1, 3), 2, 2));

        BVNodeComparator instance = new BVNodeComparator();
        int expResult01 = 1;
        int expResult02 = -1;
        int expResult03 = 1;
        int expResult12 = -1;
        int expResult13 = -1;
        int expResult23 = 1;

        int result01 = instance.compare(o0, o1);
        int result02 = instance.compare(o0, o2);
        int result03 = instance.compare(o0, o3);
        int result12 = instance.compare(o1, o2);
        int result13 = instance.compare(o1, o3);
        int result23 = instance.compare(o2, o3);

        assertEquals(expResult01, result01);
        assertEquals(expResult02, result02);
        assertEquals(expResult03, result03);
        assertEquals(expResult12, result12);
        assertEquals(expResult13, result13);
        assertEquals(expResult23, result23);
    }

}
