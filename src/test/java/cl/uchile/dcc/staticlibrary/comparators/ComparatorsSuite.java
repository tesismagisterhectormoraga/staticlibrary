/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.comparators;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author hmoraga
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({PairIntegerComparatorTest.class, AABB2DComparatorTest.class, Edge2DWrapperComparatorTest.class, Point2DStaticWrapComparatorTest.class, AABB2DComparatorAreaTest.class, ElementComparatorTest.class, AABB2DComparatorYAxisByCenterTest.class, BoxWrapComparatorTest.class, Point2DComparatorTest.class, BVNodeComparatorTest.class, AABB2DWrapperComparatorTest.class, AABB2DComparatorXAxisByCenterTest.class})
public class ComparatorsSuite {

    /**
     *
     * @throws Exception
     */
    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    /**
     *
     * @throws Exception
     */
    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    /**
     *
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     *
     * @throws Exception
     */
    @After
    public void tearDown() throws Exception {
    }

}
