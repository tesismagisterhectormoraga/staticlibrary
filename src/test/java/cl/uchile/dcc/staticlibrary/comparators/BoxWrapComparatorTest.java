/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.comparators;

import cl.uchile.dcc.staticlibrary.primitives.AABB2D;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import cl.uchile.dcc.staticlibrary.sap.BoxWrap;
import static java.lang.System.out;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class BoxWrapComparatorTest {

    private BoxWrapComparator cmp0, cmp1, cmp2;
    private BoxWrap boxWp1, boxWp2, boxWp3;

    /**
     *
     */
    public BoxWrapComparatorTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.boxWp1 = new BoxWrap(new AABB2D(new Point2D(-1.5, 0), 1.0, 4.0), 0);
        this.boxWp2 = new BoxWrap(new AABB2D(new Point2D(3.5, 1.0), 3.0, 2.0), 1);
        this.boxWp3 = new BoxWrap(new AABB2D(new Point2D(2.0, -2.0), 2.0, 2.0), 2);
        this.cmp0 = new BoxWrapComparator();
        this.cmp1 = new BoxWrapComparator(0);
        this.cmp2 = new BoxWrapComparator(1);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of compare method, of class BoxWrapComparator.
     */
    @Test
    public void testCompare() {
        out.println("compare");
        int expResult00 = -1, expResult01 = -1, expResult02 = 1;
        int expResult10 = -1, expResult11 = -1, expResult12 = 1;
        int expResult20 = -1, expResult21 = 1, expResult22 = 1;

        int result00 = this.cmp0.compare(this.boxWp1, this.boxWp2);
        int result01 = this.cmp0.compare(this.boxWp1, this.boxWp3);
        int result02 = this.cmp0.compare(this.boxWp2, this.boxWp3);

        int result10 = this.cmp1.compare(this.boxWp1, this.boxWp2);
        int result11 = this.cmp1.compare(this.boxWp1, this.boxWp3);
        int result12 = this.cmp1.compare(this.boxWp2, this.boxWp3);

        int result20 = this.cmp2.compare(this.boxWp1, this.boxWp2);
        int result21 = this.cmp2.compare(this.boxWp1, this.boxWp3);
        int result22 = this.cmp2.compare(this.boxWp2, this.boxWp3);

        assertEquals(expResult00, result00);
        assertEquals(expResult01, result01);
        assertEquals(expResult02, result02);

        assertEquals(expResult10, result10);
        assertEquals(expResult11, result11);
        assertEquals(expResult12, result12);

        assertEquals(expResult20, result20);
        assertEquals(expResult21, result21);
        assertEquals(expResult22, result22);
    }
}
