/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.comparators;

import cl.uchile.dcc.staticlibrary.primitives.AABB2D;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import static java.lang.System.out;
import java.util.ArrayList;
import static java.util.Collections.sort;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class AABB2DComparatorXAxisByCenterTest {

    private AABB2D cajaUno, cajaDos, cajaTres, resultado;

    /**
     *
     */
    public AABB2DComparatorXAxisByCenterTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of compare method, of class AABB2DComparatorXAxis.
     */
    @Test
    public void testCompare() {
        out.println("sortByX");
        AABB2DComparatorXAxisByCenter result = new AABB2DComparatorXAxisByCenter();

        //primer escenario 
        AABB2D caja1 = new AABB2D(new Point2D(1, 1), 2, 2);
        AABB2D caja2 = new AABB2D(new Point2D(1, 1), 4, 4);
        //segundo escenario
        AABB2D caja3 = new AABB2D(new Point2D(0.5, 0.5), 1, 1);
        AABB2D caja4 = new AABB2D(new Point2D(1, 1), 2, 2);
        //tercer escenario
        AABB2D caja5 = new AABB2D(new Point2D(3, 2), 2, 2);
        AABB2D caja6 = new AABB2D(new Point2D(3, 5), 2, 2);

        assertEquals(result.compare(caja1, caja2), 0);
        assertEquals(result.compare(caja3, caja4), -1);
        assertEquals(result.compare(caja5, caja6), 0);

        this.cajaUno = new AABB2D(new Point2D(-3.0, 4.0), 4.0, 2.0);
        this.cajaDos = new AABB2D(new Point2D(3.5, 3.0), 3.0, 4.0);
        this.cajaTres = new AABB2D(new Point2D(1.0, 1.0), 4.0, 4.0);
        AABB2D cajaCuatro = new AABB2D(new Point2D(3.0, -4.0), 2.0, 2.0);
        List<AABB2D> listaCajas1 = new ArrayList<>();
        List<AABB2D> listaCajas2 = new ArrayList<>();

        listaCajas1.add(this.cajaUno);
        listaCajas1.add(this.cajaDos);
        listaCajas1.add(this.cajaTres);
        listaCajas1.add(cajaCuatro);

        listaCajas2.add(this.cajaUno);
        listaCajas2.add(this.cajaTres);
        listaCajas2.add(cajaCuatro);
        listaCajas2.add(this.cajaDos);

        sort(listaCajas1, new AABB2DComparatorXAxisByCenter());
        assertEquals(listaCajas1, listaCajas2);
    }
}
