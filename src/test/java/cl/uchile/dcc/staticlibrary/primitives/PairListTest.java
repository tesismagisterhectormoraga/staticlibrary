/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.primitives;

import static java.lang.System.out;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class PairListTest {
    
    /**
     *
     */
    public PairListTest() {
    }
    
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of agregarPar method, of class PairList.
     */
    @Test
    public void testAddPair_1() {
        out.println("agregarPar: nueva clave");
        Pair<Integer> par = new PairInteger(3, 4);
        PairList instance = new PairList();
        boolean expResult = true;
        boolean result = instance.addPair(par);
        assertEquals(expResult, result);
    }

    /**
     * Test of agregarPar method, of class PairList.
     */
    @Test
    public void testAddPair_2() {
        out.println("agregarPar: clave antigua");
        Pair<Integer> par1 = new PairInteger(3, 4);
        Pair<Integer> par2 = new PairInteger(3, 7);
        PairList instance = new PairList();
        boolean expResult = true;
        instance.addPair(par1);
        boolean result = instance.addPair(par2);
        assertEquals(expResult, result);
    }

    /**
     * Test of pairExists method, of class PairList.
     */
    @Test
    public void testPairExists_1() {
        out.println("pairExists: falso");
        Pair<Integer> par = new PairInteger(1, 5);
        PairList instance = new PairList();
        // lista pares tendrá
        //(1,2)-(1,3)-(1,4)-(2,3)-(2,4)-(2,5)
        instance.addPair(new PairInteger(1, 2));
        instance.addPair(new PairInteger(1, 3));
        instance.addPair(new PairInteger(1, 4));
        instance.addPair(new PairInteger(2, 3));
        instance.addPair(new PairInteger(2, 4));
        instance.addPair(new PairInteger(2, 5));
        
        boolean expResult = false;
        boolean result = instance.pairExists(par);
        assertEquals(expResult, result);
    }

    /**
     * Test of pairExists method, of class PairList.
     */
    @Test
    public void testPairExists_2() {
        out.println("pairExists: verdadero");
        Pair<Integer> par = new PairInteger(2, 3);
        PairList instance = new PairList();
        // lista pares tendrá
        //(1,2)-(1,3)-(1,4)-(2,3)-(2,4)-(2,5)
        instance.addPair(new PairInteger(1, 2));
        instance.addPair(new PairInteger(1, 3));
        instance.addPair(new PairInteger(1, 4));
        instance.addPair(new PairInteger(2, 3));
        instance.addPair(new PairInteger(2, 4));
        instance.addPair(new PairInteger(2, 5));

        boolean expResult = true;
        boolean result = instance.pairExists(par);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of deletePair method, of class PairList.
     */
    @Test
    public void testDeletePair_1() {
        out.println("deletePair: falso");
        Pair<Integer> par = new PairInteger(10, 13);
        PairList instance = new PairList();
        boolean expResult = false;
        boolean result = instance.deletePair(par);
        assertEquals(expResult, result);
    }

    /**
     * Test of deletePair method, of class PairList.
     */
    @Test
    public void testDeletePair_2() {
        out.println("deletePair: verdadero");
        Pair<Integer> par = new PairInteger(3, 5);
        PairList instance = new PairList();
        instance.addPair(new PairInteger(1, 4));
        instance.addPair(new PairInteger(2, 3));
        instance.addPair(new PairInteger(3, 4));
        instance.addPair(new PairInteger(3, 5));
        instance.addPair(new PairInteger(4, 6));
        
        boolean expResult = true;
        boolean result = instance.deletePair(par);
        assertEquals(expResult, result);
    }    
    
    /**
     * Test of isEmpty method, of class PairList.
     */
    @Test
    public void testIsEmpty_1() {
        out.println("isEmpty: verdadero");
        PairList instance = new PairList();
        boolean expResult = true;
        boolean result = instance.isEmpty();
        assertEquals(expResult, result);
    }

    /**
     * Test of isEmpty method, of class PairList.
     */
    @Test
    public void testIsEmpty_2() {
        out.println("isEmpty: falso");
        PairList instance = new PairList();
        instance.addPair(new PairInteger(7, 12));
        boolean expResult = false;
        boolean result = instance.isEmpty();
        assertEquals(expResult, result);
    }    
    
    /**
     * Test of size method, of class PairList.
     */
    @Test
    public void testSize_1() {
        out.println("size: empty");
        PairList instance = new PairList();
        int expResult = 0;
        int result = instance.size();
        assertEquals(expResult, result);
    }

    /**
     * Test of size method, of class PairList.
     */
    @Test
    public void testSize_2() {
        out.println("size: no vacío");
        PairList instance = new PairList();
        instance.addPair(new PairInteger(1, 2));
        instance.addPair(new PairInteger(1, 3));
        instance.addPair(new PairInteger(4, 5));
        instance.addPair(new PairInteger(4, 7));
        instance.addPair(new PairInteger(5, 6));
        
        int expResult = 5;
        int result = instance.size();
        assertEquals(expResult, result);
    }    
    
    /**
     * Test of retainAll method, of class PairList.
     */
    @Test
    public void testRetainAll() {
        out.println("retainAll");
        PairList instance = new PairList();
        // (1,2)-(1,3)-(1,4)-(1,5)-(2,3)-(2,4)-(2,5)-(3,4)-(3,5)-(4,5)
        instance.addPair(new PairInteger(1, 2));
        instance.addPair(new PairInteger(1, 3));
        instance.addPair(new PairInteger(1, 4));
        instance.addPair(new PairInteger(1, 5));
        instance.addPair(new PairInteger(2, 3));
        instance.addPair(new PairInteger(2, 4));
        instance.addPair(new PairInteger(2, 5));
        instance.addPair(new PairInteger(3, 4));
        instance.addPair(new PairInteger(3, 5));
        instance.addPair(new PairInteger(4, 5));
        
        PairList listaParcialY = new PairList();
        listaParcialY.addPair(new PairInteger(3,1));
        listaParcialY.addPair(new PairInteger(5,1));
        listaParcialY.addPair(new PairInteger(4,2));
        listaParcialY.addPair(new PairInteger(1,6));
        listaParcialY.addPair(new PairInteger(4,3));
        listaParcialY.addPair(new PairInteger(2,6));
        listaParcialY.addPair(new PairInteger(4,5));

        instance.retainAll(listaParcialY);
        // lo que queda en instance
        PairList compara = new PairList();
        compara.addPair(new PairInteger(1,3));
        compara.addPair(new PairInteger(1,5));
        compara.addPair(new PairInteger(2,4));
        compara.addPair(new PairInteger(3,4));
        compara.addPair(new PairInteger(4,5));
        assertEquals(instance, compara);
    }

    /**
     * Test of keyExists method, of class PairList.
     */
    @Test
    public void testExisteKey_1() {
        out.println("keyExists: verdadero");
        Integer key = 2;
        PairList instance = new PairList();
        instance.addPair(new PairInteger(1, 2));
        instance.addPair(new PairInteger(1, 3));
        instance.addPair(new PairInteger(1, 4));
        instance.addPair(new PairInteger(1, 5));
        instance.addPair(new PairInteger(2, 3));
        instance.addPair(new PairInteger(2, 4));
        instance.addPair(new PairInteger(4, 5));
        boolean expResult = true;
        boolean result = instance.keyExists(key);
        assertEquals(expResult, result);
    }

    /**
     * Test of keyExists method, of class PairList.
     */
    @Test
    public void testExisteKey_2() {
        out.println("keyExists: falso");
        Integer key = 3;
        PairList instance = new PairList();
        instance.addPair(new PairInteger(1, 2));
        instance.addPair(new PairInteger(1, 3));
        instance.addPair(new PairInteger(1, 4));
        instance.addPair(new PairInteger(1, 5));
        instance.addPair(new PairInteger(2, 3));
        instance.addPair(new PairInteger(2, 4));
        instance.addPair(new PairInteger(4, 5));
        boolean expResult = false;
        boolean result = instance.keyExists(key);
        assertEquals(expResult, result);
    }    
    
    /**
     * Test of obtenerLista method, of class PairList.
     */
    @Test
    public void testObtainList() {
        out.println("obtainList");
        PairList instance = new PairList();
        instance.addPair(new PairInteger(1, 2));
        instance.addPair(new PairInteger(1, 3));
        instance.addPair(new PairInteger(1, 4));
        instance.addPair(new PairInteger(1, 5));
        instance.addPair(new PairInteger(2, 3));
        instance.addPair(new PairInteger(2, 4));
        instance.addPair(new PairInteger(4, 5));

        List<Pair<Integer>> expResult = new ArrayList<>();
        expResult.add(new PairInteger(1, 2));
        expResult.add(new PairInteger(1, 3));
        expResult.add(new PairInteger(1, 4));
        expResult.add(new PairInteger(1, 5));
        expResult.add(new PairInteger(2, 3));
        expResult.add(new PairInteger(2, 4));
        expResult.add(new PairInteger(4, 5));
        
        List<Pair<Integer>> result = instance.obtainList();
        assertEquals(expResult, result);
    }

    /**
     * Test of addPair method, of class PairList.
     */
    @Test
    public void testAddPair() {
        out.println("addPair");
        Pair<Integer> par0 = new PairInteger(3, 4);
        Pair<Integer> par1 = new PairInteger(4, 3);
        PairList instance = new PairList();
        boolean expResult0 = true;
        boolean expResult1 = false;
        boolean result0 = instance.addPair(par0);
        boolean result1 = instance.addPair(par1);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of showList method, of class PairList.
     */
    @Test
    public void testShowList() {
        out.println("showList");
        PairList instance = new PairList();
        instance.addPair(new PairInteger(6, 13));
        instance.showList();
    }

    /**
     * Test of pairExists method, of class PairList.
     */
    @Test
    public void testPairExists() {
        out.println("pairExists");
        Pair<Integer> par0 = null;
        Pair<Integer> par1 = new PairInteger(4, 5);
        Pair<Integer> par2 = new PairInteger(1, 2);
        PairList instance = new PairList();
        instance.addPair(par1);
        boolean expResult0 = false;
        boolean expResult1 = true;
        boolean expResult2 = false;
        boolean result0 = instance.pairExists(par0);
        boolean result1 = instance.pairExists(par1);
        boolean result2 = instance.pairExists(par2);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
    }

    /**
     * Test of isEmpty method, of class PairList.
     */
    @Test
    public void testIsEmpty() {
        out.println("isEmpty");
        PairList instance = new PairList();
        boolean expResult0 = true;
        boolean result0 = instance.isEmpty();
        assertEquals(expResult0, result0);
        instance.addPair(new PairInteger(9, 17));
        boolean expResult1 = false;
        boolean result1 = instance.isEmpty();
        assertEquals(expResult1, result1);
    }

    /**
     * Test of size method, of class PairList.
     */
    @Test
    public void testSize() {
        out.println("size");
        PairList instance = new PairList();
        int expResult0 = 0;
        int result0 = instance.size();
        assertEquals(expResult0, result0);
        instance.addPair(new PairInteger(2, 5));
        instance.addPair(new PairInteger(3, 4));
        int expResult1 = 2;
        int result1 = instance.size();
        assertEquals(expResult1, result1);
        assertFalse(instance.addPair(new PairInteger(4, 3)));
        int expResult2 = 2;
        int result2 = instance.size();
        assertEquals(expResult2, result2);
    }

    /**
     * Test of hashCode method, of class PairList.
     */
    @Test
    public void testHashCode() {
        out.println("hashCode");
        PairList instance = new PairList();
        int expResult0 = 0;
        int result0 = instance.hashCode();
        assertNotEquals(expResult0, result0);
        instance.addPair(new PairInteger(4, 5));
        int expResult1 = 0;
        int result1 = instance.hashCode();
        assertNotEquals(expResult1, result1);
    }

    /**
     * Test of equals method, of class PairList.
     */
    @Test
    public void testEquals() {
        out.println("equals");
        Object obj0 = new PairList();
        Object obj1 = new PairList();
        ((PairList)obj1).addPair(new PairInteger(1, 4));
        PairList instance = new PairList();
        
        boolean expResult0 = true;
        boolean result0 = instance.equals(obj0);
        boolean expResult1 = false;
        boolean result1 = instance.equals(obj1);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of deletePair method, of class PairList.
     */
    @Test
    public void testDeletePair() {
        out.println("deletePair");
        Pair<Integer> par0 = new PairInteger(4, 9);
        Pair<Integer> par1 = new PairInteger(5, 8);
        PairList instance = new PairList();
        instance.addPair(new PairInteger(1, 5));
        instance.addPair(new PairInteger(2, 6));
        instance.addPair(new PairInteger(3, 7));
        instance.addPair(new PairInteger(4, 9));
        instance.addPair(new PairInteger(8, 11));        
        boolean expResult0 = true;
        boolean expResult1 = false;
        boolean result0 = instance.deletePair(par0);
        boolean result1 = instance.deletePair(par1);
        
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of keyExists method, of class PairList.
     */
    @Test
    public void testKeyExists_1() {
        out.println("keyExists false");
        Integer key = 3;
        PairList instance = new PairList();
        instance.addPair(new PairInteger(2, 7));
        instance.addPair(new PairInteger(6, 10));
        instance.addPair(new PairInteger(1, 5));
        instance.addPair(new PairInteger(1, 8));
        instance.addPair(new PairInteger(1, 3));
        boolean expResult = false;
        boolean result = instance.keyExists(key);
        assertEquals(expResult, result);
    }

    /**
     * Test of keyExists method, of class PairList.
     */
    @Test
    public void testKeyExists_2() {
        out.println("keyExists true");
        Integer key = 6;
        PairList instance = new PairList();
        instance.addPair(new PairInteger(2, 7));
        instance.addPair(new PairInteger(6, 10));
        instance.addPair(new PairInteger(1, 5));
        instance.addPair(new PairInteger(1, 8));
        instance.addPair(new PairInteger(1, 3));
        boolean expResult = true;
        boolean result = instance.keyExists(key);
        assertEquals(expResult, result);
    }    
}
