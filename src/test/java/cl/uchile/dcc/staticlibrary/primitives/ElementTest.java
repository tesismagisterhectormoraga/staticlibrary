/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.primitives;

import cl.uchile.dcc.staticlibrary.comparators.ElementComparator;
import static java.lang.System.out;
import java.util.ArrayList;
import static java.util.Collections.sort;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import cl.uchile.dcc.staticlibrary.sap.Element;

/**
 *
 * @author hmoraga
 */
public class ElementTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    public Element e0, 

    /**
     *
     */
    e1, 

    /**
     *
     */
    e2, 

    /**
     *
     */
    e3, 

    /**
     *
     */
    e4, 

    /**
     *
     */
    e5;

    /**
     *
     */
    public List<Element> listaElementos;

    /**
     *
     */
    public ElementTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.e0 = new Element(-2.0, 0, false);
        this.e1 = new Element(-1.0, 1, false);
        this.e2 = new Element(0.0, 2, false);
        this.e3 = new Element(1.0, 0, true);
        this.e4 = new Element(2.0, 1, true);
        this.e5 = new Element(3.0, 2, true);
        this.listaElementos = new ArrayList<>();
        this.listaElementos.add(this.e0);
        this.listaElementos.add(this.e1);
        this.listaElementos.add(this.e2);
        this.listaElementos.add(this.e3);
        this.listaElementos.add(this.e4);
        this.listaElementos.add(this.e5);
        sort(this.listaElementos, new ElementComparator());
    }

    /**
     *
     */
    @After
    public void tearDown() {
        this.listaElementos.clear();
    }

    /**
     *
     */
    @Test
    public void testGetPosicion() {
        out.println("getPosicion");
        Element instance0 = new Element(2.5, 0, false);
        Element instance1 = new Element(4.5, 0, true);
        double expResult0 = 2.5, expResult1 = 4.5;
        double result0 = instance0.getPosition(), result1 = instance1.getPosition();
        assertEquals(expResult0, result0, 0.0);
        assertEquals(expResult1, result1, 0.0);
    }

    /**
     *
     */
    @Test
    public void testSetPosicion() {
        out.println("setPosicion");
        double value = 3.0;
        Element instance = new Element(2.5, 0, false);
        instance.setPosition(value);
        assertEquals(instance.getPosition(), value, 0.0);
    }

    /**
     *
     */
    @Test
    public void testGetId() {
        out.println("getId");
        Element instance = new Element(2.5, 3, false);
        int expResult = 3;
        int result = instance.getId();
        assertEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testSetId() {
        out.println("setId");
        int id = 1;
        Element instance = new Element(2.5, 0, false);
        instance.setId(id);
        assertEquals(instance.getId(), id);
    }

    /**
     *
     */
    @Test
    public void testIsFin() {
        out.println("isFin");
        Element instance0 = new Element(2.5, 0, false);
        Element instance1 = new Element(2.5, 0, true);
        boolean expResult0 = false;
        boolean expResult1 = true;
        boolean result0 = instance0.isIsFin();
        boolean result1 = instance1.isIsFin();
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     *
     */
    @Test
    public void testSetFin() {
        out.println("setFin");
        boolean fin0 = true, fin1 = false;
        Element instance0 = new Element(2.5, 0, false);
        Element instance1 = new Element(-4.58, 12, true);
        instance0.setIsFin(fin0);
        instance1.setIsFin(fin1);
        assertTrue(instance0.isIsFin());
        assertFalse(instance1.isIsFin());
    }

    /**
     *
     */
    @Test
    public void testToString() {
        out.println("toString");
        Element instance0 = new Element(2.5, 0, false);
        String expResult0 = "p_0=2.5";
        String result0 = instance0.toString();
        Element instance1 = new Element(7.32, 1, true);
        String expResult1 = "q_1=7.32";
        String result1 = instance1.toString();
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     *
     */
    @Test
    public void testHashCode() {
        out.println("hashCode");
        Element instance = new Element(2.5, 0, false);
        int expResult = 0;
        int result = instance.hashCode();
        assertTrue(expResult != result);
    }

    /**
     *
     */
    @Test
    public void testEquals() {
        out.println("equals");
        Object obj = new Element(2.5, 0, false);
        Element instance0 = new Element(2.5, 0, false);
        Element instance1 = new Element(2.5, 0, true);
        Element instance2 = new Element(2.5, 1, false);
        Element instance3 = new Element(1.5, 0, false);
        boolean expResult0 = true, expResult1 = false, expResult2 = false, expResult3 = false;
        boolean result0 = instance0.equals(obj), result1 = instance1.equals(obj), result2 = instance2.equals(obj), result3 = instance3.equals(obj);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);
    }

    /*@Test
    public void testObtenerListaPares() {
        System.out.println("obtenerListaPares");
        e1 = new Element(-5, 1, false);
        e2 = new Element(-1, 1, true);
        e3 = new Element(-1, 3, false);
        e4 = new Element(0, 4, false);
        e5 = new Element(2, 4, true);
        Element e6 = new Element(2, 2, false);
        Element e7 = new Element(3, 3, true);
        Element e8 = new Element(5, 2, true);

        List<Element> listaElems = new ArrayList<>();
        listaElems.add(e8);
        listaElems.add(e2);
        listaElems.add(e6);
        listaElems.add(e4);
        listaElems.add(e5);
        listaElems.add(e3);
        listaElems.add(e7);
        listaElems.add(e1);
        Collections.sort(listaElems, new ElementComparator());

        List<Pair<Integer>> expResult = new ArrayList<>();
        expResult.add(new PairInteger(1, 3));
        expResult.add(new PairInteger(4, 3));
        expResult.add(new PairInteger(4, 2));
        expResult.add(new PairInteger(3, 2));

        List<Pair<Integer>> result = Element.obtenerListaPares(listaElems);
        assertEquals(expResult, result);
    }

    @Test
    public void testBuscarIndiceElemInicial() {
        // donde esta en el arreglo el elemento con indice x
        System.out.println("buscarIndiceElemInicial");
        e1 = new Element(-5, 1, false);
        e2 = new Element(-1, 1, true);
        e3 = new Element(-1, 3, false);
        e4 = new Element(0, 4, false);
        e5 = new Element(2, 4, true);
        Element e6 = new Element(2, 2, false);
        Element e7 = new Element(3, 3, true);
        Element e8 = new Element(5, 2, true);

        List<Element> listaElems = new ArrayList<>();
        listaElems.add(e1);
        listaElems.add(e2);
        listaElems.add(e3);
        listaElems.add(e4);
        listaElems.add(e5);
        listaElems.add(e6);
        listaElems.add(e7);
        listaElems.add(e8);

        int elemId = 3;
        int expResult = 2;
        int result = Element.buscarIndiceElemInicial(listaElems, elemId);
        assertEquals(expResult, result);
    }*/
}
