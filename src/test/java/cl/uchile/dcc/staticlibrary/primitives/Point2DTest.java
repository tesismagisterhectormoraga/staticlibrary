/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.primitives;

import static org.junit.Assert.assertEquals;
 
import static java.lang.Math.PI;
import static java.lang.Math.sqrt;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class Point2DTest {
    private Point2D p0, p1, p2, q, r, s, instance;
    private List<Point2D> listaPuntos;
    private AABB2D caja;
    
    /**
     *
     */
    public Point2DTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.listaPuntos = new ArrayList<>();
    }

    /**
     *
     */
    @After
    public void tearDown() {
        this.listaPuntos.clear();
    }

    /**
     * Test of equals method, of class Point2D.
     */
    @Test
    public void testEquals() {
        out.println("equals");
        Object obj = new Point2D(2.25, -1.75);
        this.p0 = new Point2D(2.25, -1.75);
        this.p1 = new Point2D(-2.25, -1.75);
        boolean expResult1 = true, expResult2 = false;
        boolean result1 = this.p0.equals(obj);
        boolean result2 = this.p1.equals(obj);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
    }

    /**
     * Test of hashCode method, of class Point2D.
     */
    @Test
    public void testHashCode() {
        out.println("hashCode");
        this.instance = new Point2D();
        int expResult = 0;
        int result = this.instance.hashCode();
        assertTrue(expResult != result);
    }

    /**
     * Test of getX method, of class Point2D.
     */
    @Test
    public void testGetX() {
        out.println("getX");
        this.instance = new Point2D(3.457, -2.278);
        double expResult = 3.457;
        double result = this.instance.getX();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getY method, of class Point2D.
     */
    @Test
    public void testGetY() {
        out.println("getY");
        this.instance = new Point2D(3.457, -2.278);
        double expResult = -2.278;
        double result = this.instance.getY();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of exists method, of class Point2D.
     */
    @Test
    public void testExists() {
        out.println("exists");
        this.listaPuntos.add(new Point2D(3, 4));
        this.listaPuntos.add(new Point2D(new double[]{-0.2, 4.3}));
        this.listaPuntos.add(new Point2D(-5.0, 0.23));

        Point2D instance1 = new Point2D(-5.0, 0.23);
        Point2D instance2 = new Point2D();
        boolean expResult1 = true, expResult2 = false;
        boolean result1 = instance1.exists(this.listaPuntos);
        boolean result2 = instance2.exists(this.listaPuntos);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
    }

    /**
     * Test of isInside method, of class Point2D.
     */
    @Test
    public void testIsInside() {
        out.println("isInside");
        this.p0 = new Point2D(5, 10);
        this.p1 = new Point2D(-5, 10);
        this.p2 = new Point2D(-5, -10);
        Point2D p3 = new Point2D(5, -10);

        this.listaPuntos.add(this.p0);
        this.listaPuntos.add(this.p1);
        this.listaPuntos.add(this.p2);
        this.listaPuntos.add(p3);
        caja = new AABB2D(this.listaPuntos);
        Point2D inside = new Point2D(-1, -2);
        Point2D border = new Point2D(-5, -3);
        Point2D outside = new Point2D(6, 7);

        boolean expResult1 = true, expResult2 = true, expResult3 = false;
        boolean result1 = inside.isInside(caja);
        boolean result2 = border.isInside(caja);
        boolean result3 = outside.isInside(caja);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);
    }

    /**
     * Test of add method, of class Point2D.
     */
    @Test
    public void testAdd_Point2D() {
        out.println("add");
        this.p0 = new Point2D(3.5, 2.5);
        this.instance = new Point2D(2.0, -1.0);
        this.instance.add(this.p0);
        assertEquals(this.instance.getX(), 5.5, 0.00001);
        assertEquals(this.instance.getY(), 1.5, 0.00001);
    }

    /**
     * Test of add method, of class Point2D.
     */
    @Test
    public void testAdd_double_double() {
        out.println("add");
        double x = -1.0;
        double y = 3.0;
        this.instance = new Point2D(3.5, -2.3);
        this.instance.add(x, y);
        assertEquals(this.instance.distanceTo(new Point2D(2.5, 0.7)), 0, 0.00001);
    }

    /**
     * Test of distanceTo part of superclass Point.
     */
    @Test
    public void testDistanceTo() {
        out.println("DistanceTo");
        this.p0 = new Point2D(1, 1);
        this.p1 = new Point2D(-1, -1);
        assertEquals(this.p1.distanceTo(this.p0), 2 * sqrt(2), 0.00001);
        assertEquals(this.p0.distanceTo(this.p1), 2 * sqrt(2), 0.00001);
    }

    /**
     * Test of distanceSquaredTo part of superclass Point.
     */
    @Test
    public void testDistanceSquaredTo() {
        out.println("DistanceSquaredTo");
        this.p0 = new Point2D(1, 1);
        this.p1 = new Point2D(-1, -1);
        assertEquals(this.p1.distanceSquaredTo(this.p0), 8f, 0.00001);
        assertEquals(this.p0.distanceSquaredTo(this.p1), 8f, 0.00001);
    }

    /**
     * Test of equals method, of class Point2D.
     */
    @Test
    public void testEqualsFalse() {
        out.println("equals false");
        this.p0 = new Point2D(1, 1);
        this.p1 = new Point2D(-1, -1);

        assertFalse(this.p0 == this.p1);
        assertFalse(this.p0.equals(this.p1));
        assertFalse(this.p1.equals(this.p0));
        assertNotSame(this.p0, this.p1);
    }

    /**
     * Test of equals method, of class Point2D.
     */
    @Test
    public void testEqualsTrue() {
        out.println("equals true");
        this.p0 = new Point2D(1, 1);
        this.p1 = this.p0;

        assertTrue(this.p0.equals(this.p1));
        assertTrue(this.p1.equals(this.p0));
        assertTrue(this.p0 == this.p1);
        assertSame(this.p0, this.p1);
    }

    /**
     * Test of getCoords method, of class Point
     */
    @Test
    public void testGetCoords() {
        out.println("GetCoords");
        Point2D p = new Point2D(1, -1);
        assertEquals(p.getCoords(0), p.getX(), 0.0);
        assertEquals(p.getCoords(1), p.getY(), 0.0);
        assertEquals(p.getCoords(0), 1.0, 0.0);
        assertEquals(p.getCoords(1), -1.0, 0.0);
    }

    /**
     * Test of toString method, of class Point2D.
     */
    @Test
    public void testToString() {
        out.println("toString");
        Point2D instance1 = new Point2D();
        String expResult = "(0.0,0.0)";
        String result = instance1.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of add method, of class Point2D.
     */
    @Test
    public void testAdd_Point() {
        out.println("add");
        this.instance = new Point2D(2, -1);
        Point p = new Point2D(1, 3);
        this.instance.add(p);
        assertEquals(this.instance, new Point2D(3, 2));
    }

    /**
     * Test of rotateBy method, of class Point2D.
     */
    @Test
    public void testRotateBy() {
        out.println("rotateBy");
        double angle = PI / 2;
        this.instance = new Point2D(1, 1);
        Point2D expResult = new Point2D(1, -1);
        Point2D result = this.instance.rotateBy(angle);
        assertEquals(expResult.getX(), result.getX(), 0.00001);
        assertEquals(expResult.getY(), result.getY(), 0.00001);
    }

    /**
     * Test of multipliedByScalar method, of class Point2D.
     */
    @Test
    public void testMultipliedByScalar() {
        out.println("multipliedByScalar");
        double scale = 2.0;
        this.instance = new Point2D(2.0, 3.0);
        Point2D expResult = new Point2D(4.0, 6.0);
        Point2D result = this.instance.multipliedByScalar(scale);
        assertEquals(expResult, result);
    }

    /**
     * Test of linearTransform method, of class Point2D.
     */
    @Test
    public void testLinearTransform() {
        out.println("linearTransform");
        double xmin = -10.0;
        double xmax = 10.0;
        double ymin = -10.0;
        double ymax = 10.0;
        double A = 800.0;
        double B = 600.0;
        this.instance = new Point2D(1, 3);
        Point expResult = new Point2D(440, 210);
        Point result = this.instance.linearTransform(xmin, xmax, ymin, ymax, A, B);
        assertEquals(expResult, result);
    }
}
