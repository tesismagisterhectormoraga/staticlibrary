/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.primitives;

import static java.lang.Math.sqrt;
import static java.lang.System.out;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class Edge2DTest {

    /**
     *
     */
    public Edge2DTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getPoints method, of class Edge2D.
     */
    @Test
    public void testGetPoints() {
        out.println("getPoints");
        Edge2D instance = new Edge2D(new Point2D(-1.5, 2.3), new Point2D(0.87, 1.03));
        Point2D[] expResult = new Point2D[]{new Point2D(-1.5, 2.3), new Point2D(0.87, 1.03)};
        assertArrayEquals(expResult, instance.getPoints());
    }

    /**
     * Test of signed2DTriArea method, of class Edge2D.
     */
    @Test
    public void testSigned2DTriArea_Point2D() {
        out.println("signed2DTriArea");
        Point2D c1 = new Point2D(4, 10);
        Point2D c2 = new Point2D(5, 6);
        Point2D c3 = new Point2D(7, 8.2f);

        Edge2D instance = new Edge2D(new Point2D(3, 5), new Point2D(8, 9));
        double expResult1 = 21.0, expResult2 = -3, expResult3 = 0.0;
        double result1 = instance.signed2DTriArea(c1);
        double result2 = instance.signed2DTriArea(c2);
        double result3 = instance.signed2DTriArea(c3);

        assertEquals(expResult1, result1, 0.0);
        assertEquals(expResult2, result2, 0.0);
        assertEquals(expResult3, result3, 0.00001);
    }

    /**
     * Test of signed2DTriArea method, of class Edge2D.
     */
    @Test
    public void testSigned2DTriArea_Point() {
        out.println("signed2DTriArea");
        Point c1 = new Point2D(4, 10);
        Point c2 = new Point2D(5, 6);
        Point c3 = new Point2D(7, 8.2f);

        Edge2D instance = new Edge2D(new Point2D(3, 5), new Point2D(8, 9));
        double expResult1 = 21.0, expResult2 = -3, expResult3 = 0.0;
        double result1 = instance.signed2DTriArea(c1);
        double result2 = instance.signed2DTriArea(c2);
        double result3 = instance.signed2DTriArea(c3);

        assertEquals(expResult1, result1, 0.0);
        assertEquals(expResult2, result2, 0.0);
        assertEquals(expResult3, result3, 0.00001);
    }

    /**
     * Test of intersects method, of class Edge2D.
     */
    @Test
    public void testIntersects_Edge2D() {
        out.println("intersects");
        Edge2D arista = new Edge2D(new Point2D(-5, 2), new Point2D(-1, 2));
        Edge2D instance1 = new Edge2D(new Point2D(-1, 2), new Point2D(-1, -2));
        Edge2D instance2 = new Edge2D(new Point2D(-3, 3), new Point2D(-2, 1));
        Edge2D instance3 = new Edge2D(new Point2D(-2, 5), new Point2D(0, 5));
        boolean expResult1 = true, expResult2 = true, expResult3 = false;
        boolean result1 = instance1.intersects(arista), result2 = instance2.intersects(arista), result3 = instance3.intersects(arista);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);
    }

    /**
     * Test of intersects method, of class Edge2D.
     */
    @Test
    public void testIntersects_Edge() {
        out.println("intersects");
        Edge arista = new Edge2D(new Point2D(-1, 2), new Point2D(-1, -2));
        Edge2D instance1 = new Edge2D(new Point2D(-2, 1), new Point2D(0, 1));
        Edge2D instance2 = new Edge2D(new Point2D(-3, 3), new Point2D(-2, 5));
        boolean expResult1 = true, expResult2 = false;
        boolean result1 = instance1.intersects(arista);
        boolean result2 = instance2.intersects(arista);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
    }

    /**
     * Test of getDistance method, of class Edge2D.
     */
    @Test
    public void testGetDistance() {
        out.println("getDistance");
        Edge2D instance = new Edge2D(new Point2D(1, 1), new Point2D(-1, -1));
        double expResult = 2 * sqrt(2);
        double result = instance.getDistance();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of toString method, of class Edge2D.
     */
    @Test
    public void testToString() {
        out.println("toString");
        Edge2D instance = new Edge2D(new Point2D(-1, 0), new Point2D());
        String expResult = "Edge2D{" + "a=(-1.0,0.0), b=(0.0,0.0)}";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Edge2D.
     */
    @Test
    public void testEquals() {
        out.println("equals");
        Object obj = new Edge2D(new Point2D(-1, 1), new Point2D(1, -1));
        Edge2D instance1 = new Edge2D(new Point2D(-1, 1), new Point2D(1, -1));
        Edge2D instance2 = new Edge2D(new Point2D(1, 1), new Point2D(-1, -1));
        boolean expResult1 = true, expResult2 = false;
        boolean result1 = instance1.equals(obj), result2 = instance2.equals(obj);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
    }

    /**
     * Test of hashCode method, of class Edge2D.
     */
    @Test
    public void testHashCode() {
        out.println("hashCode");
        Edge2D instance = new Edge2D(new Point2D(-1, -1), new Point2D(1, 1));
        int expResult = 0;
        int result = instance.hashCode();
        assertTrue(expResult != result);
    }

    /**
     *
     */
    @Test
    public void testAdd() {
        out.println("add");
        Point2D p = new Point2D(3, 2);
        Edge2D instance = new Edge2D(new Point2D(-1, 0), new Point2D(1, 0));
        instance.add(p);
        Point2D[] listaPuntos = new Point2D[]{new Point2D(2, 2), new Point2D(4, 2)};
        assertArrayEquals(instance.getPoints(), listaPuntos);
    }

    /**
     * Test of axisProjectionIntersection method, of class Edge2D. 2 concentric
     * Edge2D
     */
    @Test
    public void testAxisProjectionIntersection_1() {
        out.println("axisProjectionIntersection: caso 1");
        Edge o = new Edge2D(new Point2D(0, 0), new Point2D(3, 3));
        int dimension0 = 0, dimension1 = 1;
        Edge2D instance = new Edge2D(new Point2D(1, 2), new Point2D(2, 1));
        Edge2D expResult0 = new Edge2D(new Point2D(1, 0), new Point2D(2, 0));
        Edge2D expResult1 = new Edge2D(new Point2D(0, 1), new Point2D(0, 2));
        Edge2D result0 = instance.axisProjectionIntersection(o, dimension0);
        Edge2D result1 = instance.axisProjectionIntersection(o, dimension1);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of axisProjectionIntersection method, of class Edge2D. 1 AABB2D
     * esquina superior izquierda
     */
    @Test
    public void testAxisProjectionIntersection_2() {
        out.println("axisProjectionIntersection: caso 2");
        Edge o = new Edge2D(new Point2D(0, 0), new Point2D(3, 3));
        int dimension0 = 0, dimension1 = 1;
        Edge2D instance = new Edge2D(new Point2D(0, 3), new Point2D(2, 1));
        Edge2D expResult0 = new Edge2D(new Point2D(0, 0), new Point2D(2, 0));
        Edge2D expResult1 = new Edge2D(new Point2D(0, 1), new Point2D(0, 3));
        Edge2D result0 = instance.axisProjectionIntersection(o, dimension0);
        Edge2D result1 = instance.axisProjectionIntersection(o, dimension1);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of axisProjectionIntersection method, of class Edge2D. 1 AABB2D
     * esquina superior derecha
     */
    @Test
    public void testAxisProjectionIntersection_3() {
        out.println("axisProjectionIntersection: caso 3");
        Edge o = new Edge2D(new Point2D(0, 3), new Point2D(3, 0));
        int dimension0 = 0, dimension1 = 1;
        Edge2D instance = new Edge2D(new Point2D(1, 1), new Point2D(3, 3));
        Edge2D expResult0 = new Edge2D(new Point2D(1, 0), new Point2D(3, 0));
        Edge2D expResult1 = new Edge2D(new Point2D(0, 1), new Point2D(0, 3));
        Edge2D result0 = instance.axisProjectionIntersection(o, dimension0);
        Edge2D result1 = instance.axisProjectionIntersection(o, dimension1);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of axisProjectionIntersection method, of class Edge2D. 1 AABB2D
     * esquina inferior derecha
     */
    @Test
    public void testAxisProjectionIntersection_4() {
        out.println("axisProjectionIntersection: caso 4");
        Edge o = new Edge2D(new Point2D(0, 0), new Point2D(3, 3));
        int dimension0 = 0, dimension1 = 1;
        Edge2D instance = new Edge2D(new Point2D(1, 2), new Point2D(3, 0));
        Edge2D expResult0 = new Edge2D(new Point2D(1, 0), new Point2D(3, 0));
        Edge2D expResult1 = new Edge2D(new Point2D(0, 0), new Point2D(0, 2));
        Edge2D result0 = instance.axisProjectionIntersection(o, dimension0);
        Edge2D result1 = instance.axisProjectionIntersection(o, dimension1);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of axisProjectionIntersection method, of class Edge2D. 1 AABB2D
     * esquina inferior izquierda
     */
    @Test
    public void testAxisProjectionIntersection_5() {
        out.println("axisProjectionIntersection: caso 5");
        Edge o = new Edge2D(new Point2D(0, 3), new Point2D(3, 0));
        int dimension0 = 0, dimension1 = 1;
        Edge2D instance = new Edge2D(new Point2D(0, 0), new Point2D(2, 2));
        Edge2D expResult0 = new Edge2D(new Point2D(0, 0), new Point2D(2, 0)), expResult1 = new Edge2D(new Point2D(0, 0), new Point2D(0, 2));
        Edge2D result0 = instance.axisProjectionIntersection(o, dimension0);
        Edge2D result1 = instance.axisProjectionIntersection(o, dimension1);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of axisProjectionIntersection method, of class Edge2D.
     */
    @Test
    public void testAxisProjectionIntersection_6() {
        out.println("axisProjectionIntersection: caso 6");
        Edge o = new Edge2D(new Point2D(1, 0), new Point2D(3, 2));
        int dimension0 = 0, dimension1 = 1;
        Edge2D instance = new Edge2D(new Point2D(0, 3), new Point2D(2, 1));
        Edge2D expResult0 = new Edge2D(new Point2D(1, 0), new Point2D(2, 0)), expResult1 = new Edge2D(new Point2D(0, 1), new Point2D(0, 2));
        Edge2D result0 = instance.axisProjectionIntersection(o, dimension0);
        Edge2D result1 = instance.axisProjectionIntersection(o, dimension1);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of axisProjectionIntersection method, of class Edge2D. 2 edge2D uno
     * sobre sobre otro (en y desplazado un poco)
     */
    @Test
    public void testAxisProjectionIntersection_7() {
        out.println("axisProjectionIntersection: caso 7");
        Edge o = new Edge2D(new Point2D(0, 1), new Point2D(2, 3));
        int dimension0 = 0, dimension1 = 1;
        Edge2D instance = new Edge2D(new Point2D(0, 2), new Point2D(2, 0));
        Edge2D expResult0 = new Edge2D(new Point2D(0, 0), new Point2D(2, 0));
        Edge2D expResult1 = new Edge2D(new Point2D(0, 1), new Point2D(0, 2));
        Edge2D result0 = instance.axisProjectionIntersection(o, dimension0);
        Edge2D result1 = instance.axisProjectionIntersection(o, dimension1);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of axisProjectionIntersection method, of class Edge2D. Espejo hacia
     * la derecha del caso 6
     */
    @Test
    public void testAxisProjectionIntersection_8() {
        out.println("axisProjectionIntersection: caso 8");
        Edge o = new Edge2D(new Point2D(0, 0), new Point2D(2, 2));
        int dimension0 = 0, dimension1 = 1;
        Edge2D instance = new Edge2D(new Point2D(1, 3), new Point2D(3, 1));
        Edge2D expResult0 = new Edge2D(new Point2D(1, 0), new Point2D(2, 0)), expResult1 = new Edge2D(new Point2D(0, 1), new Point2D(0, 2));
        Edge2D result0 = instance.axisProjectionIntersection(o, dimension0);
        Edge2D result1 = instance.axisProjectionIntersection(o, dimension1);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of axisProjectionIntersection method, of class Edge2D. Similar a
     * caso 7
     */
    @Test
    public void testAxisProjectionIntersection_9() {
        out.println("axisProjectionIntersection: caso 9");
        Edge o = new Edge2D(new Point2D(0, 2), new Point2D(2, 0));
        int dimension0 = 0, dimension1 = 1;
        Edge2D instance = new Edge2D(new Point2D(0, 1), new Point2D(2, 3));
        Edge2D expResult0 = new Edge2D(new Point2D(0, 0), new Point2D(2, 0)), expResult1 = new Edge2D(new Point2D(0, 1), new Point2D(0, 2));
        Edge2D result0 = instance.axisProjectionIntersection(o, dimension0);
        Edge2D result1 = instance.axisProjectionIntersection(o, dimension1);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of axisProjectionIntersection method, of class Edge2D. Espejo hacia
     * abajo del caso 6
     */
    @Test
    public void testAxisProjectionIntersection_10() {
        out.println("axisProjectionIntersection: caso 10");
        Edge o = new Edge2D(new Point2D(1, 3), new Point2D(3, 1));
        int dimension0 = 0, dimension1 = 1;
        Edge2D instance = new Edge2D(new Point2D(0, 0), new Point2D(2, 2));
        Edge2D expResult0 = new Edge2D(new Point2D(1, 0), new Point2D(2, 0)), expResult1 = new Edge2D(new Point2D(0, 1), new Point2D(0, 2));
        Edge2D result0 = instance.axisProjectionIntersection(o, dimension0);
        Edge2D result1 = instance.axisProjectionIntersection(o, dimension1);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of axisProjectionIntersection method, of class Edge2D. Espejo hacia
     * la derecha del caso 10
     */
    @Test
    public void testAxisProjectionIntersection_11() {
        out.println("axisProjectionIntersection: caso 11");
        Edge o = new Edge2D(new Point2D(0, 1), new Point2D(2, 3));
        int dimension0 = 0, dimension1 = 1;
        Edge2D instance = new Edge2D(new Point2D(1, 2), new Point2D(3, 0));
        Edge2D expResult0 = new Edge2D(new Point2D(1, 0), new Point2D(2, 0)), expResult1 = new Edge2D(new Point2D(0, 1), new Point2D(0, 2));
        Edge2D result0 = instance.axisProjectionIntersection(o, dimension0);
        Edge2D result1 = instance.axisProjectionIntersection(o, dimension1);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of axisProjectionIntersection method, of class Edge2D. 2 edge2D
     * superpuestos hacia la derecha
     */
    @Test
    public void testAxisProjectionIntersection_12() {
        out.println("axisProjectionIntersection: caso 12");
        Edge o = new Edge2D(new Point2D(0, 0), new Point2D(2, 2));
        int dimension0 = 0, dimension1 = 1;
        Edge2D instance = new Edge2D(new Point2D(1, 2), new Point2D(3, 0));
        Edge2D expResult0 = new Edge2D(new Point2D(1, 0), new Point2D(2, 0)), expResult1 = new Edge2D(new Point2D(0, 0), new Point2D(0, 2));
        Edge2D result0 = instance.axisProjectionIntersection(o, dimension0);
        Edge2D result1 = instance.axisProjectionIntersection(o, dimension1);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of axisProjectionIntersection method, of class Edge2D. Espejo a la
     * izquierda del caso 12
     */
    @Test
    public void testAxisProjectionIntersection_13() {
        out.println("axisProjectionIntersection: caso 13");
        Edge o = new Edge2D(new Point2D(1, 2), new Point2D(3, 0));
        int dimension0 = 0, dimension1 = 1;
        Edge2D instance = new Edge2D(new Point2D(0, 0), new Point2D(2, 2));
        Edge2D expResult0 = new Edge2D(new Point2D(1, 0), new Point2D(2, 0)), expResult1 = new Edge2D(new Point2D(0, 0), new Point2D(0, 2));
        Edge2D result0 = instance.axisProjectionIntersection(o, dimension0);
        Edge2D result1 = instance.axisProjectionIntersection(o, dimension1);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of axisProjectionIntersection method, of class Edge2D. 2 Edge2D que
     * tienen 1 extremo en comun
     */
    @Test
    public void testAxisProjectionIntersection_14() {
        out.println("axisProjectionIntersection: caso 14");
        Edge o = new Edge2D(new Point2D(0, 0), new Point2D(2, 2));
        int dimension0 = 0, dimension1 = 1;
        Edge2D instance = new Edge2D(new Point2D(2, 2), new Point2D(4, 0));
        Edge2D expResult0 = new Edge2D(new Point2D(2, 0), new Point2D(2, 0)), expResult1 = new Edge2D(new Point2D(0, 0), new Point2D(0, 2));
        Edge2D result0 = instance.axisProjectionIntersection(o, dimension0);
        Edge2D result1 = instance.axisProjectionIntersection(o, dimension1);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of axisProjectionIntersection method, of class Edge2D. Espejo a la
     * izquierda del caso 14
     */
    @Test
    public void testAxisProjectionIntersection_15() {
        out.println("axisProjectionIntersection: caso 15");
        Edge o = new Edge2D(new Point2D(2, 2), new Point2D(4, 0));
        int dimension0 = 0, dimension1 = 1;
        Edge2D instance = new Edge2D(new Point2D(0, 0), new Point2D(2, 2));
        Edge2D expResult0 = new Edge2D(new Point2D(2, 0), new Point2D(2, 0)), expResult1 = new Edge2D(new Point2D(0, 0), new Point2D(0, 2));
        Edge2D result0 = instance.axisProjectionIntersection(o, dimension0);
        Edge2D result1 = instance.axisProjectionIntersection(o, dimension1);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of axisProjectionIntersection method, of class Edge2D.
     */
    @Test
    public void testAxisProjectionIntersection_16() {
        out.println("axisProjectionIntersection: caso 16");
        Edge o = new Edge2D(new Point2D(0, 2), new Point2D(2, 4));
        int dimension0 = 0, dimension1 = 1;
        Edge2D instance = new Edge2D(new Point2D(0, 2), new Point2D(2, 0));
        Edge2D expResult0 = new Edge2D(new Point2D(0, 0), new Point2D(2, 0)), expResult1 = new Edge2D(new Point2D(0, 2), new Point2D(0, 2));
        Edge2D result0 = instance.axisProjectionIntersection(o, dimension0);
        Edge2D result1 = instance.axisProjectionIntersection(o, dimension1);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of axisProjectionIntersection method, of class Edge2D. Espejo hacia
     * abajo del caso 16
     */
    @Test
    public void testAxisProjectionIntersection_17() {
        out.println("axisProjectionIntersection: caso 17");
        Edge o = new Edge2D(new Point2D(0, 2), new Point2D(2, 0));
        int dimension0 = 0, dimension1 = 1;
        Edge2D instance = new Edge2D(new Point2D(0, 2), new Point2D(2, 4));
        Edge2D expResult0 = new Edge2D(new Point2D(0, 0), new Point2D(2, 0)), expResult1 = new Edge2D(new Point2D(0, 2), new Point2D(0, 2));
        Edge2D result0 = instance.axisProjectionIntersection(o, dimension0);
        Edge2D result1 = instance.axisProjectionIntersection(o, dimension1);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of axisProjectionIntersection method, of class Edge2D. 2 Edge2D que
     * no se intersectan, pero si sus cajas
     */
    @Test
    public void testAxisProjectionIntersection_18() {
        out.println("axisProjectionIntersection: caso 18");
        Edge o = new Edge2D(new Point2D(0, 2), new Point2D(2, 4));
        int dimension0 = 0, dimension1 = 1;
        Edge2D instance = new Edge2D(new Point2D(2, 2), new Point2D(4, 0));
        Edge2D expResult0 = new Edge2D(new Point2D(2, 0), new Point2D(2, 0)), expResult1 = new Edge2D(new Point2D(0, 2), new Point2D(0, 2));
        Edge2D result0 = instance.axisProjectionIntersection(o, dimension0);
        Edge2D result1 = instance.axisProjectionIntersection(o, dimension1);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of axisProjectionIntersection method, of class Edge2D. Dos Edge2D
     * que no se tocan
     */
    @Test
    public void testAxisProjectionIntersection_19() {
        out.println("axisProjectionIntersection: caso 19");
        Edge o = new Edge2D(new Point2D(0, 2), new Point2D(2, 4));
        int dimension0 = 0, dimension1 = 1;
        Edge2D instance = new Edge2D(new Point2D(3, 2), new Point2D(5, 0));
        Edge2D expResult0 = null, expResult1 = new Edge2D(new Point2D(0, 2), new Point2D(0, 2));
        Edge2D result0 = instance.axisProjectionIntersection(o, dimension0);
        Edge2D result1 = instance.axisProjectionIntersection(o, dimension1);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of axisProjectionIntersection method, of class Edge2D. " aristas
     * superpuestas totalmente
     */
    @Test
    public void testAxisProjectionIntersection_20() {
        out.println("axisProjectionIntersection: caso 20");
        Edge o = new Edge2D(new Point2D(0, 2), new Point2D(2, 0));
        int dimension0 = 0, dimension1 = 1;
        Edge2D instance = new Edge2D(new Point2D(0, 0), new Point2D(2, 2));
        Edge2D expResult0 = new Edge2D(new Point2D(0, 0), new Point2D(2, 0)), expResult1 = new Edge2D(new Point2D(0, 0), new Point2D(0, 2));
        Edge2D result0 = instance.axisProjectionIntersection(o, dimension0);
        Edge2D result1 = instance.axisProjectionIntersection(o, dimension1);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of getProjection method, of class Edge2D.
     */
    @Test
    public void testGetProjection() {
        out.println("getProjection");
        int dimension0 = 0;
        int dimension1 = 1;
        Edge2D instance = new Edge2D(new Point2D(-4, 4), new Point2D(4, -4));
        Edge2D expResult0 = new Edge2D(new Point2D(-4, 0), new Point2D(4, 0));
        Edge2D expResult1 = new Edge2D(new Point2D(0, 4), new Point2D(0, -4));
        Edge2D result0 = instance.getProjection(dimension0);
        Edge2D result1 = instance.getProjection(dimension1);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of axisProjectionIntersection method, of class Edge2D.
     */
    @Test
    public void testAxisProjectionIntersection() {
        out.println("axisProjectionIntersection");
        Edge o = new Edge2D(new Point2D(-4, 5), new Point2D(5, 1));
        int dimension0 = 0;
        int dimension1 = 1;
        
        Edge2D instance = new Edge2D(new Point2D(-5,-3), new Point2D(2,4));
        Edge2D expResult0 = new Edge2D(new Point2D(-4, 0), new Point2D(2, 0));
        Edge2D expResult1 = new Edge2D(new Point2D(0, 1), new Point2D(0, 4));
        Edge2D result0 = instance.axisProjectionIntersection(o, dimension0);
        Edge2D result1 = instance.axisProjectionIntersection(o, dimension1);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of getMinValue method, of class Edge2D.
     */
    @Test
    public void testGetMinValue() {
        out.println("getMinValue");
        int dimension0 = 0;
        int dimension1 = 1;
        Edge2D instance = new Edge2D(new Point2D(-3, 4), new Point2D(4, -5));
        double expResult0 = -3;
        double expResult1 = -5;
        double result0 = instance.getMinValue(dimension0);
        double result1 = instance.getMinValue(dimension1);
        
        assertEquals(expResult0, result0, 0.0);
        assertEquals(expResult1, result1, 0.0);
    }

    /**
     * Test of getMaxValue method, of class Edge2D.
     */
    @Test
    public void testGetMaxValue() {
        out.println("getMaxValue");
        out.println("getMinValue");
        int dimension0 = 0;
        int dimension1 = 1;
        Edge2D instance = new Edge2D(new Point2D(-3, 4), new Point2D(4, -5));
        double expResult0 = 4;
        double expResult1 = 4;
        double result0 = instance.getMaxValue(dimension0);
        double result1 = instance.getMaxValue(dimension1);
        
        assertEquals(expResult0, result0, 0.0);
        assertEquals(expResult1, result1, 0.0);
    }
}
