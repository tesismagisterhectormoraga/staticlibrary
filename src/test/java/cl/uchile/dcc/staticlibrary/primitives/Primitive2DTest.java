/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.primitives;

import static java.lang.System.out;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class Primitive2DTest {

    /**
     *
     */
    public Primitive2DTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of toString method, of class Primitiva.
     */
    @Test
    public void testToString() {
        out.println("toString");
        Primitive instance = new Primitive2D(new Edge2D(new Point2D(1, 3), new Point2D(3, 1)), 2);
        String expResult = "Primitive2D{objeto=Edge2D{a=(1.0,3.0), b=(3.0,1.0)}, indice=2}";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of hashCode method, of class Primitiva.
     */
    @Test
    public void testHashCode() {
        out.println("hashCode");
        Primitive instance = new Primitive2D(new Edge2D(new Point2D(), new Point2D(0.1f, 0.1f)), 0);
        int expResult = 0;
        int result = instance.hashCode();
        assertTrue(expResult != result);
    }

    /**
     * Test of equals method, of class Primitiva.
     */
    @Test
    public void testEquals() {
        out.println("equals");
        Object obj = new Primitive2D(new Edge2D(new Point2D(), new Point2D(0.1, 0.1)), 0);
        Primitive instance1 = new Primitive2D(new Edge2D(new Point2D(), new Point2D(0.1, 0.1)), 0);
        Primitive instance2 = new Primitive2D(new Edge2D(new Point2D(), new Point2D(0.1, 0.1)), 1);
        Primitive instance3 = new Primitive2D(new Edge2D(new Point2D(0, 0), new Point2D(0.1, 0.2)), 0);
        boolean expResult1 = true, expResult2 = false, expResult3 = false;
        boolean result1 = instance1.equals(obj);
        boolean result2 = instance2.equals(obj);
        boolean result3 = instance3.equals(obj);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);
    }

    /**
     * Test of getEdge method, of class Primitiva2D.
     */
    @Test
    public void testGetEdge() {
        out.println("getEdge");
        Primitive2D instance = new Primitive2D(new Edge2D(new Point2D(), new Point2D(0.1, 0.1)), 0);
        Edge2D expResult = new Edge2D(new Point2D(), new Point2D(0.1, 0.1));
        Edge2D result = instance.getEdge();
        assertEquals(expResult, result);
    }

    /**
     * Test of getIndice method, of class Primitiva2D.
     */
    @Test
    public void testGetIndex() {
        out.println("getIndex");
        Primitive2D instance = new Primitive2D(new Edge2D(new Point2D(), new Point2D(0.1, 0.1)), 1);
        int expResult = 1;
        int result = instance.getIndex();
        assertEquals(expResult, result);
    }
}
