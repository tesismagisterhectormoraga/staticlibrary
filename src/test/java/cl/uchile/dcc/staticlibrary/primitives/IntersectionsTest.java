/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.primitives;

import static java.lang.System.out;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class IntersectionsTest {

    public Pair<Integer> par0, par1, par2, par3, par4;
    public Intersections listaX, listaY, listaZ;

    /**
     *
     */
    public IntersectionsTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.par0 = new PairInteger(0, 1);
        this.par1 = new PairInteger(0, 2);
        this.par2 = new PairInteger(1, 2);
        this.par3 = new PairInteger(2, 0);
        this.par4 = new PairInteger(1, 0);

        this.listaX = new Intersections();
        this.listaY = new Intersections();
        this.listaZ = new Intersections();
    }

    /**
     *
     */
    @After
    public void tearDown() {
        this.listaX.clear();
        this.listaY.clear();
        this.listaZ.clear();
    }

    /**
     *
     */
    @Test
    public void testAdd() {
        out.println("add");
        this.listaX.add(this.par0);   // (0,1)
        this.listaX.add(this.par1);   // (0,2)

        this.listaY.add(this.par0);   // (0,1)
        this.listaY.add(this.par2);   // (1,2)
        this.listaY.add(this.par3);   // (2,0)

        assertFalse(this.listaX.add(this.par3));
        assertTrue(this.listaX.add(this.par2));
    }

    /**
     * Test of getColisiones method, of class Intersecciones.
     */
    @Test
    public void testGetCollisionsList() {
        out.println("getColisionsList");
        Intersections instance = new Intersections();
        this.listaX.add(this.par0);   // (0,1)
        this.listaX.add(this.par1);   // (0,2)

        instance.addAll(this.listaX);
        List<Pair<Integer>> expResult = new ArrayList<>();
        expResult.add(this.par0);
        expResult.add(this.par1);

        List<Pair<Integer>> result = instance.getCollisionsList();
        assertEquals(expResult, result);
    }    
}
