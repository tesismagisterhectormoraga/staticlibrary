/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.primitives;

import static java.lang.System.out;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class PairIntegerTest {
    
    /**
     *
     */
    public PairIntegerTest() {
    }
    
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of inverted method, of class PairInteger.
     */
    @Test
    public void testInverted() {
        out.println("inverted");
        Pair<Integer> instance = new PairInteger(3, 5);
        Pair<Integer> expResult = new PairInteger(5, 3);
        
        Pair<Integer> result = instance.inverted();
        assertEquals(expResult, result);
    }

    /**
     * Test of hashCode method, of class PairInteger.
     */
    @Test
    public void testHashCode() {
        out.println("hashCode");
        Pair<Integer> instance0 = new PairInteger(1, -7);
        Pair<Integer> instance1 = new PairInteger(-7, 1);
        int expResult0 = 0;
        int expResult1 = 0;
        int result0 = instance0.hashCode();
        int result1 = instance1.hashCode();
        assertNotEquals(expResult0, result0);
        assertNotEquals(expResult1, result1);
        assertEquals(result0, result1);
    }

    /**
     * Test of equals method, of class PairInteger.
     */
    @Test
    public void testEquals() {
        out.println("equals");
        Object obj = new PairInteger(-2, 6);
        Pair<Integer> instance0 = new PairInteger(-2, 6);
        Pair<Integer> instance1 = new PairInteger(6, -2);
        
        boolean expResult0 = true;
        boolean expResult1 = true;
        boolean result0 = instance0.equals(obj);
        boolean result1 = instance1.equals(obj);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of getFirst method, of class PairInteger.
     */
    @Test
    public void testGetFirst() {
        out.println("getFirst");
        Pair<Integer> instance = new PairInteger(1, 5);
        Integer expResult = 1;
        Integer result = instance.getFirst();
        assertEquals(expResult, result);
    }

    /**
     * Test of getSecond method, of class PairInteger.
     */
    @Test
    public void testGetSecond() {
        out.println("getSecond");
        Pair<Integer> instance = new PairInteger(-7, -13);
        Integer expResult = -7;
        Integer result = instance.getSecond();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class PairInteger.
     */
    @Test
    public void testToString() {
        out.println("toString");
        Pair<Integer> instance = new PairInteger(2, 5);
        String expResult = "(2,5)";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFirst method, of class PairInteger.
     */
    @Test
    public void testSetFirst() {
        out.println("setFirst");
        Integer first = 4;
        Pair<Integer> instance = new PairInteger(0, 3);
        instance.setFirst(first);
        assertTrue(Objects.equals(instance.getFirst(), first));
    }

    /**
     * Test of setSecond method, of class PairInteger.
     */
    @Test
    public void testSetSecond() {
        out.println("setSecond");
        Integer second = 7;
        Pair<Integer> instance = new PairInteger(1, 3);
        instance.setSecond(second);
        assertTrue(Objects.equals(instance.getSecond(), second));
    }

    /**
     * Test of exists method, of class PairInteger.
     */
    @Test
    public void testExists() {
        out.println("exists");
        List<Pair<Integer>> listaPares = new ArrayList<>();
        Pair<Integer> instance = new PairInteger(10, 12);
        boolean expResult0 = false;
        boolean result0 = instance.exists(listaPares);
        assertEquals(expResult0, result0);
        listaPares.add(instance);
        boolean expResult1 = true;
        boolean result1 = instance.exists(listaPares);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of intersection method, of class PairInteger.
     */
    @Test
    public void testIntersection() {
        out.println("intersection");
        Pair<Integer> otro0 = new PairInteger(9,13);
        Pair<Integer> otro1 = new PairInteger(1,5);
        Pair<Integer> otro2 = new PairInteger(4,11);
        Pair<Integer> instance = new PairInteger(2, 7);
        Pair<Integer> expResult0 = null;
        Pair<Integer> expResult1 = new PairInteger(2,5);
        Pair<Integer> expResult2 = new PairInteger(4,7);
        Pair<Integer> result0 = instance.intersection(otro0);
        Pair<Integer> result1 = instance.intersection(otro1);
        Pair<Integer> result2 = instance.intersection(otro2);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
    }

    /**
     * Test of intersects method, of class PairInteger.
     */
    @Test
    public void testIntersects_true() {
        out.println("intersects true");
        Pair<Integer> otro = new PairInteger(3, 7);
        Pair<Integer> instance0 = new PairInteger(1, 6);
        Pair<Integer> instance1 = new PairInteger(4, 10);
        Pair<Integer> instance2 = new PairInteger(5, 6);
        boolean expResult0 = true;
        boolean expResult1 = true;
        boolean expResult2 = true;
        boolean result0 = instance0.intersects(otro);
        boolean result1 = instance1.intersects(otro);
        boolean result2 = instance2.intersects(otro);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
    }
    
    /**
     * Test of intersects method, of class PairInteger.
     */
    @Test
    public void testIntersects_false() {
        out.println("intersects false");
        Pair<Integer> otro = new PairInteger(4, 7);
        Pair<Integer> instance0 = new PairInteger(1, 3);
        Pair<Integer> instance1 = new PairInteger(8, 10);
        boolean expResult0 = false;
        boolean expResult1 = false;
        boolean result0 = instance0.intersects(otro);
        boolean result1 = instance1.intersects(otro);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }
}
