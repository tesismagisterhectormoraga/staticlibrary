/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.staticlibrary.primitives;

import static cl.uchile.dcc.staticlibrary.primitives.BVNode.collidePrimitives;
import static cl.uchile.dcc.staticlibrary.primitives.BVNode.descendA;
import static cl.uchile.dcc.staticlibrary.primitives.BVNode.getMinorCombinedVolumeIndice;
import static cl.uchile.dcc.staticlibrary.primitives.BVNode.obtainCollidingEdges;
import static cl.uchile.dcc.staticlibrary.primitives.BVNode.obtainMinimalAreaCombinadedNode;
import static cl.uchile.dcc.staticlibrary.primitives.BVNode.sortNodesList;
import static cl.uchile.dcc.staticlibrary.primitives.BVNode.wrap;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class BVNodeTest {

    private BVNode root, nodoleft, nodoright;
    private AABB2D cajaRoot, cajaLeft, cajaRight;

    /**
     *
     */
    public BVNodeTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.cajaLeft = new AABB2D(new Edge2D(new Point2D(0, 2), new Point2D(2, 0)), 0);
        this.cajaRight = new AABB2D(new Edge2D(new Point2D(1, -1), new Point2D(4, 1)), 1);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getData method, of class BVNode.
     */
    @Test
    public void testGetData() {
        out.println("getData");
        BVNode instance = new BVNode(this.cajaLeft);
        BV expResult = this.cajaLeft;
        BV result = instance.getData();
        assertEquals(expResult, result);
    }

    /**
     * Test of setData method, of class BVNode.
     */
    @Test
    public void testSetData() {
        out.println("setData");
        BV data = this.cajaLeft;
        BVNode instance = new BVNode(this.cajaRight);
        instance.setData(data);
        assertTrue(instance.getData().equals(this.cajaLeft));
    }

    /**
     * Test of getLeft method, of class BVNode.
     */
    @Test
    public void testGetLeft() {
        out.println("getLeft");
        this.cajaRoot = this.cajaLeft.unionBV(this.cajaRight);
        BVNode instance = new BVNode(this.cajaRoot);
        instance.setLeft(this.cajaLeft);

        BVNode expResult = new BVNode(this.cajaLeft);
        BVNode result = instance.getLeft();
        assertEquals(expResult, result);
    }

    /**
     * Test of setLeft method, of class BVNode.
     */
    @Test
    public void testSetLeft_BVNode() {
        out.println("setLeft");
        this.nodoleft = new BVNode(this.cajaLeft);
        BVNode left = this.nodoleft;

        this.root = new BVNode(this.cajaLeft.unionBV(this.cajaRight));
        BVNode instance = this.root;

        instance.setLeft(left);
        assertTrue(instance.getLeft().equals(this.nodoleft));
    }

    /**
     * Test of setLeft method, of class BVNode.
     */
    @Test
    public void testSetLeft_BV() {
        out.println("setLeft_BV");
        AABB2D caja = new AABB2D(new Point2D(5.0, 4.0), 3.0, 3.0);
        AABB2D hijoIzq = new AABB2D(new Point2D(3.0, 2.0), 2.0, 2.0);

        BVNode nodo = new BVNode(caja);
        nodo.setLeft(hijoIzq);

        BV p = nodo.getLeft().getData();

        assertNotNull(nodo.getLeft());
        assertEquals(p, hijoIzq);
    }

    /**
     * Test of getRight method, of class BVNode.
     */
    @Test
    public void testGetRight() {
        out.println("getRight");
        this.cajaRoot = this.cajaLeft.unionBV(this.cajaRight);
        BVNode instance = new BVNode(this.cajaRoot);
        instance.setRight(this.cajaRight);

        BVNode expResult = new BVNode(this.cajaRight);
        BVNode result = instance.getRight();
        assertEquals(expResult, result);
    }

    /**
     * Test of setRight method, of class BVNode.
     */
    @Test
    public void testSetRight_BVNode() {
        out.println("setRight");
        this.nodoright = new BVNode(this.cajaRight);
        BVNode instance = new BVNode(this.cajaLeft.unionBV(this.cajaRight));
        instance.setRight(this.nodoright);
        assertTrue(instance.getRight().equals(this.nodoright));
    }

    /**
     * Test of setRight method, of class BVNode.
     */
    @Test
    public void testSetRight_BV() {
        out.println("setRight");

        AABB2D caja = new AABB2D(new Point2D(5.0, 4.0), 3.0, 3.0);
        AABB2D hijoDer = new AABB2D(new Point2D(6.0, 7.0), 2.0, 2.0);

        BVNode nodo = new BVNode(caja);
        nodo.setRight(hijoDer);

        BV p = nodo.getRight().getData();

        assertNotNull(nodo.getRight());
        assertEquals(p, hijoDer);
    }

    /**
     * Test of hashCode method, of class BVNode.
     */
    @Test
    public void testHashCode() {
        out.println("hashCode");
        BVNode instance = new BVNode(this.cajaRoot);
        int expResult = 0;
        int result = instance.hashCode();
        assertNotEquals(expResult, result);
    }

    /**
     * Test of equals method, of class BVNode.
     */
    @Test
    public void testEquals_1() {
        out.println("equals false 1");

        // dos cajas totalmente diferentes
        AABB2D cajaUno = new AABB2D(new Point2D(5.0, 4.0), 3.0, 3.0);
        AABB2D cajaDos = new AABB2D(new Point2D(3.0, 5.0), 2.0, 6.0);

        BVNode nodoUno = new BVNode(cajaUno);
        BVNode nodoDos = new BVNode(cajaDos);

        assertFalse(cajaUno == cajaDos);
        assertFalse(nodoUno == nodoDos);
    }

    /**
     * Test of equals method, of class BVNode.
     */
    @Test
    public void testEquals_2() {
        out.println("equals false 2");

        // dos cajas Con los puntos centrales diferentes
        AABB2D cajaUno = new AABB2D(new Point2D(5.0, 4.0), 3.0, 3.0);
        AABB2D cajaDos = new AABB2D(new Point2D(3.0, 5.0), 3.0, 3.0);

        BVNode nodoUno = new BVNode(cajaUno);
        BVNode nodoDos = new BVNode(cajaDos);

        assertFalse(cajaUno == cajaDos);
        assertFalse(nodoUno == nodoDos);
    }

    /**
     * Test of equals method, of class BVNode.
     */
    @Test
    public void testEquals_3() {
        out.println("equals false 3");

        // dos cajas con la dimension X diferente
        AABB2D cajaUno = new AABB2D(new Point2D(5.0, 4.0), 3.0, 3.0);
        AABB2D cajaDos = new AABB2D(new Point2D(5.0, 4.0), 2.0, 3.0);

        BVNode nodoUno = new BVNode(cajaUno);
        BVNode nodoDos = new BVNode(cajaDos);

        assertFalse(cajaUno == cajaDos);
        assertFalse(nodoUno == nodoDos);
    }

    /**
     * Test of equals method, of class BVNode.
     */
    @Test
    public void testEquals_4() {
        out.println("equals false 4");

        // dos cajas con la dimension Y diferente
        AABB2D cajaUno = new AABB2D(new Point2D(5.0, 4.0), 3.0, 3.0);
        AABB2D cajaDos = new AABB2D(new Point2D(5.0, 4.0), 3.0, 2.0);

        BVNode nodoUno = new BVNode(cajaUno);
        BVNode nodoDos = new BVNode(cajaDos);

        assertFalse(cajaUno == cajaDos);
        assertFalse(nodoUno == nodoDos);
    }

    /**
     * Test of equals method, of class BVNode.
     */
    @Test
    public void testEquals_5() {
        out.println("equals true");

        AABB2D cajaUno = new AABB2D(new Point2D(5.0, 4.0), 3.0, 3.0);
        AABB2D cajaDos = new AABB2D(new Point2D(5.0, 4.0), 3.0, 3.0);

        BVNode nodoUno = new BVNode(cajaUno);
        BVNode nodoDos = new BVNode(cajaDos);

        Point2D centroCajaUno = ((AABB2D) nodoUno.getData()).getCentro();
        Point2D centroCajaDos = ((AABB2D) nodoDos.getData()).getCentro();

        assertEquals(centroCajaUno, centroCajaDos);

        double largoXUno, largoXDos, largoYUno, largoYDos;
        largoXUno = ((AABB2D) nodoUno.getData()).getLargoX();
        largoXDos = ((AABB2D) nodoDos.getData()).getLargoX();
        largoYUno = ((AABB2D) nodoUno.getData()).getLargoY();
        largoYDos = ((AABB2D) nodoDos.getData()).getLargoY();

        assertEquals(largoXUno, largoXDos, 0.00001);
        assertEquals(largoYUno, largoYDos, 0.00001);
        assertTrue(nodoUno.getLeft() == nodoDos.getLeft());
        assertTrue(nodoUno.getRight() == nodoDos.getRight());
        assertEquals(cajaUno, cajaDos);
    }

    /**
     * Test of compareTo method, of class BVNode.
     */
    @Test
    public void testCompareTo() {
        out.println("compareTo");
        BVNode o1 = new BVNode(new AABB2D(new Point2D(2, -2), 4, 4));
        BVNode o2 = new BVNode(new AABB2D(new Point2D(1.5, -5.5), 3, 3));
        BVNode o3 = new BVNode(new AABB2D(new Point2D(1, -8), 2, 2));
        int expResult0 = -1, expResult1 = -1, expResult2 = -1;  // el orden es de menor a mayor area
        int result0 = o1.compareTo(o2), result1 = o1.compareTo(o3), result2 = o2.compareTo(o3);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
    }

    /**
     * Test of wrap method, of class BVNode.
     */
    @Test
    public void testWrap() {
        out.println("wrap");

        AABB2D cajaUno = new AABB2D(new Point2D(5.0, 4.0), 3.0, 3.0);
        AABB2D cajaDos = new AABB2D(new Point2D(5.0, 4.0), 3.0, 3.0);
        AABB2D cajaTres = new AABB2D(new Point2D(6.0, 4.0), 5.0, 6.0);

        List<AABB2D> listaCajas = new ArrayList<>();
        listaCajas.add(cajaUno);
        listaCajas.add(cajaDos);
        listaCajas.add(cajaTres);

        List<BVNode> listaTmp = new ArrayList<>();

        listaCajas.forEach((caja) -> {
            listaTmp.add(wrap(caja));
        });
        // reviso que no sean vacios
        listaTmp.stream().map((nodo) -> {
            assertNotNull(nodo);
            return nodo;
        }).forEachOrdered((nodo) -> {
            assertTrue(nodo.getClass().getSimpleName().equals("BVNode"));
        });
    }

    /**
     * Test of unwrap method, of class BVNode.
     */
    @Test
    public void testUnwrap() {
        out.println("unwrap");
        BVNode instance = new BVNode(this.cajaLeft);
        BV expResult = this.cajaLeft;
        BV result = instance.unwrap();
        assertEquals(expResult, result);
    }

    /**
     * Test of union method, of class BVNode.
     */
    @Test
    public void testUnion() {
        out.println("union");
        this.nodoleft = new BVNode(this.cajaLeft);
        this.nodoright = new BVNode(this.cajaRight);
        this.cajaRoot = this.cajaLeft.unionBV(this.cajaRight);
        this.root = new BVNode(this.cajaRoot);

        BVNode expResult = new BVNode(this.cajaRoot);
        BVNode result = this.nodoleft.union(this.nodoright);
        assertEquals(expResult, result);
    }

    /**
     * Test of obtenerIndiceMenorVolumenCombinado method, of class BVNode.
     */
    @Test
    public void testGetMinorCombinedVolumeIndice_1() {
        out.println("getMinorCombinedVolumeIndice 1");
        List<BVNode> listaBVNodes = new ArrayList<>();
        listaBVNodes.add(new BVNode(new AABB2D(new Point2D(0, 0), 2, 2)));
        listaBVNodes.add(new BVNode(new AABB2D(new Point2D(3, 0), 2, 2)));

        BVNode nodo = new BVNode(new AABB2D(new Point2D(0, 0), 2, 2));
        int expResult = 0;
        int result = getMinorCombinedVolumeIndice(listaBVNodes, nodo);
        assertEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testGetMinorCombinedVolumeIndice_2() {
        out.println("getMinorCombinedVolumeIndice 2");
        List<BVNode> listaBVNodes = new ArrayList<>();
        listaBVNodes.add(new BVNode(new AABB2D(new Point2D(2, 0), 2, 2)));
        listaBVNodes.add(new BVNode(new AABB2D(new Point2D(0, 0), 2, 2)));

        BVNode nodo = new BVNode(new AABB2D(new Point2D(0, -2.5), 1, 1));
        int expResult = 1;
        int result = getMinorCombinedVolumeIndice(listaBVNodes, nodo);
        assertEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testGetMinorCombinedVolumeIndice_3() {
        out.println("obtenerIndiceMenorVolumenCombinado 3");
        List<BVNode> listaBVNodes = new ArrayList<>();
        listaBVNodes.add(new BVNode(new AABB2D(new Point2D(2, 2), 1, 1)));
        listaBVNodes.add(new BVNode(new AABB2D(new Point2D(-2, -2), 1, 1)));

        BVNode nodo = new BVNode(new AABB2D(new Point2D(-2, 2), 1, 1));
        int expResult = 1;
        int result = getMinorCombinedVolumeIndice(listaBVNodes, nodo);
        assertEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testGetMinorCombinedVolumeIndice_4() {
        out.println("getMinorCombinedVolumeIndice 4");
        List<BVNode> listaBVNodes = new ArrayList<>();
        listaBVNodes.add(new BVNode(new AABB2D(new Point2D(0.5, -2.5), 3, 3)));
        listaBVNodes.add(new BVNode(new AABB2D(new Point2D(4, 0), 2, 2)));

        BVNode nodo = new BVNode(new AABB2D(new Point2D(0, 0), 2, 2));
        int expResult = 1;
        int result = getMinorCombinedVolumeIndice(listaBVNodes, nodo);
        assertEquals(expResult, result);
    }

    /**
     * Test of isLeaf method, of class BVNode.
     */
    @Test
    public void testIsLeaf_1() {
        out.println("isLeaf False Complete Node");
        // la unica forma de definir un hijo es que tenga una primitiva en el interior, si no es un nodo cualquiera
        //nodo con hijos izq y derecho
        AABB2D caja = new AABB2D(new Point2D(5.0, 4.0), 3.0, 3.0);
        AABB2D hijoIzq = new AABB2D(new Edge2D(new Point2D(2, 1), new Point2D(4, 3)), 0);
        AABB2D hijoDer = new AABB2D(new Edge2D(new Point2D(5.0, 6.0), new Point2D(7.0, 8.0)), 1);

        BVNode nodo = new BVNode(caja);
        nodo.setLeft(hijoIzq);
        nodo.setRight(hijoDer);

        assertFalse(nodo.isLeaf());
        assertTrue(hijoIzq.isLeaf());
        assertTrue(hijoDer.isLeaf());
    }

    /**
     * Test of isLeaf method, of class BVNode.
     */
    @Test
    public void testIsLeaf_2() {
        out.println("isLeaf False Left Node Complete");

        //nodo con hijo izq
        AABB2D caja = new AABB2D(new Point2D(5.0, 4.0), 3.0, 3.0);
        AABB2D hijoIzq = new AABB2D(new Point2D(3.0, 2.0), 2.0, 2.0);

        BVNode nodo = new BVNode(caja);
        nodo.setLeft(hijoIzq);
        assertFalse(nodo.isLeaf());
    }

    /**
     * Test of isLeaf method, of class BVNode.
     */
    @Test
    public void testIsLeaf_3() {
        out.println("isLeaf False Right Node Complete");
        //nodo con hijo derecho
        AABB2D caja = new AABB2D(new Point2D(5.0, 4.0), 3.0, 3.0);
        AABB2D hijoDer = new AABB2D(new Point2D(6.0, 7.0), 2.0, 2.0);

        BVNode nodo = new BVNode(caja);
        nodo.setRight(hijoDer);
        assertFalse(nodo.isLeaf());
    }

    /**
     * Test of isLeaf method, of class BVNode.
     */
    @Test
    public void testIsLeaf_4() {
        out.println("isLeaf True");
        //nodo con data pero sin hijos
        AABB2D caja = new AABB2D(new Point2D(5.0, 4.0), 3.0, 3.0);

        BVNode nodo = new BVNode(caja);
        assertTrue(nodo.isLeaf());
    }

    /**
     * Test of addingNode method, of class BVNode.
     */
    @Test
    public void testAddingNode_1() {
        out.println("addingNode caja dentro de otra");
        AABB2D cajaUno = new AABB2D(new Point2D(-2.0, -2.0), 2.0, 2.0);
        AABB2D cajaDos = new AABB2D(new Point2D(0.0, 0.0), 6.0, 6.0);

        BVNode nodoUno = new BVNode(cajaUno);
        BVNode nodoDos = new BVNode(cajaDos);

        BVNode resultado = nodoUno.addingNode(nodoDos);
        BVNode AComparar = new BVNode(new AABB2D(new Point2D(0.0, 0.0), 6.0, 6.0));

        AComparar.setLeft(new AABB2D(new Point2D(-2.0, -2.0), 2.0, 2.0));
        AComparar.setRight(new AABB2D(new Point2D(0.0, 0.0), 6.0, 6.0));

        assertEquals(resultado.getData(), AComparar.getData());
    }

    /**
     * Test of addingNode method, of class BVNode.
     */
    @Test
    public void testAddingNode_2() {
        out.println("addingNode cajas disjuntas");
        AABB2D cajaUno = new AABB2D(new Point2D(1, 1), 6.0, 6.0);
        AABB2D cajaDos = new AABB2D(new Point2D(9, 1), 8.0, 8.0);

        BVNode nodoUno = new BVNode(cajaUno);
        BVNode nodoDos = new BVNode(cajaDos);

        BVNode resultado = nodoUno.addingNode(nodoDos);
        BVNode AComparar = new BVNode(new AABB2D(new Point2D(5.5, 1.0), 15.0, 8.0));

        AComparar.setLeft(new AABB2D(new Point2D(1, 1), 6.0, 6.0));
        AComparar.setRight(new AABB2D(new Point2D(9, 1), 8.0, 8.0));
        AABB2D pres = (AABB2D) resultado.getData();
        AABB2D pcom = (AABB2D) AComparar.getData();

        assertEquals(resultado, AComparar);
        assertEquals(pres, pcom);
        assertEquals(resultado.getLeft().getData(), AComparar.getLeft().getData());
        assertEquals(resultado.getRight().getData(), AComparar.getRight().getData());
    }

    /**
     * Test of addingNode method, of class BVNode.
     */
    @Test
    public void testAddingNode_3() {
        out.println("addingNode Cajas Traslapadas Mismo EjeX");

        AABB2D cajaUno = new AABB2D(new Point2D(3, 3), 6.0, 6.0);
        AABB2D cajaDos = new AABB2D(new Point2D(9, 3), 8.0, 8.0);

        BVNode nodoUno = new BVNode(cajaUno);
        BVNode nodoDos = new BVNode(cajaDos);

        BVNode resultado = nodoUno.addingNode(nodoDos);
        BVNode AComparar = new BVNode(new AABB2D(new Point2D(6.5, 3.0), 13.0, 8.0));
        AComparar.setLeft(new AABB2D(new Point2D(3, 3), 6.0, 6.0));
        AComparar.setRight(new AABB2D(new Point2D(9, 3), 8.0, 8.0));

        AABB2D pres = (AABB2D) resultado.getData();
        AABB2D pcom = (AABB2D) AComparar.getData();

        assertEquals(pres, pcom);
        assertEquals(resultado.getLeft().getData(), AComparar.getLeft().getData());
        assertEquals(resultado.getRight().getData(), AComparar.getRight().getData());
    }

    /**
     * Test of addingNode method, of class BVNode.
     */
    @Test
    public void testAddingNode_4() {
        out.println("addingNode Cajas Traslapadas Mismo EjeY");

        AABB2D cajaUno = new AABB2D(new Point2D(3.0, 3.0), 6.0, 6.0);
        AABB2D cajaDos = new AABB2D(new Point2D(3.0, 9.0), 8.0, 8.0);

        BVNode nodoUno = new BVNode(cajaUno);
        BVNode nodoDos = new BVNode(cajaDos);

        BVNode resultado = nodoUno.addingNode(nodoDos);
        BVNode AComparar = new BVNode(new AABB2D(new Point2D(3.0, 6.5), 8.0, 13.0));
        AComparar.setLeft(new AABB2D(new Point2D(3.0, 3.0), 6.0, 6.0));
        AComparar.setRight(new AABB2D(new Point2D(3.0, 9.0), 8.0, 8.0));

        assertEquals(resultado, AComparar);
    }

    /**
     * Test of addingNode method, of class BVNode.
     */
    @Test
    public void testAddingNode_5() {
        out.println("addingNode Union Cajas Iguales");

        AABB2D cajaUno = new AABB2D(new Point2D(-2.0, -2.0), 2.0, 2.0);
        AABB2D cajaDos = new AABB2D(new Point2D(-2.0, -2.0), 2.0, 2.0);

        BVNode nodoUno = new BVNode(cajaUno);
        BVNode nodoDos = new BVNode(cajaDos);

        BVNode resultado = nodoUno.addingNode(nodoDos);
        BVNode AComparar = new BVNode(new AABB2D(new Point2D(-2.0, -2.0), 2.0, 2.0));
        AComparar.setLeft(new AABB2D(new Point2D(-2.0, -2.0), 2.0, 2.0));
        AComparar.setRight(new AABB2D(new Point2D(-2.0, -2.0), 2.0, 2.0));

        assertEquals(resultado, AComparar);
        assertEquals(resultado.getData(), AComparar.getData());
        assertEquals(resultado.getLeft(), AComparar.getLeft());
        assertEquals(resultado.getRight(), AComparar.getRight());
    }

    /**
     * Test of addingNode method, of class BVNode.
     */
    @Test
    public void testAddingNode_6() {
        out.println("addingNode Union Cajas Traslapadas Cualquiera");

        AABB2D cajaUno = new AABB2D(new Point2D(3.0, 3.0), 6.0, 6.0);
        AABB2D cajaDos = new AABB2D(new Point2D(8.0, 9.0), 8.0, 8.0);

        BVNode nodoUno = new BVNode(cajaUno);
        BVNode nodoDos = new BVNode(cajaDos);

        BVNode resultado = nodoUno.addingNode(nodoDos);
        BVNode AComparar = new BVNode(new AABB2D(new Point2D(6.0, 6.5), 12.0, 13.0));
        AComparar.setLeft(new AABB2D(new Point2D(3.0, 3.0), 6.0, 6.0));
        AComparar.setRight(new AABB2D(new Point2D(8.0, 9.0), 8.0, 8.0));

        assertEquals(resultado, AComparar);
        assertEquals(resultado.getData(), AComparar.getData());
        assertEquals(resultado.getLeft(), AComparar.getLeft());
        assertEquals(resultado.getRight(), AComparar.getRight());
    }

    /**
     * Test of addingNode method, of class BVNode.
     */
    @Test
    public void testAddingNode_7() {
        out.println("addingNode Union idempotencia");

        AABB2D cajaUno = new AABB2D(new Point2D(3.0, 3.0), 6.0, 6.0);
        AABB2D cajaDos = new AABB2D(new Point2D(8.0, 9.0), 8.0, 8.0);

        BVNode nodoUno = new BVNode(cajaUno);
        BVNode nodoDos = new BVNode(cajaDos);

        BVNode resultadoUno = nodoUno.addingNode(nodoDos);
        BVNode resultadoDos = nodoDos.addingNode(nodoUno);

        assertEquals(resultadoUno, resultadoDos);
        assertEquals(resultadoUno.getData(), resultadoDos.getData());
    }

    /**
     * Test of toString method, of class BVNode.
     */
    @Test
    public void testToString() {
        out.println("toString");
        BVNode instance = new BVNode(this.cajaRight);
        String expResult = "BVNode{" + "data=AABB2D{p=(1.0,-1.0), q=(4.0,1.0), arista=Primitive2D{objeto=Edge2D{a=(1.0,-1.0), b=(4.0,1.0)}, indice=1}}, left=null, right=null}";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of obtenerAristasColisionando method, of class BVNode.
     */
    @Test
    public void testObtainCollidingEdges_1() {
        out.println("obtainCollidingEdges 2 nodos simples");

        BVNode nodoUno = new BVNode(new AABB2D(new Edge2D(new Point2D(2, 2), new Point2D(-2, -2)), 0));
        BVNode nodoDos = new BVNode(new AABB2D(new Edge2D(new Point2D(-3, 3), new Point2D(1, -1)), 1));
        List<Pair<Integer>> listaColisiones = new ArrayList<>();

        obtainCollidingEdges(listaColisiones, nodoUno, nodoDos);
        assertTrue(!listaColisiones.isEmpty());
        assertEquals(listaColisiones.get(0).getFirst(), 0, 0.0);
        assertEquals(listaColisiones.get(0).getSecond(), 1, 0.0);
    }

    /**
     * Test of obtenerAristasColisionando method, of class BVNode.
     */
    @Test
    public void testObtainCollidingEdges_2() {
        out.println("obtainCollidingEdges 2 cuadrados");

        int i = 0, j = 0;
        List<BVNode> listaNodosObjetoUno = new ArrayList<>();
        List<BVNode> listaNodosObjetoDos = new ArrayList<>();

        listaNodosObjetoUno.add(new BVNode(new AABB2D(new Edge2D(new Point2D(-2, -2), new Point2D(2, -2)), i++)));
        listaNodosObjetoDos.add(new BVNode(new AABB2D(new Edge2D(new Point2D(-3, -1), new Point2D(1, -1)), j++)));
        listaNodosObjetoUno.add(new BVNode(new AABB2D(new Edge2D(new Point2D(2, -2), new Point2D(2, 2)), i++)));
        listaNodosObjetoDos.add(new BVNode(new AABB2D(new Edge2D(new Point2D(1, -1), new Point2D(1, 3)), j++)));
        listaNodosObjetoUno.add(new BVNode(new AABB2D(new Edge2D(new Point2D(2, 2), new Point2D(-2, 2)), i++)));
        listaNodosObjetoDos.add(new BVNode(new AABB2D(new Edge2D(new Point2D(1, 3), new Point2D(-3, 3)), j++)));
        listaNodosObjetoUno.add(new BVNode(new AABB2D(new Edge2D(new Point2D(-2, 2), new Point2D(-2, -2)), i++)));
        listaNodosObjetoDos.add(new BVNode(new AABB2D(new Edge2D(new Point2D(-3, 3), new Point2D(-3, -1)), j++)));

        List<Pair<Integer>> listaColisiones = new ArrayList<>();

        for (i = 0; i < listaNodosObjetoUno.size(); i++) {
            for (j = 0; j < listaNodosObjetoDos.size(); j++) {
                obtainCollidingEdges(listaColisiones, listaNodosObjetoUno.get(i), listaNodosObjetoDos.get(j));
            }
        }

        List<Pair<Integer>> result = new ArrayList<>(2);
        result.add(new PairInteger(1, 2));
        result.add(new PairInteger(0, 3));
        
        assertTrue(!listaColisiones.isEmpty());
        assertEquals(listaColisiones, result);
    }

    /**
     * Test of descendA method, of class BVNode.
     */
    @Test
    public void testDescendA_1() {
        out.println("descendA 1: True");
        BVNode objetoUno = new BVNode(new AABB2D(new Point2D(0, 0), 3.0, 3.0));
        objetoUno.setLeft(new AABB2D(new Point2D(-1, -1), 1.0, 1.0));
        objetoUno.setRight(new AABB2D(new Point2D(1, 1), 1.0, 1.0));

        BVNode objetoDos = new BVNode(new AABB2D(new Point2D(0, 0), 3.0, 3.0));
        objetoDos.setLeft(new AABB2D(new Point2D(-0.5, -0.5), 2.0, 2.0));
        objetoDos.setRight(new AABB2D(new Point2D(0.5, 0.5), 2.0, 2.0));

        boolean expResult = true;
        boolean result = descendA(objetoUno, objetoDos);
        assertEquals(expResult, result);
    }

    /**
     * Test of descendA method, of class BVNode.
     */
    @Test
    public void testDescendA_2() {
        out.println("descendA 2: False");
        BVNode objetoUno = new BVNode(new AABB2D(new Point2D(0, 0), 3.0, 3.0));
        objetoUno.setLeft(new AABB2D(new Point2D(0, 0.5), 3.0, 2.0));
        objetoUno.setRight(new AABB2D(new Point2D(0.5, 0.5), 2.0, 2.0));

        BVNode objetoDos = new BVNode(new AABB2D(new Point2D(0, 0), 4.0, 3.0));
        objetoDos.setLeft(new AABB2D(new Point2D(-1.0, -0.5), 2.0, 2.0));
        objetoDos.setRight(new AABB2D(new Point2D(1.0, 0.5), 2.0, 2.0));

        boolean expResult = false;
        boolean result = descendA(objetoUno, objetoDos);
        assertEquals(expResult, result);
    }

    /**
     * Test of descendA method, of class BVNode.
     */
    @Test
    public void testDescendA_3() {
        out.println("descendA 3: True");
        BVNode objetoUno = new BVNode(new AABB2D(new Point2D(0, 0), 4.0, 3.0));
        objetoUno.setLeft(new AABB2D(new Point2D(-1, -0.5), 2.0, 2.0));
        objetoUno.setRight(new AABB2D(new Point2D(0, 1), 4.0, 1.0));

        BVNode objetoDos = new BVNode(new AABB2D(new Point2D(0, 0), 2.0, 3.0));
        objetoDos.setLeft(new AABB2D(new Point2D(-0.5, 1.0), 1.0, 1.0));
        objetoDos.setRight(new AABB2D(new Point2D(0.5, -1.0), 1.0, 1.0));

        boolean expResult = false;
        boolean result = descendA(objetoUno, objetoDos);
        assertEquals(expResult, result);
    }

    /**
     * Test of intersects method, of class BVNode.
     */
    @Test
    public void testIntersects() {
        out.println("intersects");
        BVNode o1 = new BVNode(new AABB2D(new Point2D(), 2, 2));
        BVNode o2 = new BVNode(new AABB2D(new Point2D(3, 0), 2, 2));
        BVNode o3 = new BVNode(new AABB2D(new Point2D(1.75, -1.75), 2, 2));
        BVNode o4 = new BVNode(new AABB2D(new Point2D(2, -2), 2, 2));
        boolean expResult0 = false;
        boolean expResult1 = true;
        boolean expResult2 = true;
        boolean result0 = o1.intersects(o2);
        boolean result1 = o1.intersects(o3);
        boolean result2 = o1.intersects(o4);

        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
    }

    /**
     * Test of collidePrimitives method, of class BVNode.
     */
    @Test
    public void testCollidePrimitives_1() {
        out.println("collidePrimitives no collision at all");
        List<Pair<Integer>> listaColisiones = new ArrayList<>();
        BVNode objetoUno = new BVNode(new AABB2D(new Edge2D(new Point2D(-1, -1), new Point2D(1, 1)), 0));
        BVNode objetoDos = new BVNode(new AABB2D(new Edge2D(new Point2D(2, 1), new Point2D(4, -1)), 1));
        collidePrimitives(listaColisiones, objetoUno, objetoDos);
        assertTrue(listaColisiones.isEmpty());
    }

    /**
     * Test of collidePrimitives method, of class BVNode.
     */
    @Test
    public void testCollidePrimitives_2() {
        out.println("collidePrimitives: true collision");
        List<Pair<Integer>> listaColisiones = new ArrayList<>();
        Edge2D e00, e01, e02, e03, e10, e11, e12, e13, e14;
        Point2D p00, p01, p02, p03, p10, p11, p12, p13, p14;

        p00 = new Point2D();
        p01 = new Point2D(2, 1);
        p02 = new Point2D(1, 2);
        p03 = new Point2D(0, 1);
        p10 = new Point2D(1, 2);
        p11 = new Point2D(2, 2);
        p12 = new Point2D(4, 2.5);
        p13 = new Point2D(2, 3);
        p14 = new Point2D(1, 2.5);

        e00 = new Edge2D(p00, p01);
        e01 = new Edge2D(p01, p02);
        e02 = new Edge2D(p02, p03);
        e03 = new Edge2D(p03, p00);

        e10 = new Edge2D(p10, p11);
        e11 = new Edge2D(p11, p12);
        e12 = new Edge2D(p12, p13);
        e13 = new Edge2D(p13, p14);
        e14 = new Edge2D(p14, p10);

        BVNode objetoUno;
        BVNode objetoDos;
        BVNode base00, base01, base02, base03, base04, base05;
        BVNode base10, base11, base12, base13, base14, base15, base16, base17;

        base00 = new BVNode(new AABB2D(e00, 0));
        base01 = new BVNode(new AABB2D(e01, 1));
        base02 = new BVNode(new AABB2D(e02, 2));
        base03 = new BVNode(new AABB2D(e03, 3));

        base04 = new BVNode(base00.union(base01));
        base04.setLeft(base00);
        base04.setRight(base01);
        base05 = new BVNode(base02.union(base03));
        base05.setLeft(base02);
        base05.setRight(base03);

        objetoUno = new BVNode(base04.union(base05));
        objetoUno.setLeft(base04);
        objetoUno.setRight(base05);

        base10 = new BVNode(new AABB2D(e10, 0));
        base11 = new BVNode(new AABB2D(e11, 1));
        base12 = new BVNode(new AABB2D(e12, 2));
        base13 = new BVNode(new AABB2D(e13, 3));
        base14 = new BVNode(new AABB2D(e14, 4));

        base15 = new BVNode(base10.union(base14));
        base15.setLeft(base10);
        base15.setRight(base14);

        base16 = new BVNode(base12.union(base13));
        base16.setLeft(base12);
        base16.setRight(base13);

        base17 = new BVNode(base15.union(base16));
        base17.setLeft(base15);
        base17.setRight(base16);

        objetoDos = new BVNode(base11.union(base17));
        objetoDos.setLeft(base11);
        objetoDos.setRight(base17);

        collidePrimitives(listaColisiones, objetoUno, objetoDos);
        assertFalse(listaColisiones.isEmpty());
    }

    /**
     * Test of collidePrimitives method, of class BVNode.
     */
    @Test
    public void testCollidePrimitives_3() {
        out.println("collidePrimitives: Boxes only collision");
        List<Pair<Integer>> listaColisiones = new ArrayList<>();
        Point2D p00, p01, p02, p03, p10, p11, p12, p13;
        Edge2D e00, e01, e02, e03, e10, e11, e12, e13;
        BVNode nodo00, nodo01, nodo02, nodo03, nodo04, nodo05, objetoUno;
        BVNode nodo10, nodo11, nodo12, nodo13, nodo14, nodo15, objetoDos;

        p00 = new Point2D(1, 0);
        p01 = new Point2D(2, 1);
        p02 = new Point2D(1, 2);
        p03 = new Point2D(0, 1);

        e00 = new Edge2D(p00, p01);
        e01 = new Edge2D(p01, p02);
        e02 = new Edge2D(p02, p03);
        e03 = new Edge2D(p03, p00);

        nodo00 = new BVNode(new AABB2D(e00, 0));
        nodo01 = new BVNode(new AABB2D(e01, 1));
        nodo02 = new BVNode(new AABB2D(e02, 2));
        nodo03 = new BVNode(new AABB2D(e03, 3));

        nodo04 = new BVNode(nodo00.union(nodo01));
        nodo04.setLeft(nodo00);
        nodo04.setRight(nodo01);
        nodo05 = new BVNode(nodo02.union(nodo03));
        nodo05.setLeft(nodo02);
        nodo05.setRight(nodo03);
        objetoUno = new BVNode(nodo04.union(nodo05));
        objetoUno.setLeft(nodo04);
        objetoUno.setRight(nodo05);

        p10 = new Point2D(2.1, -1);
        p11 = new Point2D(3.1, 0);
        p12 = new Point2D(2.1, 1);
        p13 = new Point2D(1.1, 0);

        e10 = new Edge2D(p10, p11);
        e11 = new Edge2D(p11, p12);
        e12 = new Edge2D(p12, p13);
        e13 = new Edge2D(p13, p10);

        nodo10 = new BVNode(new AABB2D(e10, 0));
        nodo11 = new BVNode(new AABB2D(e11, 1));
        nodo12 = new BVNode(new AABB2D(e12, 2));
        nodo13 = new BVNode(new AABB2D(e13, 3));

        nodo14 = new BVNode(nodo10.union(nodo11));
        nodo14.setLeft(nodo10);
        nodo14.setRight(nodo11);
        nodo15 = new BVNode(nodo12.union(nodo13));
        nodo15.setLeft(nodo12);
        nodo15.setRight(nodo13);
        objetoDos = new BVNode(nodo14.union(nodo15));
        objetoUno.setLeft(nodo04);
        objetoUno.setRight(nodo05);

        collidePrimitives(listaColisiones, objetoUno, objetoDos);
        assertTrue(listaColisiones.isEmpty());
    }

    /**
     * Test of obtenerNodoCombinadoMinimaArea method, of class BVNode.
     */
    @Test
    public void testObtainMinimalAreaCombinadedNode() {
        out.println("obtainMinimalAreaCombinadedNode");
        List<BVNode> listaBVNodes = new ArrayList<>();
        listaBVNodes.add(new BVNode(new AABB2D(new Point2D(0.5, -2.5), 3, 3)));
        listaBVNodes.add(new BVNode(new AABB2D(new Point2D(4, 0), 2, 2)));
        listaBVNodes.add(new BVNode(new AABB2D(new Point2D(0, 0), 2, 2)));

        BVNode expResult = new BVNode(new AABB2D(new Point2D(2, 0), 6, 2));
        expResult.setLeft(new BVNode(new AABB2D(new Point2D(0, 0), 2, 2)));
        expResult.setRight(new BVNode(new AABB2D(new Point2D(4, 0), 2, 2)));

        BVNode result = obtainMinimalAreaCombinadedNode(listaBVNodes);
        assertEquals(expResult, result);
    }

    /**
     * Test of sortNodesList method, of class BVNode.
     */
    @Test
    public void testSortNodesList_1() {
        out.println("sortNodesList 1 - Different Area boxes");

        List<BVNode> listaNodos = new ArrayList<>();

        listaNodos.add(new BVNode(new AABB2D(new Point2D(-3, -5), 4, 4)));
        listaNodos.add(new BVNode(new AABB2D(new Point2D(2, -3), 3, 3)));
        listaNodos.add(new BVNode(new AABB2D(new Point2D(0, 0), 3.5, 3.5)));

        sortNodesList(listaNodos);

        assertEquals(listaNodos.get(2).getData(), new AABB2D(new Point2D(-3, -5), 4, 4));
        assertEquals(listaNodos.get(1).getData(), new AABB2D(new Point2D(0, 0), 3.5, 3.5));
        assertEquals(listaNodos.get(0).getData(), new AABB2D(new Point2D(2, -3), 3, 3));
    }

    /**
     * Test of sortNodesList method, of class BVNode.
     */
    @Test
    public void testSortNodesList_2() {
        out.println("sortNodesList 2 - Equal Area boxes");

        List<BVNode> listaNodos = new ArrayList<>();

        listaNodos.add(new BVNode(new AABB2D(new Point2D(-3, -5), 3, 4)));
        listaNodos.add(new BVNode(new AABB2D(new Point2D(2, -3), 3, 4)));
        listaNodos.add(new BVNode(new AABB2D(new Point2D(0, 0), 3, 4)));

        //al usar los metodos wrap y unwrap se ordenan en base a la varianza de los BV!!!
        sortNodesList(listaNodos);

        //areas iguales ordena por eje X primero y luego por eje Y
        assertEquals(listaNodos.get(0).getData(), new AABB2D(new Point2D(-3, -5), 3, 4));
        assertEquals(listaNodos.get(1).getData(), new AABB2D(new Point2D(0, 0), 3, 4));
        assertEquals(listaNodos.get(2).getData(), new AABB2D(new Point2D(2, -3), 3, 4));
    }

    /**
     * Test of equals method, of class BVNode.
     */
    @Test
    public void testEquals() {
        out.println("equals");
        this.cajaRoot = this.cajaLeft.unionBV(this.cajaRight);
        Object obj = new BVNode(this.cajaRoot);
        ((BVNode) obj).setLeft(this.cajaLeft);
        ((BVNode) obj).setRight(this.cajaRight);

        BVNode instance0 = new BVNode(this.cajaRoot);
        instance0.setLeft(this.cajaLeft);
        instance0.setRight(this.cajaRight);

        BVNode instance1 = new BVNode(this.cajaLeft.unionBV(this.cajaRight));
        instance1.setLeft(this.cajaLeft);

        boolean expResult0 = true;
        boolean expResult1 = false;

        boolean result0 = instance0.equals(obj);
        boolean result1 = instance1.equals(obj);

        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of obtenerIndiceMenorVolumenCombinado method, of class BVNode.
     */
    @Test
    public void testGetMinorCombinedVolumeIndice() {
        out.println("getMinorCombinedVolumeIndice");
        List<BVNode> listaBVNodes0 = new ArrayList<>();
        listaBVNodes0.add(new BVNode(new AABB2D(new Point2D(-1.5, 1.5), 1.0, 1.0))); // 0
        listaBVNodes0.add(new BVNode(new AABB2D(new Point2D(-0.5, 1.5), 1.0, 1.0))); // 1

        List<BVNode> listaBVNodes1 = new ArrayList<>();
        listaBVNodes1.add(new BVNode(new AABB2D(new Point2D(-1.5, 1.5), 1.0, 1.0))); // 0
        listaBVNodes1.add(new BVNode(new AABB2D(new Point2D(-1.5, -0.5), 1.0, 1.0))); // 1

        BVNode nodo = new BVNode(new AABB2D(new Edge2D(new Point2D(0, 1), new Point2D(1, 0)), 0));
        int expResult0 = 1, expResult1 = 1;
        int result0 = getMinorCombinedVolumeIndice(listaBVNodes0, nodo);
        int result1 = getMinorCombinedVolumeIndice(listaBVNodes1, nodo);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of isLeaf method, of class BVNode.
     */
    @Test
    public void testIsLeaf() {
        out.println("isLeaf");
        BVNode instance0 = new BVNode(new AABB2D(new Edge2D(new Point2D(0, 1), new Point2D(1, 0)), 0));
        BVNode instance1 = new BVNode(new AABB2D(new Point2D(-1.5, 1.5), 1.0, 1.0));
        instance1.setLeft(this.cajaRoot);

        boolean expResult0 = true, expResult1 = false;
        boolean result0 = instance0.isLeaf();
        boolean result1 = instance1.isLeaf();
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of addingNode method, of class BVNode.
     */
    @Test
    public void testAddingNode() {
        out.println("addingNode");
        this.cajaRoot = this.cajaLeft.unionBV(this.cajaRight);

        BVNode other = new BVNode(this.cajaLeft);
        BVNode instance = new BVNode(this.cajaRight);

        BVNode expResult = new BVNode(this.cajaRoot);
        expResult.setLeft(other);
        expResult.setRight(this.cajaRight);

        BVNode result = instance.addingNode(other);
        assertEquals(expResult, result);
    }

    /**
     * Test of obtenerAristasColisionando method, of class BVNode.
     */
    @Test
    public void testObtainCollidingEdges() {
        out.println("obtainCollidingEdges");

        AABB2D caja00, caja01, caja02, caja03;
        AABB2D caja10, caja11, caja12, caja13;
        AABB2D caja20, caja21, caja22, caja23;

        AABB2D root0, root1, root2;

        BVNode nodo00, nodo01, nodo02, nodo03;
        BVNode nodo10, nodo11, nodo12, nodo13;
        BVNode nodo20, nodo21, nodo22, nodo23;

        BVNode nodoRoot0, nodoRoot1, nodoRoot2;

        List<Pair<Integer>> listaColisiones0 = new ArrayList<>();
        List<Pair<Integer>> listaColisiones1 = new ArrayList<>();

        listaColisiones1.add(new PairInteger(1, 0));
        listaColisiones1.add(new PairInteger(1, 2));

        //objeto0
        List<Edge2D> listaAristas0 = new ArrayList<>();
        listaAristas0.add(new Edge2D(new Point2D(0, -1), new Point2D(1, 0)));
        listaAristas0.add(new Edge2D(new Point2D(1, 0), new Point2D(0, 1)));
        listaAristas0.add(new Edge2D(new Point2D(0, 1), new Point2D(0, -1)));

        caja00 = new AABB2D(listaAristas0.get(0), 0);
        caja01 = new AABB2D(listaAristas0.get(1), 1);
        caja02 = new AABB2D(listaAristas0.get(2), 2);

        nodo00 = new BVNode(caja00);
        nodo01 = new BVNode(caja01);
        nodo02 = new BVNode(caja02);

        caja03 = caja02.unionBV(caja00);
        nodo03 = new BVNode(caja03);
        nodo03.setLeft(nodo02);
        nodo03.setRight(nodo00);

        root0 = caja03.unionBV(caja01);
        nodoRoot0 = new BVNode(root0);
        nodoRoot0.setLeft(nodo01);
        nodoRoot0.setRight(nodo03);

        //objeto1
        List<Edge2D> listaAristas1 = new ArrayList<>();
        listaAristas1.add(new Edge2D(new Point2D(2, -2), new Point2D(3, -1)));
        listaAristas1.add(new Edge2D(new Point2D(3, -1), new Point2D(1, 1)));
        listaAristas1.add(new Edge2D(new Point2D(1, 1), new Point2D(2, -2)));

        caja10 = new AABB2D(listaAristas1.get(0), 0);
        caja11 = new AABB2D(listaAristas1.get(1), 1);
        caja12 = new AABB2D(listaAristas1.get(2), 2);

        nodo10 = new BVNode(caja10);
        nodo11 = new BVNode(caja11);
        nodo12 = new BVNode(caja12);

        caja13 = caja12.unionBV(caja10);
        nodo13 = new BVNode(caja13);
        nodo13.setLeft(nodo12);
        nodo13.setRight(nodo10);

        root1 = caja13.unionBV(caja11);
        nodoRoot1 = new BVNode(root1);
        nodoRoot1.setLeft(nodo11);
        nodoRoot1.setRight(nodo13);

        //objeto2
        List<Edge2D> listaAristas2 = new ArrayList<>();
        listaAristas2.add(new Edge2D(new Point2D(2, -1), new Point2D(4, 0)));
        listaAristas2.add(new Edge2D(new Point2D(4, 0), new Point2D(3, 1)));
        listaAristas2.add(new Edge2D(new Point2D(3, 1), new Point2D(2, -1)));

        caja20 = new AABB2D(listaAristas2.get(0), 0);
        caja21 = new AABB2D(listaAristas2.get(1), 1);
        caja22 = new AABB2D(listaAristas2.get(2), 2);

        nodo20 = new BVNode(caja20);
        nodo21 = new BVNode(caja21);
        nodo22 = new BVNode(caja22);

        caja23 = caja22.unionBV(caja20);
        nodo23 = new BVNode(caja23);
        nodo23.setLeft(nodo22);
        nodo23.setRight(nodo20);

        root2 = caja23.unionBV(caja21);
        nodoRoot2 = new BVNode(root2);
        nodoRoot2.setLeft(nodo21);
        nodoRoot2.setRight(nodo23);

        obtainCollidingEdges(listaColisiones0, nodoRoot0, nodoRoot1);
        obtainCollidingEdges(listaColisiones1, nodoRoot1, nodoRoot2);
        assertTrue(!listaColisiones1.isEmpty());
        assertTrue(listaColisiones0.isEmpty());
    }

    /**
     * Test of descendA method, of class BVNode.
     */
    @Test
    public void testDescendA() {
        out.println("descendA");

        AABB2D caja00, caja01, caja02, caja03;
        AABB2D caja10, caja11, caja12, caja13;
        AABB2D caja20, caja21, caja22, caja23;

        AABB2D root0, root1, root2;

        BVNode nodo00, nodo01, nodo02, nodo03;
        BVNode nodo10, nodo11, nodo12, nodo13;
        BVNode nodo20, nodo21, nodo22, nodo23;

        BVNode nodoRoot0, nodoRoot1, nodoRoot2;

        //List<Pair<Integer>> listaColisiones0 = new ArrayList<>();
        List<Pair<Integer>> listaColisiones1 = new ArrayList<>();

        listaColisiones1.add(new PairInteger(1, 0));
        listaColisiones1.add(new PairInteger(1, 2));

        //objeto0
        List<Edge2D> listaAristas0 = new ArrayList<>();
        listaAristas0.add(new Edge2D(new Point2D(0, -1), new Point2D(1, 0)));
        listaAristas0.add(new Edge2D(new Point2D(1, 0), new Point2D(0, 1)));
        listaAristas0.add(new Edge2D(new Point2D(0, 1), new Point2D(0, -1)));

        caja00 = new AABB2D(listaAristas0.get(0), 0);
        caja01 = new AABB2D(listaAristas0.get(1), 1);
        caja02 = new AABB2D(listaAristas0.get(2), 2);

        nodo00 = new BVNode(caja00);
        nodo01 = new BVNode(caja01);
        nodo02 = new BVNode(caja02);

        caja03 = caja02.unionBV(caja00);
        nodo03 = new BVNode(caja03);
        nodo03.setLeft(nodo02);
        nodo03.setRight(nodo00);

        root0 = caja03.unionBV(caja01);
        nodoRoot0 = new BVNode(root0);
        nodoRoot0.setLeft(nodo01);
        nodoRoot0.setRight(nodo03);

        //objeto1
        List<Edge2D> listaAristas1 = new ArrayList<>();
        listaAristas1.add(new Edge2D(new Point2D(2, -2), new Point2D(3, -1)));
        listaAristas1.add(new Edge2D(new Point2D(3, -1), new Point2D(1, 1)));
        listaAristas1.add(new Edge2D(new Point2D(1, 1), new Point2D(2, -2)));

        caja10 = new AABB2D(listaAristas1.get(0), 0);
        caja11 = new AABB2D(listaAristas1.get(1), 1);
        caja12 = new AABB2D(listaAristas1.get(2), 2);

        nodo10 = new BVNode(caja10);
        nodo11 = new BVNode(caja11);
        nodo12 = new BVNode(caja12);

        caja13 = caja12.unionBV(caja10);
        nodo13 = new BVNode(caja13);
        nodo13.setLeft(nodo12);
        nodo13.setRight(nodo10);

        root1 = caja13.unionBV(caja11);
        nodoRoot1 = new BVNode(root1);
        nodoRoot1.setLeft(nodo11);
        nodoRoot1.setRight(nodo13);

        //objeto2
        List<Edge2D> listaAristas2 = new ArrayList<>();
        listaAristas2.add(new Edge2D(new Point2D(2, -1), new Point2D(4, 0)));
        listaAristas2.add(new Edge2D(new Point2D(4, 0), new Point2D(3, 1)));
        listaAristas2.add(new Edge2D(new Point2D(3, 1), new Point2D(2, -1)));

        caja20 = new AABB2D(listaAristas2.get(0), 0);
        caja21 = new AABB2D(listaAristas2.get(1), 1);
        caja22 = new AABB2D(listaAristas2.get(2), 2);

        nodo20 = new BVNode(caja20);
        nodo21 = new BVNode(caja21);
        nodo22 = new BVNode(caja22);

        caja23 = caja22.unionBV(caja20);
        nodo23 = new BVNode(caja23);
        nodo23.setLeft(nodo22);
        nodo23.setRight(nodo20);

        root2 = caja23.unionBV(caja21);
        nodoRoot2 = new BVNode(root2);
        nodoRoot2.setLeft(nodo21);
        nodoRoot2.setRight(nodo23);

        //caso 1: obj2 es hoja
        BVNode objetoUno = nodo03;
        BVNode objetoDos = nodo12;
        boolean expResult0 = true;
        boolean result0 = descendA(objetoUno, objetoDos);
        assertEquals(expResult0, result0);

        // caso 2: obj1 es hoja pero areas de obj1 y obj2 son diferentes
        objetoUno = nodo00;
        objetoDos = nodo13;
        boolean expResult1 = false;
        boolean result1 = descendA(objetoUno, objetoDos);
        assertEquals(expResult1, result1);

        // caso 3: obj1 es hoja Y el area de ambos objs es igual
        objetoUno = nodo00;
        objetoDos = nodo10;
        boolean expResult2 = true;
        boolean result2 = descendA(objetoUno, objetoDos);
        assertEquals(expResult2, result2);
    }

    /**
     * Test of collidePrimitives method, of class BVNode.
     */
    @Test
    public void testCollidePrimitives() {
        out.println("collidePrimitives");
        AABB2D caja00, caja01, caja02, caja03;
        AABB2D caja10, caja11, caja12, caja13;
        AABB2D caja20, caja21, caja22, caja23;

        AABB2D root0, root1, root2;

        BVNode nodo00, nodo01, nodo02, nodo03;
        BVNode nodo10, nodo11, nodo12, nodo13;
        BVNode nodo20, nodo21, nodo22, nodo23;

        BVNode nodoRoot0, nodoRoot1, nodoRoot2;

        List<Pair<Integer>> listaColisiones0 = new ArrayList<>();
        List<Pair<Integer>> listaColisiones1 = new ArrayList<>();

        listaColisiones1.add(new PairInteger(1, 0));
        listaColisiones1.add(new PairInteger(1, 2));

        //objeto0
        List<Edge2D> listaAristas0 = new ArrayList<>();
        listaAristas0.add(new Edge2D(new Point2D(0, -1), new Point2D(1, 0)));
        listaAristas0.add(new Edge2D(new Point2D(1, 0), new Point2D(0, 1)));
        listaAristas0.add(new Edge2D(new Point2D(0, 1), new Point2D(0, -1)));

        caja00 = new AABB2D(listaAristas0.get(0), 0);
        caja01 = new AABB2D(listaAristas0.get(1), 1);
        caja02 = new AABB2D(listaAristas0.get(2), 2);

        nodo00 = new BVNode(caja00);
        nodo01 = new BVNode(caja01);
        nodo02 = new BVNode(caja02);

        caja03 = caja02.unionBV(caja00);
        nodo03 = new BVNode(caja03);
        nodo03.setLeft(nodo02);
        nodo03.setRight(nodo00);

        root0 = caja03.unionBV(caja01);
        nodoRoot0 = new BVNode(root0);
        nodoRoot0.setLeft(nodo01);
        nodoRoot0.setRight(nodo03);

        //objeto1
        List<Edge2D> listaAristas1 = new ArrayList<>();
        listaAristas1.add(new Edge2D(new Point2D(2, -2), new Point2D(3, -1)));
        listaAristas1.add(new Edge2D(new Point2D(3, -1), new Point2D(1, 1)));
        listaAristas1.add(new Edge2D(new Point2D(1, 1), new Point2D(2, -2)));

        caja10 = new AABB2D(listaAristas1.get(0), 0);
        caja11 = new AABB2D(listaAristas1.get(1), 1);
        caja12 = new AABB2D(listaAristas1.get(2), 2);

        nodo10 = new BVNode(caja10);
        nodo11 = new BVNode(caja11);
        nodo12 = new BVNode(caja12);

        caja13 = caja12.unionBV(caja10);
        nodo13 = new BVNode(caja13);
        nodo13.setLeft(nodo12);
        nodo13.setRight(nodo10);

        root1 = caja13.unionBV(caja11);
        nodoRoot1 = new BVNode(root1);
        nodoRoot1.setLeft(nodo11);
        nodoRoot1.setRight(nodo13);

        //objeto2
        List<Edge2D> listaAristas2 = new ArrayList<>();
        listaAristas2.add(new Edge2D(new Point2D(2, -1), new Point2D(4, 0)));
        listaAristas2.add(new Edge2D(new Point2D(4, 0), new Point2D(3, 1)));
        listaAristas2.add(new Edge2D(new Point2D(3, 1), new Point2D(2, -1)));

        caja20 = new AABB2D(listaAristas2.get(0), 0);
        caja21 = new AABB2D(listaAristas2.get(1), 1);
        caja22 = new AABB2D(listaAristas2.get(2), 2);

        nodo20 = new BVNode(caja20);
        nodo21 = new BVNode(caja21);
        nodo22 = new BVNode(caja22);

        caja23 = caja22.unionBV(caja20);
        nodo23 = new BVNode(caja23);
        nodo23.setLeft(nodo22);
        nodo23.setRight(nodo20);

        root2 = caja23.unionBV(caja21);
        nodoRoot2 = new BVNode(root2);
        nodoRoot2.setLeft(nodo21);
        nodoRoot2.setRight(nodo23);

        collidePrimitives(listaColisiones0, nodoRoot0, nodoRoot1);
        assertTrue(listaColisiones0.isEmpty());

        collidePrimitives(listaColisiones1, nodoRoot1, nodoRoot2);
        assertTrue(!listaColisiones1.isEmpty());
        assertEquals(listaColisiones1.get(0), new PairInteger(1, 0));
        assertEquals(listaColisiones1.get(1), new PairInteger(1, 2));
    }

    /**
     * Test of sortNodesList method, of class BVNode.
     */
    @Test
    public void testSortNodesList() {
        out.println("sortNodesList");
        BVNode node0 = new BVNode(new AABB2D(new Edge2D(new Point2D(0, 0), new Point2D(1, 1)), 0));
        BVNode node1 = new BVNode(new AABB2D(new Edge2D(new Point2D(0, 2), new Point2D(1, 0)), 1));
        BVNode node2 = new BVNode(new AABB2D(new Edge2D(new Point2D(0, 2), new Point2D(2, 0)), 2));
        //casos node3 y node4 son nodos compuestos
        Edge2D a0, a1, b0, b1;
        a0 = new Edge2D(new Point2D(0, 0), new Point2D(1, 1));
        a1 = new Edge2D(new Point2D(1, 1), new Point2D(0, 2));

        BVNode node3 = new BVNode(new AABB2D(a0, 0).unionBV(new AABB2D(a1, 1)));
        node3.setLeft(new AABB2D(a0, 0));
        node3.setRight(new AABB2D(a1, 1));

        b0 = new Edge2D(new Point2D(-1, 2), new Point2D(0, 0));
        b1 = new Edge2D(new Point2D(0, 0), new Point2D(1, 2));

        BVNode node4 = new BVNode(new AABB2D(b0, 0).unionBV(new AABB2D(b1, 1)));
        node4.setLeft(new AABB2D(b0, 0));
        node4.setRight(new AABB2D(b1, 1));

        List<BVNode> listaBVNodes = new ArrayList<>();
        listaBVNodes.add(node3);
        listaBVNodes.add(node0);
        listaBVNodes.add(node2);
        listaBVNodes.add(node1);
        listaBVNodes.add(node4);

        sortNodesList(listaBVNodes);
        assertEquals(listaBVNodes.get(0), node0);
        assertEquals(listaBVNodes.get(1), node3);
        assertEquals(listaBVNodes.get(2), node1);
        assertEquals(listaBVNodes.get(3), node4);
        assertEquals(listaBVNodes.get(4), node2);
    }
}
