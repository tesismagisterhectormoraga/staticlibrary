package cl.uchile.dcc.staticlibrary.primitives;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import static java.lang.System.out;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class PairDoubleTest {

    /**
     *
     */
    public PairDoubleTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of inverted method, of class PairDouble.
     */
    @Test
    public void testInverted() {
        out.println("inverted");
        Pair<Double> instance = new PairDouble(0.45, -0.35);
        Pair<Double> expResult = new PairDouble(-0.35, 0.45);
        Pair<Double> result = instance.inverted();
        assertEquals(expResult, result);
    }

    /**
     * Test of exists method, of class PairDouble.
     */
    @Test
    public void testExists() {
        out.println("exists");
        List<Pair<Double>> listaPares = new ArrayList<>();
        listaPares.add(new PairDouble(1.1, 2.2));
        listaPares.add(new PairDouble(-2.2, 1.1));
        listaPares.add(new PairDouble(3.3, -1.1));        
        Pair<Double> instance = new PairDouble(-2.2, 1.1);
        boolean expResult = true;
        boolean result = instance.exists(listaPares);
        assertEquals(expResult, result);
    }

    /**
     * Test of setFirst method, of class PairDouble.
     */
    @Test
    public void testSetFirst() {
        out.println("setFirst");
        Double first = -5.7132;
        Pair<Double> instance = new PairDouble(34.01, 28.45);
        instance.setFirst(first);
        assertEquals(first, instance.getFirst());
    }

    /**
     * Test of getFirst method, of class PairDouble.
     */
    @Test
    public void testGetFirst() {
        out.println("getFirst");
        Pair<Double> instance = new PairDouble(-6.1, -0.005);
        Double expResult = -6.1;
        Double result = instance.getFirst();
        assertEquals(expResult, result);
    }

    /**
     * Test of getSecond method, of class PairDouble.
     */
    @Test
    public void testGetSecond() {
        out.println("getSecond");
        Pair<Double> instance = new PairDouble(81.45, 45.08);
        Double expResult = 45.08;
        Double result = instance.getSecond();
        assertEquals(expResult, result);
    }

    /**
     * Test of setSecond method, of class PairDouble.
     */
    @Test
    public void testSetSecond() {
        out.println("setSecond");
        Double second = -5.03;
        Pair<Double> instance = new PairDouble(13.7, 25.14);
        instance.setSecond(second);
        assertEquals(second, instance.getSecond());
    }

    /**
     * Test of hashCode method, of class PairDouble.
     */
    @Test
    public void testHashCode() {
        out.println("hashCode");
        Pair<Double> instance = new PairDouble(1.5, 2.5);
        int expResult = 0;
        int result = instance.hashCode();
        assertNotEquals(expResult, result);
    }

    /**
     * Test of equals method, of class PairDouble.
     */
    @Test
    public void testEquals() {
        out.println("equals");
        Object obj0 = new PairDouble(6.52, 23.09);
        Object obj1 = new PairDouble(6.5, 23.09);
        Object obj2 = new PairDouble(6.52, 23.01);
        
        Pair<Double> instance = new PairDouble(6.52, 23.09);
        boolean expResult0 = true;
        boolean expResult1 = false;
        boolean expResult2 = false;
        boolean result0 = instance.equals(obj0);
        boolean result1 = instance.equals(obj1);
        boolean result2 = instance.equals(obj2);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
    }

    /**
     * Test of intersection method, of class PairDouble.
     */
    @Test
    public void testIntersection() {
        out.println("intersection");
        Pair<Double> otro = new PairDouble(-3.5, 3.2);
        PairDouble instance0 = new PairDouble(0, 1);
        PairDouble instance1 = new PairDouble(4.1, 6.7);
        PairDouble instance2 = new PairDouble(-5.1, -0.7);
        Pair<Double> expResult0 = new PairDouble(0, 1);
        Pair<Double> expResult1 = null;
        Pair<Double> expResult2 = new PairDouble(-3.5, -0.7);
        Pair<Double> result0 = instance0.intersection(otro);
        Pair<Double> result1 = instance1.intersection(otro);
        Pair<Double> result2 = instance2.intersection(otro);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
    }

}
